<?php



class User_model extends CI_Model {

	

	private $primary_key= 'id_user';

	private $table_name= 'tbl_user';

	



	function __construct(){

		parent::__construct();

	}

	

	function get_all($condition="",$like=0)

    {

		if($condition!=""){

			if($like==1)

				$this->db->like($condition);	

			else	

				$this->db->where($condition);

		}

        

        return $this->db->get($this->table_name)->result();

    }

	



	function get_paged_list($limit = 10, $offset = 0, $order_column = '', $order_type = 'asc',$condition=""){

		if($condition !=""){

			$this->db->where($condition);

		}

		if (empty($order_column) || empty($order_type))

			$this->db->order_by($this->primary_key,'asc');

		else

			$this->db->order_by($order_column, $order_type);

		return $this->db->get($this->table_name, $limit, $offset);

	}

	function login($limit = 10, $offset = 0, $order_column = '', $order_type = 'asc',$condition=""){

		$this->db->select('*');

		

		if($condition !=""){

			$this->db->where($condition);

		}



		return $this->db->get($this->table_name, $limit, $offset);

	}

	

	

	function count_all($condition=""){

		if($condition !=""){

			$this->db->where($condition);

		}

		

		$this->db->from($this->table_name);



		return $this->db->count_all_results();

	}

	

	function get_by_id($condition){

		$this->db->where($condition);

		return $this->db->get($this->table_name);

	}

	

	function save($person){

		$this->db->insert($this->table_name, $person);

		return $this->db->insert_id();

	}

	

	function update($condition, $person){

		foreach($person as $field=>$val){

			if($field == 'point')

				$escape = FALSE;

			else

				$escape = TRUE;

			$this->db->set($field, $val, $escape);

		}

		$this->db->where($condition);

		return $this->db->update($this->table_name);

	}

	

	function delete($id){

		$this->db->where($this->primary_key, $id);

		$this->db->delete($this->table_name);

	}

	

	

	function register($person){

		$this->db->insert($this->table_register, $person);

		return $this->db->insert_id();

	}

	

    function getCombo($like=1)
    {
        $condition = array("id_user"=>$this->custom_library->Filter_by_level($this->session->userdata("id_user"),$this->session->userdata("level")));
        if($condition!=""){
            if($like==1)
                foreach($condition as $field=>$val){
                    $this->db->like($field,$val,'after');
                }
            else    
                $this->db->where($condition);
        }
        $this->db->select("id_user as value, nama_user as label");
        $this->db->order_by('nama_user', 'asc');
        return $this->db->get('tbl_user')->result_array();
    }
	

}