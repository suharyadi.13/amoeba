<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Konsumen_model extends CI_Model
{

    public $table = 'tbl_konsumen';
	public $view = 'view_konsumen';
    public $id = 'id_konsumen';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all($condition="",$like=0)
    {
		if($condition!=""){
			if($like==1)
				foreach($condition as $field=>$val){
					$this->db->like($field,$val,'after');
				}
			else	
				$this->db->where($condition);
		}
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->view)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->view)->row();
    }
    
    // get total rows
    function total_rows($q = NULL,$condition="") {
	if($condition!=""){
		foreach($condition as $field=>$val){
			$this->db->like($field,$val,'after');
		}
	}else{
		$this->db->like('id_user', $q);
	}	
        //$this->db->like('id_konsumen', $q);
	//$this->db->or_like('id_user', $q);
	//$this->db->or_like('nama_konsumen', $q);
	//$this->db->or_like('telp_konsumen', $q);
	//$this->db->or_like('alamat_konsumen', $q);
	//$this->db->or_like('status', $q);
	$this->db->from($this->view);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL,$condition="") {
        $this->db->order_by($this->id, $this->order);
        //$this->db->like('id_konsumen', $q);
	if($condition!=""){
		foreach($condition as $field=>$val){
			$this->db->like($field,$val,'after');
		}
	}else{
		$this->db->like('id_user', $q);
	}	
	
	//$this->db->or_like('nama_konsumen', $q);
	//$this->db->or_like('telp_konsumen', $q);
	//$this->db->or_like('alamat_konsumen', $q);
	//$this->db->or_like('status', $q);
	
	
	$this->db->limit($limit, $start);
        return $this->db->get($this->view)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
		$this->db->query("delete  from tbl_kunjungan WHERE tbl_kunjungan.id_konsumen ='".$id."' ");
		$this->db->query("delete  from tbl_transaksi WHERE tbl_transaksi.id_konsumen ='".$id."' ");
    }

    function getCombo($table,$fieldID="id",$FieldName="nama"){
        
        return $this->db->select($fieldID.' as value, '.$FieldName.' as label')
                            ->from($table)
                            ->get()->result_array();
    }

}

/* End of file Konsumen_model.php */
/* Location: ./application/models/Konsumen_model.php */
/* Please DO NOT modify this information : tbl_transaksi*/
/* Generated by Harviacode Codeigniter CRUD Generator 2016-08-17 11:00:45 */
/* http://harviacode.com */