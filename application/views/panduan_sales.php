<html>
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width,initial-scale=1">

<title>Panduan Laporan SIP</title>    
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="https://kit-pro.fontawesome.com/releases/v5.9.0/css/pro.min.css">
<link rel="stylesheet" type="text/css" href="http://202.149.67.146:800/surplus//assets/login_v3/fonts/iconic/css/material-design-iconic-font.min.css">

<style type="text/css">
table { 
  table-layout:fixed;
  width:110%;
}   
td,th{
    width:100px;
    white-space:nowrap;

}
</style>  

</head>
<body>
    <div class="container-fluid">
    	<div class="row d-flex justify-content-center">
           <div class="col-md-8 col-12"> 
    		<span class="h3"> Langkah-langkah melaporkan transaksi via WhatssApp</span>
    		<ol type="1">
    			<li> tambahakan dahulu kontak d bawah ini</li>
    			<li> Kirim pesan kenkontak tersebut "join ill-courage" (tanpa kutip)<br>
    				tunggu smp ada balasan dari nomor tersebut dengan pesan "Twilio Sandbox: ✅ You are all set! The sandbox can now send/receive "	
    			</li>
                <li> kirim lagi pesan dengan format : <strong> REG#kode agen#Nama Lengkap#jabatan (sales/korsal/spv/manager/korwil)</strong></li>
    			<li> Setelah pesan tersebut terikrim (ceklis 2 ✅✅) maka anda bisa mulai melaprkan ke nomor tersebut</li>
                <li> Untuk No 2 dan 3, cukup dilakukan satu kali saja. jika sudah dilakukan tidak perlu d ulangi lagi.</li>
    			<li> Kirim laporan dengan format : <br>
    				<b>kode agen#No Faktur#Sapi-Domba-1/7#ist/spr/A/B/C/D#Jumlah Ekor</b><br>
    				<ul>
    					<li>kode agen : silahkan minta kepada agen nya</li>
    					<li>No Faktur : silahkan minta kepada agen nya</li>
    					<li>Sapi-Domba-1/7 : isi dengan salah satu  (contoh: sapi)</li>
    					<li>ist/spr/A/B/C/D : Isi salah satu ( contoh : A)</li>
    					<li>Jumlah : Jumlah ekor dari kelas tersebut</li>
    					<li>jika dalam 1 faktur ada 2 kelas maka d laporkan 2x dengan format yang sama namun kelas dan jumlahnya masing-masing</li>
    				</ul>
                </li>
    			<li> laporan dikirim dengan format diatas dan dikirim ke no kontak laporan agen</li>
    			<li> Jika pesan anda sudah ceklis 2 (✅✅) maka laporan sudah masuk </li>
    		</ol>
        </div>
        <div class="col-12 col-md-8">
            <h2>Berikut ini adalah video langkah-langkah untuk membuat laporan</h2>
            <div class="embed-responsive embed-responsive-16by9">
              <iframe class="embed-responsive-item" src="<?php echo base_url("video_panduan.mp4")?>" allowfullscreen></iframe>
            </div>
        </div>
    	</div>
    </div>
</body>
</html>


<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script type="text/javascript" charset="utf8" src="<?php echo base_url() ?>/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" charset="utf8" src="<?php echo base_url() ?>/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/select/1.3.0/js/dataTables.select.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>/js/datatables.min.js"></script>

<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>





<script language="javascript">

</script>