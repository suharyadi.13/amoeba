  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
    <meta name="author" content="GeeksLabs">
    <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
    <link rel="shortcut icon" href="img/favicon.png">

    <title><?php echo $this->config->item('company_name') ?></title>


    <!-- Bootstrap css     -->
    <link href="<?php echo base_url(); ?>css/bootstrap.min.css" rel="stylesheet">
    <!-- bootstrap theme -->
    <link href="<?php echo base_url(); ?>css/bootstrap-theme.css" rel="stylesheet">
    <!--external <?php echo base_url(); ?>css-->
    <!-- font icon -->
    <link href="<?php echo base_url(); ?>css/elegant-icons-style.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>css/font-awesome.min.css" rel="stylesheet" />    
    <!-- full calendar <?php echo base_url(); ?>css-->
    <link href="<?php echo base_url(); ?>assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" />
    <!-- easy pie chart-->
    <link href="<?php echo base_url(); ?>assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/<?php echo base_url(); ?>css" media="screen"/>
    <!-- owl carousel -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/owl.carousel.css" type="text/<?php echo base_url(); ?>css">
	<link href="<?php echo base_url(); ?>css/jquery-jvectormap-1.2.2.css" rel="stylesheet">
    <!-- Custom styles -->
	<link rel="<?php echo base_url(); ?>stylesheet" href="<?php echo base_url(); ?>css/fullcalendar.css">
	<link href="<?php echo base_url(); ?>css/widgets.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>css/style-responsive.css" rel="stylesheet" />
	<script src="<?php echo base_url()."assets/js/jquery-1.11.2.min.js" ?>" ></script>
	
	<link rel="stylesheet" href="<?php echo base_url('assets/bootstrapdatepicker') ?>/datepicker.min.css" />
	<link rel="stylesheet" href="<?php echo base_url('assets/bootstrapdatepicker') ?>/datepicker3.min.css" />

	<script src="<?php echo base_url('assets/bootstrapdatepicker') ?>/bootstrap-datepicker.min.js"></script>
  <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
	<script src="<?php echo base_url('js') ?>/jquery.number.js"></script>
	<style type="text/css">
	/**
	 * Override feedback icon position
	 * See http://formvalidation.io/examples/adjusting-feedback-icon-position/
	 */
	#eventForm .form-control-feedback {
		top: 0;
		right: -15px;
	}
	</style>
	
	<style>
/* NOTE: The styles were added inline because Prefixfree needs access to your styles and they must be inlined if they are on local disk! */

table#mytable { 
margin: 0; white-space:nowrap;
margin: 0 auto;
border-collapse: collapse;
font-family: Agenda-Light, sans-serif;
font-weight: 100; 
background: #333; color: #fff;
text-rendering: optimizeLegibility;
border-radius: 5px; 
}

table#mytable thead th { font-weight: 600; }
table#mytable thead th, table#mytable tbody td { 
  padding: .8rem; font-size: 1.4rem;
}
table#mytable tbody td { 
  padding: .8rem; font-size: 1.4rem;
  color: #444; background: #eee; 
  vertical-align:top;
}
table#mytable tbody tr:not(:last-child) { 
  border-top: 1px solid #ddd;
  border-bottom: 1px solid #ddd;  
}

@media screen and (max-width: 600px) {
  table#mytable caption { background-image: none; }
  table#mytable thead { display: none; }
  table#mytable tbody td { 
    display: block; padding: .6rem; 
  }
  table#mytable tbody tr td:first-child{ 
	display:none;
  }
  table#mytable tbody tr td:first-child+td { 
    background: #666; color: #fff; 
  }
	table#mytable tbody td:before { 
    content: attr(data-th); 
    font-weight: bold;
    display: inline-block;
    width: 6rem;  
  }
}
    </style>
	
	
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
      <script src="js/lte-ie7.js"></script>
    <![endif]-->
  </head>