<?php
$this->load->view("head_file");

//var_dump($berita);

?>
<style>
video {
    width: 100%;
    height: auto;
}
</style>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<div id="myCarousel" class="carousel slide" data-ride="carousel">
      <!-- Wrapper for slides -->
    <div class="carousel-inner">
		<?php
			$active ="active";
			if(is_array($berita )){
				foreach($berita as $item){
					echo '<div class="item '.$active.'">
						<div style="background-color:#FFF;padding:0px 10px 10px 10px;">
						<h2>'.$item->judul.'</h2>';
					echo $item->pesan;
					echo '
						</div>
						</div>';
					$active ="";	
				}
			}
			?>
     
    
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
  


<div class="row">
	<div class="col-md-12" style="padding:10px;text-align:center">
		<a href="<?php echo site_url("dashboard") ?>" class="btn btn-primary">Lanjutkan</a>
	</div>
</div>
