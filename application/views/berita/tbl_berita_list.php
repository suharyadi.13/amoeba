
        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-4">
                <h2 style="margin-top:0px">Data Berita</h2>
            </div>
            <div class="col-md-4 text-center">
                <div style="margin-top: 4px"  id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
            <div class="col-md-4 text-right">
                <?php echo anchor(site_url('berita/create'), 'Create', 'class="btn btn-primary"'); ?>
	    </div>
        </div>
        <table class="table table-bordered table-striped" id="mytable">
            <thead>
                <tr>
                    <th width="80px">No</th>
		    <th>Id User Post</th>
		    <th>Id Level Tujuan</th>
			<th>Judul</th>
		    <th>Pesan</th>
		    <th>Date Expired</th>
		    <th>Date Time</th>
		    <th>Action</th>
                </tr>
            </thead>
	    <tbody>
            <?php
            $start = 0;
            foreach ($berita_data as $berita)
            {
                ?>
                <tr>
		    <td><?php echo ++$start ?></td>
		    <td><?php echo $berita->id_user_post ?></td>
		    <td><?php 
				$level = array("","korsal","spv","manager","Agen","Rayon");
				$tujuan = explode(",",$berita->id_level_tujuan );
				if(is_array($tujuan)){
					foreach($tujuan as $lvltujuan){
						echo $level[$lvltujuan]."<br>";
					}
				}else{
					echo $berita->id_level_tujuan ;
				}
				
			?></td>
		    <td><?php echo $berita->judul ?></td>
			<td><?php echo $berita->pesan ?></td>
		    <td><?php echo $berita->date_expired ?></td>
		    <td><?php echo $berita->date_time ?></td>
		    <td style="text-align:center" width="200px">
			<?php 
			echo anchor(site_url('berita/read/'.$berita->id_berita),'Read'); 
			echo ' | '; 
			echo anchor(site_url('berita/update/'.$berita->id_berita),'Update'); 
			echo ' | '; 
			echo anchor(site_url('berita/delete/'.$berita->id_berita),'Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
			?>
		    </td>
	        </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
        <script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/datatables/jquery.dataTables.js') ?>"></script>
        <script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js') ?>"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $("#mytable").dataTable();
            });
        </script>
    