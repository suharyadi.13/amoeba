<div class="row">
    <div class="col-lg-12">
        <div class="card card-outline-info">
            <div class="card-header">

               
                <h2><a href="#"><?php echo $title; ?></a></h2>  
            </div>
            <div class="card-body">
                <?php
                if($this->session->flashdata("message")!=""){
                    echo '<div class="alert alert-primary" role="alert">
                          '.$this->session->flashdata("message").'
                        </div>';
                }
                ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <table class="table table-bordered table-striped" id="mytable">
                                    <thead>
                                        <tr>
                                        <?php
                                        foreach($field as $key=>$item){
                                            if(is_array($item)){  
                                                echo '<th>'.$item['label'].'</th>';
                                            }else{
                                                echo '<th>'.$item.'</th>';
                                            }
                                        }
                                        
                                        ?>
                                        </tr>
                                    </thead> 
                                    <tbody>
                                    <?php
                                    
                                    foreach($list_data as $index=>$value){
                                        echo '<tr>';
                                        foreach($field as $key=>$item){
                                            echo '<td>'.$value[$key].'</td>';
                                        }
                                        
                                        echo '</tr>';
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</div>
                    
