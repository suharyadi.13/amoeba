<style>
#th_jtrans,#td_jtrans{display:none; }
</style>

<!doctype html>
<html>
    <head>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('assets/datatables/dataTables.bootstrap.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-4">
                <h2 style="margin-top:0px">Data Stok Hewan</h2>
            </div>
            <div class="col-md-4 text-center">
                <div style="margin-top: 4px"  id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
            <div class="col-md-4 text-right">
                <?php //echo anchor(site_url('datauser/create'), 'Create', 'class="btn btn-primary"'); ?>
				<?php //echo anchor(site_url('datauser/excel'), 'Excel', 'class="btn btn-primary"'); ?>
	    </div>
        </div>
        <table class="table table-bordered table-striped" id="mytable">
            <thead>
                <tr>
                    <th width="80px">No</th>
		    <th>Nama Depot</th>
			<th>Stok Ist</th>
		    <th>Stok Spr</th>
		    <th>Stok A</th>
		    <th>Stok B</th>
		    <th>Stok C</th>
		    <th>Stok D</th>
		    <th>Action</th>
                </tr>
            </thead>
	    <tbody>
            <?php
            $start = 0;
            foreach ($stok_hewan_data as $stok_hewan)
            {
                ?>
                <tr>
		    <td><?php echo ++$start ?></td>
			<td><?php echo $stok_hewan->nama_depot ?></td>
		    <td><?php echo $stok_hewan->stok_ist ?></td>
		    <td><?php echo $stok_hewan->stok_spr ?></td>
		    <td><?php echo $stok_hewan->stok_a ?></td>
		    <td><?php echo $stok_hewan->stok_b ?></td>
		    <td><?php echo $stok_hewan->stok_c ?></td>
		    <td><?php echo $stok_hewan->stok_d ?></td>
		    <td style="text-align:center" width="200px">
			<?php 
			echo anchor(site_url('stok_hewan/update/'.$stok_hewan->id_depot),'Update'); 
			?>
		    </td>
	        </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
        <script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/datatables/jquery.dataTables.js') ?>"></script>
        <script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js') ?>"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $("#mytable").dataTable();
            });
        </script>
    </body>
</html>