
<?php

/* Mengambil query report*/
$kolom = array("L1","L2","L3","L4","L5","L6");//
$x = 0;
//$temp = []
$y = 0;
$tgl = array();
//$valueAll = [];
foreach($report as $result){
	
	$tgl[$x] = $result['tanggal_transaksi']; //ambil kolom
	foreach($kolom as $field1){
		if($x == 0){
		$value[$field1][$x] = (float)$result['trx_'.$field1] ;
		}else{
			$value[$field1][$x] = (float)$result['trx_'.$field1] +$value[$field1][$x-1];
		}
	}
	$x++;
}
  /* end mengambil query*/
?>


<!-- load library jquery dan highcharts -->
<script src="<?php echo base_url();?>assets/highcharts/jquery.js"></script>
<script src="<?php echo base_url();?>assets/highcharts/highcharts.js"></script>
<script src="<?php echo base_url();?>assets/highcharts/exporting.js"></script>
<!-- end load library -->
<div class="col-sm-12">
  <section class="panel">
	  <header class="panel-heading no-border">
		Akumulasi Transaksi
	  </header>
		<table id='' class="table table-condensed">
		<tr >
		<td>Agen/Tanggal</td>
		<?php
		foreach ($tgl as $field_tgl){
			echo"<td>".substr($field_tgl,6,6)."</td>";
		}
		?>
		</tr>
		<?php
		$x=0;
		foreach($value as $data){
			echo "<tr>";
			echo "<td>".$kolom[$x]."</td>";
			foreach($data as $item){
				echo "<td>".$item."</td>";
			}
			echo "</tr>";
			$x++;
		}
		?>
		</table>
  </section>
</div>
<div id="chart" height="800" width="1000"></canvas>

<script language="javascript">
$(function () {
    $('#chart').highcharts({
		colors: ['#ff0000', '#000', '#33cccc', '#31981f', '#1a1aff', '#64E572', '#FF9655', '#FFF263', '#6AF9C4'],
        title: {
            text: 'Progres Transaksi Harian',
            x: -20 //center
        },
        subtitle: {
            text: 'Sistem Informasi Pemasaran',
            x: -20
        },
        xAxis: {
            categories: <?php echo json_encode($tgl);?>
        },
        yAxis: {
            title: {
                text: 'Jumlah'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ' Ekor'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [
		<?php 
		$max = count($kolom);
		for($x=0;$x<$max;$x++){
			$field2 = $kolom[$x];
		?>
			{
				name: "<?php echo $field2 ?>",
				data: <?php echo json_encode($value[$field2]);?>
			}
		<?php
			if($x<($max-1))
				echo ",";
			}
		?>
		]
    });
});

</script>

