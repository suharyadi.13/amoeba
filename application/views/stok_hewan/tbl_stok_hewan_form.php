<!doctype html>
<html>
    <head>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Stok Hewan <?php echo $button ?></h2>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="float">Stok Ist <?php echo form_error('stok_ist') ?></label>
            <input type="text" class="form-control" name="stok_ist" id="stok_ist" placeholder="Stok Ist" value="<?php echo $stok_ist; ?>" />
        </div>
	    <div class="form-group">
            <label for="float">Stok Spr <?php echo form_error('stok_spr') ?></label>
            <input type="text" class="form-control" name="stok_spr" id="stok_spr" placeholder="Stok Spr" value="<?php echo $stok_spr; ?>" />
        </div>
	    <div class="form-group">
            <label for="float">Stok A <?php echo form_error('stok_a') ?></label>
            <input type="text" class="form-control" name="stok_a" id="stok_a" placeholder="Stok A" value="<?php echo $stok_a; ?>" />
        </div>
	    <div class="form-group">
            <label for="float">Stok B <?php echo form_error('stok_b') ?></label>
            <input type="text" class="form-control" name="stok_b" id="stok_b" placeholder="Stok B" value="<?php echo $stok_b; ?>" />
        </div>
	    <div class="form-group">
            <label for="float">Stok C <?php echo form_error('stok_c') ?></label>
            <input type="text" class="form-control" name="stok_c" id="stok_c" placeholder="Stok C" value="<?php echo $stok_c; ?>" />
        </div>
	    <div class="form-group">
            <label for="float">Stok D <?php echo form_error('stok_d') ?></label>
            <input type="text" class="form-control" name="stok_d" id="stok_d" placeholder="Stok D" value="<?php echo $stok_d; ?>" />
        </div>
	    <input type="hidden" name="id_depot" value="<?php echo $id_depot; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('stok_hewan') ?>" class="btn btn-default">Cancel</a>
	</form>
    </body>
</html>