
<div class="post-body">
	<div class="box-body table-responsive no-padding">
		<div class="row">
			<div class="col-lg-12">
            <div class="card card-outline-info">
                <div class="card-header">
                    <h4 class="m-b-0 text-white"><h2><a href="#"><?php echo $title; ?></a></h2></h4>
                </div>
                <div class="card-body">
                   
                    <?php
                   
                    ?>
                    <!-- <form action="<?php echo $save_url ?>" method="POST" enctype="multipart"> -->
                        <input type="hidden" name="action" value="<?php echo $action ?>">
                        <input type="hidden" name="field_primary" value="<?php echo $field_primary ?>">
                    	<div><?php echo (validation_errors()); ?></div>
                        <div class="form-body">
                        <?php
                        $x=0;
                        foreach($field as $key=>$item){
                        if(isset($data[$key])){
                            $valueItem = $data[$key];
                        }else{
                            $valueItem="";
                        }
                        if(is_array($item)){
                            if($item['type']=="combo"){
                                $itemInput = '<label class="control-label">'.$item['label'].'</label>
                                                <select readonly name="'.$key.'" class="form-control" >';
                                                foreach ($item['data'] as $indexItem => $Itemvalue) {
                                                    if($valueItem ==$Itemvalue['value'] ){
                                                        $selected = "selected";
                                                    }else{
                                                        $selected = "selected";
                                                    }
                                                    $itemInput .= '<option '.$selected.' value="'.$Itemvalue['value'].'">'.$Itemvalue['label'].'</option>';
                                                }
                                $itemInput .= '</select>';
                            }else if($item['type']=="date"){
                                $itemInput = '<label class="control-label">'.$item['label'].'</label>
                                        <input type="date" readonly id="'.$key.'" name="'.$key.'" class="form-control" placeholder="Masukan '.$item['label'].'"
                                        value ="'.$valueItem.'">';
                            }else if($item['type']=="file"){
                                $itemInput = '<label class="control-label">'.$item['label'].'</label>
                                        <input type="file" readonly id="'.$key.'" name="'.$key.'" class="form-control" placeholder="Masukan '.$item['label'].'"
                                        value ="'.$valueItem.'">';
                            }
                        }else{
                            $itemInput = '<label class="control-label">'.$item.'</label>
                                        <input type="text" readonly id="'.$key.'" name="'.$key.'" class="form-control" disable placeholder="Masukan '.$item.'" value ="'.$valueItem.'">';
                        }
                        if($x%2==0){
                        ?>
                            <div class="row p-t-20">
                                <div class="col-md-6">
                                    <div class="form-group m-0">
                                        <?php echo $itemInput; ?>
                                    </div>
                                </div>
                              
                        <?php
                    	}else{
                    	?>
                    		 <div class="col-md-6">
                                    <div class="form-group m-0">
                                        <?php echo $itemInput; ?>
                                    </div>
                                </div>
                               
                            </div>
                    	<?php	
                    	}	
                        $x++;
                        }
                        ?>  
                        </div>
                        <div class="form-actions mt-2">
                            
                            <button type="button" class="btn btn-primary" onclick="window.history.go(-1); return false;">Kembali</button>
                            <?php 
                            if(isset($data['tgl_PO']) && $data['tgl_PO'] !=""){
                            ?>
                                <button type="button" class="btn btn-danger pull-right" >PROSPECT SUDAH CLOSING</button>

                            <?php
                            }else{
                            ?>
                                <button type="button" class="btn btn-success pull-right" id="closing_prospect" data-toggle="modal" data-target="#myModal" >Closing Prospect</button>
                            <?php 
                            }
                            ?>
                        </div>
                    
                </div>
            </div>
        </div>
	</div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card card-outline-info">
                <?php $this->load->view("display",$data_sub); ?>
            </div>
        </div>
	</div>
</div>
<div class="modal" tabindex="-1" role="dialog" id="myModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    <?php echo form_open_multipart($closing_url , 'id="form_closing_prospect"', $hidden);  ?>    
    
      <div class="modal-header">
        <h5 class="modal-title">Form Closing Prospect</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <div class="row">
                <div class="col-md-4"> </div>
                <div class="col-md-4">Tanggal Closing</div>
                <div class="col-md-4">
                   <input type="date" name="tgl_closing" id="tgl_closing">
                   <input type="hidden" name="id_prospect" value="<?php  ?>">
                </div>
            </div>
            <div class="row">
                <div class="col-md-4"> </div>
                <div class="col-md-4">Nominal PO</div>
                <div class="col-md-4">
                   <input type="text" name="nominal_po" id="nominal_po">
                </div>
            </div>
            <?php
            foreach($data_sub['list_data'] as $index=>$item){
                echo '<div class="row">
                        <div class="col-md-4">'.$item['nama_product'].' '.$item['type'].'</div>
                        <div class="col-md-4">Rp '.number_format($item['harga_satuan']).'</div>
                        <div class="col-md-4">
                            <select name="status['.$item['id_detail_prospect'].']">
                                <option value="deal">Deal</option>
                                <option value="batal">Batal</option>
                                <option value="pending">Pending</option>
                            </select>
                        </div>
                    </div>
                ';
            } 
            ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" id="save_closing">Save changes</button>
      </div>
    </form>
    </div>
  </div>
</div>