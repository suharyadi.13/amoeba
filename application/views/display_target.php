<div class="row">
    <div class="col-lg-12">
        <div class="card card-outline-info">
            <div class="card-header">
                <!-- <a href="<?php echo $add_realisasi ?>" class="btn btn-lg btn-primary pull-right"> Input Realisasi</a>  -->
                <a href="<?php echo $add_url ?>" class="btn btn-lg btn-success pull-right"> Tambah Target</a> 
                <a href="<?php echo $back_url ?>" title="kembali" class="btn btn-lg btn-warning pull-right"> << </a> 
                <h2><a href="#"><?php echo $title; ?></a></h2>  
            </div>
            <div class="card-body">
                <?php
                if($this->session->flashdata("message")!=""){
                    echo '<div class="alert alert-primary" role="alert">
                          '.$this->session->flashdata("message").'
                        </div>';
                }
                ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <table class="table table-bordered table-striped" id="mytable">
                                    <thead>
                                        <tr>
                                        <td rowspan="2" >Sales</td>
                                        <td rowspan="2" >Tahun </td>
                                        <td rowspan="2" >Target Thn </td>
                                        <td colspan='2'>jan</td>
                                        <td colspan='2'>feb</td>
                                        <td colspan='2'>mar</td>
                                        <td colspan='2'>apr</td>
                                        <td colspan='2'>mei</td>
                                        <td colspan='2'>jun</td>
                                        <td colspan='2'>jul</td>
                                        <td colspan='2'>agu</td>
                                        <td colspan='2'>sep</td>
                                        <td colspan='2'>okt</td>
                                        <td colspan='2'>nov</td>
                                        <td colspan='2'>des</td>
                                        </tr>
                                        <tr>
                                            <td>Rncn</td>
                                            <td>Real</td>
                                            <td>Rncn</td>
                                            <td>Real</td>
                                            <td>Rncn</td>
                                            <td>Real</td>
                                            <td>Rncn</td>
                                            <td>Real</td>
                                            <td>Rncn</td>
                                            <td>Real</td>
                                            <td>Rncn</td>
                                            <td>Real</td>
                                            <td>Rncn</td>
                                            <td>Real</td>
                                            <td>Rncn</td>
                                            <td>Real</td>
                                            <td>Rncn</td>
                                            <td>Real</td>
                                            <td>Rncn</td>
                                            <td>Real</td>
                                            <td>Rncn</td>
                                            <td>Real</td>
                                            <td>Rncn</td>
                                            <td>Real</td>
                                        </tr>
                                    </thead> 
                                    <tbody>
                                    <?php
                                    
                                    foreach($list_data as $index=>$value){
                                        echo '<tr>';
                                        foreach($field as $key=>$item){
                                            echo '<td>'.$value[$key].'</td>';
                                        }
                                        echo '<td>
                                            <a href="'.$edit_url.'/'.$value[$field_primary].'"><i class="fa fa-edit"></i></a>
                                            <a href="'.$detail_url.'/'.$value[$field_primary].'"><i class="fa fa-table"></i></a>
                                            <a href="'.$delete_url.'/'.$value[$field_primary].'" onClick="return confirm(\'Apakah anda yakin akan menghapus data? \')"><i class="fa fa-trash-o"></i></a>
                                            </td>';
                                        echo '</tr>';
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</div>
                    
