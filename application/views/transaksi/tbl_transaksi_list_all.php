
<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('assets/datatables/dataTables.bootstrap.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-4">
                <h2 style="margin-top:0px">Rekap Data Transaksi</h2>
            </div>
            <div class="col-md-4 text-center">
                <div style="margin-top: 4px"  id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
            <div class="col-md-4 text-right">
		<?php echo anchor(site_url('transaksi/excel'), 'Excel', 'class="btn btn-primary"'); ?>
	    </div>
        </div>
		<div style="    max-width: 1180px;    overflow-x: scroll;">
        <table class="table table-bordered table-striped" id="mytable">
            <thead>
                <tr>
                    <th>No</th>
					<th>Konsumen</th>
					<th>Korsal</th>
					<th>Sales</th>
					<th>Depot</th>
					<th>No Faktur</th>
					<th>Tgl Transaksi</th>
					<th>PL / PS</th>
					<th>Jenis Order </th>
					<th>Kelas </th>
					<th>Harga </th>
					<th>Lain Lain</th>
					<th>Uang Muka</th>
					<th>Tgl Kirim</th>
					<th>Action</th>
                </tr>
            </thead>
			<tbody>
            </tbody>
        </table>
		</div>
        <script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/datatables/jquery.dataTables.js') ?>"></script>
        <script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js') ?>"></script>
        <script type="text/javascript">
            var dataSet = [<?php echo $dataset; ?>]
            $("#mytable").dataTable({
					  data: dataSet
					}
				);
        </script>
    </body>
</html>