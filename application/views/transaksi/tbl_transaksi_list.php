<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('assets/datatables/dataTables.bootstrap.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-4">
                <h2 style="margin-top:0px">Data Transaksi</h2>
            </div>
            <div class="col-md-4 text-center">
                <div style="margin-top: 4px"  id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
            <div class="col-md-4 text-right">
                <?php echo anchor(site_url('transaksi/create'), 'Create', 'class="btn btn-primary"'); ?>
		<?php echo anchor(site_url('transaksi/excel'), 'Excel', 'class="btn btn-primary"'); ?>
	    </div>
        </div>
        <table class="table table-bordered table-striped" id="mytable">
            <thead>
                <tr>
                    <th width="80px">No</th>
		    <th>Id Konsumen</th>
		    <th>Id User</th>
		    <th>Sales</th>
		    <th>Depot</th>
			<th>No Faktur</th>
		    <th>Tanggal Transaksi</th>
		    <th>Kelas A</th>
		    <th>Harga A</th>
		    <th>Kelas B</th>
		    <th>Harga B</th>
		    <th>Kelas C</th>
		    <th>Harga C</th>
		    <th>Kelas D</th>
		    <th>Harga D</th>
		    <th>Kelas Spr</th>
		    <th>Harga Spr</th>
		    <th>Kelas Ist</th>
		    <th>Harga Ist</th>
		    <th>Action</th>
                </tr>
            </thead>
	    <tbody>
            <?php
            $start = 0;
            foreach ($transaksi_data as $transaksi)
            {
                ?>
                <tr>
		    <td><?php echo ++$start ?></td>
		    <td><?php echo $transaksi->id_konsumen ?></td>
		    <td><?php echo $transaksi->id_user ?></td>
		    <td><?php echo $transaksi->sales ?></td>
		    <td><?php echo $transaksi->depot ?></td>
			<td><?php echo $transaksi->no_faktur ?></td>
		    <td><?php echo $transaksi->tanggal_transaksi ?></td>
		    <td><?php echo $transaksi->kelas_a ?></td>
		    <td><?php echo $transaksi->harga_a ?></td>
		    <td><?php echo $transaksi->kelas_b ?></td>
		    <td><?php echo $transaksi->harga_b ?></td>
		    <td><?php echo $transaksi->kelas_c ?></td>
		    <td><?php echo $transaksi->harga_c ?></td>
		    <td><?php echo $transaksi->kelas_d ?></td>
		    <td><?php echo $transaksi->harga_d ?></td>
		    <td><?php echo $transaksi->kelas_spr ?></td>
		    <td><?php echo $transaksi->harga_spr ?></td>
		    <td><?php echo $transaksi->kelas_ist ?></td>
		    <td><?php echo $transaksi->harga_ist ?></td>
		    
		    <td style="text-align:center" width="200px">
			<?php 
			echo anchor(site_url('transaksi/read/'.$transaksi->id_transaksi),'Read'); 
			echo ' | '; 
			echo anchor(site_url('transaksi/update/'.$transaksi->id_transaksi),'Update'); 
			echo ' | '; 
			echo anchor(site_url('transaksi/delete/'.$transaksi->id_transaksi),'Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
			?>
		    </td>
	        </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
        <script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/datatables/jquery.dataTables.js') ?>"></script>
        <script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js') ?>"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $("#mytable").dataTable();
            });
        </script>
    </body>
</html>