

<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Data Transaksi <?php echo $button ?></h2>
		<?php echo form_error(); ?>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="bigint">Nama Konsumen : <?php echo form_error('id_konsumen') ?></label>
			<div class="alert alert-success fade in"><?php echo $konsumen[0]->nama_konsumen; ?></div>
            <input type="hidden" class="form-control" name="id_konsumen" id="id_konsumen" placeholder="Id Konsumen" value="<?php echo $konsumen[0]->id_konsumen; ?>" />
        </div>
	    <div class="form-group">
            <label for="bigint">Nama Korsal <?php echo form_error('id_user') ?></label>
			<div class="alert alert-success fade in"><?php echo $konsumen[0]->nama_user; ?></div>
            <input type="hidden" class="form-control" name="id_user" id="id_user" placeholder="Id User" value="<?php echo $konsumen[0]->id_user; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Sales <?php echo form_error('sales') ?></label>
            <input type="text" class="form-control" name="sales" id="sales" placeholder="Sales" value="<?php echo $sales; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Depot <?php echo form_error('depot') ?></label>
			<select name="depot" id="depot" class="form-control">
			<?php 
				if(isset($data_depot)){
					$data_depot = json_decode(json_encode($data_depot),TRUE);   
					$group ="";
					$x=1;
					$max = count($data_depot);
					foreach($data_depot as $item){
						if(substr($item['agen'],0,1) =="L" && $group==""){
							$group =  ' <optgroup label="DEPOT Agen L">';
							echo $group;
						}else if(substr($item['agen'],0,1) != "L" && $group !="" ){
							echo "</optgroup>";
							$group =  ' <optgroup label="DEPOT Agen Non L">';
							echo $group;
							$group = "";
						}else if($x==$max){
							echo "</optgroup>";
						}
						echo "<option value='".$item['nama_depot']." (".$item['kode'].")'  ".$this->custom_library->cekCombo($item['nama_depot']." (".$item['kode'].")",$depot)."> ".$item['nama_depot']." (".$item['kode'].")</option> ";;
						$x++;
					}
				}
				
			?>
			</select>
        </div>
		<div class="form-group">
			<label for="jenis_transaksi">No Faktur <?php echo form_error('no_faktur') ?></label>
            <input type="text" class="form-control" name="no_faktur" id="no_faktur" placeholder="No Faktur" value="<?php echo $no_faktur; ?>" />
		</div>
	    <div class="form-group input-append date" id="datePicker">
            <label for="tanggal_transaksi">Tanggal Transaksi <?php echo form_error('tanggal_transaksi') ?></label>
            <input type="text" class="form-control" name="tanggal_transaksi" id="tanggal_transaksi" placeholder="Tanggal Transaksi" value="<?php echo $tanggal_transaksi; ?>" />
			<span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
        </div>
		<div class="form-group">
            <label for="jenis_transaksi">Jenis Transaksi <?php echo form_error('jenis_transaksi') ?></label>
            <?php
				$options = $this->custom_library->GetJenisTransaksi();
				echo form_dropdown('jenis_transaksi', $options,$jenis_transaksi,'class="form-control"');
			?>
        </div>
		
		<div class="form-group">
            <label for="enum">Jenis Order <?php echo form_error('jenis_order') ?></label>
			<?php
				//$options = $this->custom_library->GetJenisOrder();
				//echo form_dropdown('jenis_order', $options,$jenis_order,'class="form-control"');
				echo form_checkbox('jenis_order[]', 'Domba', TRUE) ; echo "Domba";
				echo form_checkbox('jenis_order[]', 'Sapi'); echo "Sapi";
				echo form_checkbox('jenis_order[]', '1/7'); echo "1/7 Sapi";
			?>
        </div>
	    <div class="form-group  col-xs-6">
            <label for="int">Kelas A <?php echo form_error('kelas_a') ?></label>
            <input type="text" class="form-control" name="kelas_a" id="kelas_a" placeholder="Kelas A" value="<?php echo $kelas_a; ?>" />
        </div>
	    <div class="form-group  col-xs-6">
            <label for="double">Harga A <?php echo form_error('harga_a') ?></label>
            <input type="text" class="form-control" name="harga_a" id="harga_a" placeholder="Harga A" value="<?php echo $harga_a; ?>" />
        </div>
	    <div class="form-group  col-xs-6">
            <label for="int">Kelas B <?php echo form_error('kelas_b') ?></label>
            <input type="text" class="form-control" name="kelas_b" id="kelas_b" placeholder="Kelas B" value="<?php echo $kelas_b; ?>" />
        </div>
	    <div class="form-group  col-xs-6">
            <label for="double">Harga B <?php echo form_error('harga_b') ?></label>
            <input type="text" class="form-control" name="harga_b" id="harga_b" placeholder="Harga B" value="<?php echo $harga_b; ?>" />
        </div>
	    <div class="form-group  col-xs-6">
            <label for="int">Kelas C <?php echo form_error('kelas_c') ?></label>
            <input type="text" class="form-control" name="kelas_c" id="kelas_c" placeholder="Kelas C" value="<?php echo $kelas_c; ?>" />
        </div>
	    <div class="form-group  col-xs-6">
            <label for="double">Harga C <?php echo form_error('harga_c') ?></label>
            <input type="text" class="form-control" name="harga_c" id="harga_c" placeholder="Harga C" value="<?php echo $harga_c; ?>" />
        </div>
	    <div class="form-group  col-xs-6">
            <label for="int">Kelas D <?php echo form_error('kelas_d') ?></label>
            <input type="text" class="form-control" name="kelas_d" id="kelas_d" placeholder="Kelas D" value="<?php echo $kelas_d; ?>" />
        </div>
	    <div class="form-group  col-xs-6">
            <label for="double">Harga D <?php echo form_error('harga_d') ?></label>
            <input type="text" class="form-control" name="harga_d" id="harga_d" placeholder="Harga D" value="<?php echo $harga_d; ?>" />
        </div>
	    <div class="form-group  col-xs-6">
            <label for="int">Kelas Spr <?php echo form_error('kelas_spr') ?></label>
            <input type="text" class="form-control" name="kelas_spr" id="kelas_spr" placeholder="Kelas Spr" value="<?php echo $kelas_spr; ?>" />
        </div>
	    <div class="form-group  col-xs-6">
            <label for="double">Harga Spr <?php echo form_error('harga_spr') ?></label>
            <input type="text" class="form-control" name="harga_spr" id="harga_spr" placeholder="Harga Spr" value="<?php echo $harga_spr; ?>" />
        </div>
	    <div class="form-group  col-xs-6">
            <label for="int">Kelas Ist <?php echo form_error('kelas_ist') ?></label>
            <input type="text" class="form-control" name="kelas_ist" id="kelas_ist" placeholder="Kelas Ist" value="<?php echo $kelas_ist; ?>" />
        </div>
	    <div class="form-group  col-xs-6">
            <label for="double">Harga Ist <?php echo form_error('harga_ist') ?></label>
            <input type="text" class="form-control" name="harga_ist" id="harga_ist" placeholder="Harga Ist" value="<?php echo $harga_ist; ?>" />
        </div>
	    <div class="form-group">
            <label for="double">Lain Lain <?php echo form_error('lain_lain') ?></label>
            <input type="text" class="form-control" name="lain_lain" id="lain_lain" placeholder="Lain Lain" value="<?php echo $lain_lain; ?>" />
        </div>
	    <div class="form-group">
            <label for="double">Uang Muka <?php echo form_error('uang_muka') ?></label>
            <input type="text" class="form-control" name="uang_muka" id="uang_muka" placeholder="Uang Muka" value="<?php echo $uang_muka; ?>" />
        </div>
	    <div class="form-group input-append date" id="datePicker2">
            <label for="date">Tgl Kirim <?php echo form_error('tgl_kirim') ?></label>
            <input type="text" class="form-control" name="tgl_kirim" id="tgl_kirim" placeholder="Tgl Kirim" value="<?php echo $tgl_kirim; ?>" />
			<span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
        </div>
	    <div class="form-group">
            <label for="varchar">Alamat Kirim <?php echo form_error('alamat_kirim') ?></label>
			<input type="hidden" value="<?php echo $konsumen[0]->alamat_konsumen; ?>" name="alamat_kirim_temp" id="alamat_kirim_temp">
            <input type="text" class="form-control" name="alamat_kirim" id="alamat_kirim" placeholder="Alamat Kirim" value="<?php if(isset($alamat_kirim) && $alamat_kirim !=""){echo $alamat_kirim;}else{ echo $konsumen[0]->alamat_konsumen;} ?>" />
			
        </div>
	    <div class="form-group">
            <label for="keterangan">Keterangan <?php echo form_error('keterangan') ?></label>
            <textarea class="form-control" rows="3" name="keterangan" id="keterangan" placeholder="Keterangan"><?php echo $keterangan; ?></textarea>
        </div>
	    <input type="hidden" name="id_transaksi" value="<?php echo $id_transaksi; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('kunjungan/index/'.$konsumen[0]->id_konsumen) ?>" class="btn btn-default">Cancel</a>
	</form>
    </body>
</html>

<script language="javascript">
$("#depot").change(function(){
	var depotName = $("#depot option:selected").val()
	var KodeArea = depotName.substr((depotName.length)-5);
	KodeArea = KodeArea.replace(")","")
	KodeArea = KodeArea.replace("(","")
	
	$("#no_faktur").val(KodeArea+" ");
	$("#no_faktur").focus();
	
});

$('#datePicker')
	.datepicker({
		autoclose: true,
		format: 'yyyy-mm-dd'
	}).on('changeDate', function(e) {	
});
$('#datePicker2')
	.datepicker({
		autoclose: true,
		format: 'yyyy-mm-dd'
	}).on('changeDate', function(e) {	
});
$(function(){
	// Set up the number formatting.
	//$('#harga_a').number( true, 0);
//	$('#harga_b').number( true, 0);
	//$('#harga_c').number( true, 0);
	//$('#harga_d').number( true, 0);
	//$('#harga_spr').number( true, 0);
	//$('#harga_ist').number( true, 0);
});
</script>