<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Detail Data transaksi</h2>
        <table class="table">
	    <tr><td >Konsumen</td><td colspan="3"><?php echo $id_konsumen." : ".$nama_konsumen; ?></td></tr>
	    <tr><td >Korsal</td><td colspan="3"><?php echo $id_user." : ".$nama_user; ?></td></tr>
	    <tr><td >Sales</td><td colspan="3"><?php echo $sales; ?></td></tr>
	    <tr><td >Depot</td><td colspan="3"><?php echo $depot; ?></td></tr>
	    <tr><td >Tanggal Transaksi</td><td colspan="3" ><?php echo nice_date($tanggal_transaksi,"d-M-Y"); ?></td></tr>
		<tr><td >No Fakturi</td><td colspan="3" ><?php echo $no_faktur; ?></td></tr>
		<tr><td >Jenis Order</td><td colspan="3" ><?php echo $jenis_order; ?></td></tr>
	    <tr><td>Kelas A</td><td><?php echo $kelas_a; ?></td><td>Harga A</td><td><?php echo $harga_a; ?></td></tr>
	    <tr><td>Kelas B</td><td><?php echo $kelas_b; ?></td><td>Harga B</td><td><?php echo $harga_b; ?></td></tr>
	    <tr><td>Kelas C</td><td><?php echo $kelas_c; ?></td><td>Harga C</td><td><?php echo $harga_c; ?></td></tr>
	    <tr><td>Kelas D</td><td><?php echo $kelas_d; ?></td><td>Harga D</td><td><?php echo $harga_d; ?></td></tr>
	    <tr><td>Kelas Spr</td><td><?php echo $kelas_spr; ?></td><td>Harga Spr</td><td><?php echo $harga_spr; ?></td></tr>
	    <tr><td>Kelas Ist</td><td ><?php echo $kelas_ist; ?></td><td>Harga Ist</td><td><?php echo $harga_ist; ?></td></tr>
	    <tr><td>Lain Lain</td><td  colspan="3"><?php echo $lain_lain; ?></td></tr>
		<tr><td>Uang Muka</td><td  colspan="3"><?php echo $uang_muka; ?></td></tr>
	    <tr><td>Tgl Kirim</td><td  colspan="3"><?php echo nice_date($tgl_kirim,"d-M-y"); ?></td></tr>
	    <tr><td>Alamat Kirim</td><td  colspan="3"><?php echo $alamat_kirim; ?></td></tr>
	    <tr><td>Keterangan</td><td  colspan="3"><?php echo $keterangan; ?></td></tr>	
	    <tr><td></td><td><a href="<?php echo site_url('transaksi/show_all') ?>" class="btn btn-default">Cancel</a></td></tr>
	</table>
	<h2>Kronologis Kunjungan</h2>
	<table class="table table-bordered table-striped" id="mytable">
            <thead>
                <tr>
					<th width="80px">No</th>
					<th>Tgl Kunjungan</th>
					<th>Status</th>
					<th>Jenis Order</th>
					<th>Keterangan</th>
                </tr>
            </thead>
	    <tbody>
            <?php
            $start = 0;
            foreach ($kunjungan_data as $kunjungan)
            {
                ?>
                <tr>
		    <td><?php echo ++$start ?></td>
		    <td><?php echo nice_date($kunjungan->tanggal_kunjungan,"d-M-Y") ?></td>
		    <td><?php echo $kunjungan->status ?></td>
			<td><?php echo $kunjungan->jenis_order ?></td>
		    <td><?php echo $kunjungan->keterangan ?></td>
	        </tr>
                <?php
				
            }
            ?>
            </tbody>
        </table>
        </body>
</html>