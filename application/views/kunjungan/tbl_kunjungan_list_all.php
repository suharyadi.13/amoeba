<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('assets/datatables/dataTables.bootstrap.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-12 text-center" >
                <div style="margin-top: 4px"  id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
		</div>	
		<div class="row">
			<div class="col-md-6">
                <h2 style="margin-top:0px">Data Kunjungan</h2>
            </div>
            <div class="col-md-6 text-right">
			<?php
				echo anchor(site_url('kunjungan/excel/'), 'Excel', 'class="btn btn-primary"'); 
			?>
			</div>			
	    </div>
        <table class="table table-bordered table-striped" id="mytable">
            <thead>
                <tr>
					<th width="80px">No</th>
					<th>Korsal</th>
					<th>Konsumen</th>
					<th>Tgl Kunjungan</th>
					<th>Status</th>
					<th>Jenis Order</th>
					<th>Keterangan</th>
					<th>Action</th>
                </tr>
            </thead>
	    <tbody>
            </tbody>
        </table>
	
        <script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/datatables/jquery.dataTables.js') ?>"></script>
        <script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js') ?>"></script>
        <script type="text/javascript">
			var dataSet = [<?php echo $dataset; ?>]
            $("#mytable").dataTable({
					  data: dataSet
					}
				);
        </script>
    </body>
</html>