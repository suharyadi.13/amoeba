

<!doctype html>

<html>

    <head>

        <title>harviacode.com - codeigniter crud generator</title>

        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>

        <link rel="stylesheet" href="<?php echo base_url('assets/datatables/dataTables.bootstrap.css') ?>"/>

        <style>

            body{

                padding: 15px;

            }

        </style>

    </head>

    <body>

        <div class="row" style="margin-bottom: 10px">

            <div class="col-md-12 text-center" >

                <div style="margin-top: 4px"  id="message">

                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>

                </div>

            </div>

			<?php

			if(isset($id_konsumen)){

			?>

			<div class="col-md-6 text-left">

			       <table class="table">
			       	<tr>
			       		<td>
			       			<a href="<?php echo site_url('konsumen/delete/'.$konsumen['id_konsumen']) ?>" class="btn btn-warning float-right" onclick="javasciprt: return confirm('Anda yakin akan menghapus data ?')" title="delete konsumen"><i class="icon_close_alt2"></i></a>
			       			<a href="<?php echo site_url('konsumen/update/'.$konsumen['id_konsumen']) ?>" class="btn btn-success float-right"  title="edit konsumen"><i class="fa fa-edit"></i></a>
			       		</td>
			       		<td>Data Konsumen</td>
			       	</tr>
					<tr>
						<td>Korsal</td>
						<td><?php echo $konsumen['id_user']." : ".$konsumen['nama_user']; ?>
						</td>
					</tr>

					<tr><td>Nama Konsumen</td><td><?php echo $konsumen['nama_konsumen']; ?></td></tr>

					<tr><td>Telp Konsumen</td><td><?php echo $konsumen['telp_konsumen']; ?></td></tr>

					<tr><td>Alamat Konsumen</td><td><?php echo $konsumen['alamat_konsumen']; ?></td></tr>

					<tr><td>Jenis Pelanggan</td><td><?php echo $konsumen['jenis_pelanggan']; ?></td></tr>

					<tr><td>Status Update</td><td><?php echo $konsumen['status']; ?></td></tr>

					<?php if ($konsumen['status']=="MQL"){?>

					<tr><td>Rencana Tgl Transaksi</td><td><?php echo nice_date($konsumen['tanggal_survey'],"d-M-Y"); ?></td></tr>

					<tr><td>Rencana Kelas</td><td><?php echo $konsumen['rencana_kelas']; ?></td></tr>

					<?php } ?>

					<tr><td>Jenis Order</td><td><?php echo $konsumen['jenis_order']; ?></td></tr>

					<tr><td>Jenis Transaksi</td><td><?php echo $konsumen['jenis_transaksi']; ?></td></tr>

				</table>

			</div>

			<div class="col-md-6 text-left">

					<?php

					if(count($transaksi)>0){

						$transaksi = (array)$transaksi[0];

					?>

					<table class="table">

					<tr><td colspan="2" align="center">

					<div class="col-xs-8">Data Transaksi</div>

					<div class="col-xs-4">

					<?php

					// tampilkan edit data jika 

					if($this->session->userdata("level") <= 2){

					?>

					<a href="<?php echo site_url('transaksi/update/'.$transaksi['id_transaksi']); ?>" class="btn btn-success" title="edit data"><i class="icon_check_alt2"></i></a>

					<a href="<?php echo site_url('transaksi/delete/'.$transaksi['id_konsumen']."/".$transaksi['id_transaksi']) ?>" class="btn btn-warning" onclick="javasciprt: return confirm('Anda yakin akan menghapus data ?')" title="delete data transaksi"><i class="icon_close_alt2"></i></a>

					<?php 

					}

					?>

					</div>

					</td></tr>

					<tr><td>Nama Sales</td><td><?php echo $transaksi['sales']; ?></td></tr>

					<tr><td>Depot</td><td><?php echo $transaksi['depot']; ?></td></tr>

					<tr><td>No Faktur</td><td><?php echo $transaksi['no_faktur']; ?></td></tr>

					<tr><td>Tgl Transaksi</td><td><?php echo nice_date($transaksi['tanggal_transaksi'],"d-M-Y"); ?></td></tr>

					<tr><td>Jenis Order</td><td><?php echo $transaksi['jenis_order']; ?></td></tr>

					<tr><td colspan="2" align="center">Pembelian</td></tr>

					<tr><td colspan="2" align="center">

						<?

						if($transaksi['kelas_ist'] >0){

							echo "<div class='col-xs-6'>Kelas IST : ".$transaksi['kelas_ist']." </div><div class='col-xs-6'> Harga : ".$transaksi['harga_ist']."</div>";

						}

						if($transaksi['kelas_spr'] >0){

							echo "<div class='col-xs-6'>Kelas SPR : ".$transaksi['kelas_spr']." </div><div class='col-xs-6'> Harga : ".$transaksi['harga_spr']."</div>";

						}

						if($transaksi['kelas_a'] >0){

							echo "<div class='col-xs-6'>Kelas A : ".$transaksi['kelas_a']." </div><div class='col-xs-6'> Harga : ".$transaksi['harga_a']."</div>";

						}

						if($transaksi['kelas_b'] >0){

							echo "<div class='col-xs-6'>Kelas B : ".$transaksi['kelas_b']." </div><div class='col-xs-6'> Harga : ".$transaksi['harga_b']."</div>";

						}

						if($transaksi['kelas_c'] >0){

							echo "<div class='col-xs-6'>Kelas C : ".$transaksi['kelas_c']." </div><div class='col-xs-6'> Harga : ".$transaksi['harga_c']."</div>";

						}

						if($transaksi['kelas_d'] >0){

							echo "<div class='col-xs-6'>Kelas D : ".$transaksi['kelas_d']." </div><div class='col-xs-6'> Harga : ".$transaksi['harga_d']."</div>";

						}

						

						?>

						

					</td></tr>

					<tr><td>Uang Muka</td><td><?php echo $transaksi['uang_muka']; ?></td></tr>

					</table>

					<?php

					}else if($konsumen['status']=="DEAL"){

						echo "<div class='alert alert-block alert-danger fade in'> Konsumen telah deal, segera masukan data transaksi";

						echo "<p> <a href='". site_url("/transaksi/create/".$id_konsumen)."' class='btn btn-danger'>Klik disini untuk input Transaksi</a> </p></div>";

					}

					?>

			</div>

		</div>	

		<div class="row">

			<div class="col-md-6">

                <h2 style="margin-top:0px">Data Kunjungan</h2>

            </div>

            <div class="col-md-6 text-right">

			<?php

				echo anchor(site_url('kunjungan/create/'.$id_konsumen), 'Create', 'class="btn btn-primary"'); 

				echo anchor(site_url('kunjungan/excel/'.$id_konsumen), 'Excel', 'class="btn btn-primary"'); 

			?>

			</div>			

	    </div>

		<?php

			}

		?>

        <table class="table table-bordered table-striped" id="mytable">

            <thead>

                <tr>

					<th width="80px">No</th>

					<th>Tgl Kunjungan</th>

					<th>Status</th>

					<th>Keterangan</th>

					<th>Action</th>

                </tr>

            </thead>

	    <tbody>

            <?php

            $start = 0;

            foreach ($kunjungan_data as $kunjungan)

            {

                ?>

                <tr>

		    <td><?php echo ++$start ?></td>

		    <td><?php echo nice_date($kunjungan->tanggal_kunjungan,"d-M-Y") ?></td>

		    <td><?php echo $kunjungan->status ?></td>

		    <td><?php echo $kunjungan->keterangan ?></td>

		    <td style="text-align:center" width="50px">

			<?php 

			//echo anchor(site_url('kunjungan/read/'.$kunjungan->id_kunjungan),'View'); 

			//echo ' | '; 

		//	echo anchor(site_url('kunjungan/update/'.$kunjungan->id_kunjungan),'Update'); 

			//echo ' | '; 

			echo anchor(site_url('kunjungan/delete/'.$id_konsumen.'/'.$kunjungan->id_kunjungan),'<i class="icon_close_alt2"></i>','class="btn btn-warning" onclick="javasciprt: return confirm(\'Anda yakin akan menghapus data ?\')" title="delete data"'); 

			?>

		    </td>

	        </tr>

                <?php

				

            }

            ?>

            </tbody>

        </table>

		 

        <script src="<?php echo base_url('assets/datatables/jquery.dataTables.js') ?>"></script>

        <script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js') ?>"></script>

        <script type="text/javascript">

            $(document).ready(function () {

                $("#mytable").dataTable();

            });

        </script>

    </body>

</html>