<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css" />
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" />

<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
<style type="text/css">
/**
 * Override feedback icon position
 * See http://formvalidation.io/examples/adjusting-feedback-icon-position/
 */
#eventForm .form-control-feedback {
    top: 0;
    right: -15px;
}
</style>

<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Data Kunjungan <?php echo $button ?></h2>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group input-append date" id="datePicker">
            <label for="tanggal_kunjungan">Tanggal Kunjungan <?php echo form_error('tanggal_kunjungan') ?></label>
            <input type="text" class="form-control" name="tanggal_kunjungan" id="tanggal_kunjungan" placeholder="Format : 2016-08-16" value="<?php echo $tanggal_kunjungan; ?>" />
			<span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
        </div>
	    <div class="form-group">
            <label for="status">Status <?php echo form_error('status') ?></label>
            <!-- <input type="text" class="form-control" name="status" id="status" placeholder="Status" value="<?php echo $status; ?>" /> -->
			<?php
				$options = $this->custom_library->getStatus();
				echo form_dropdown('status', $options, $status,'id="status" class="form-control"');
			?>
        </div>
		<div class="form-group">
            <label for="enum">Jenis Transaksi <?php echo form_error('jenis_transaksi') ?></label>
			<?php
				$options = $this->custom_library->GetJenisTransaksi();
				echo form_dropdown('jenis_transaksi', $options,$jenis_transaksi,'class="form-control"');
			?>
        </div>
		<div class="form-group">
            <label for="enum">Jenis Order <?php echo form_error('jenis_order') ?></label>
			<?php
				echo form_checkbox('jenis_order[]', 'Domba', TRUE) ; echo "Domba";
				echo form_checkbox('jenis_order[]', 'Sapi'); echo "Sapi";
				echo form_checkbox('jenis_order[]', '1/7'); echo "1/7 Sapi";
			?>
        </div>
		<div id="custom" style="display:none;">
		<div class="form-group" >
			<label for="tanggal_survey">Renana Tanggal Transaksi <?php echo form_error('tanggal_survey') ?></label>
			<input type="text" class="form-control" name="tanggal_survey" id="tanggal_survey" placeholder="2008-0-01" value="<?php echo $tanggal_survey; ?>" />
		</div>
		<div class="form-group" >
			<label for="rencana_kelas"> Rencana Kelas<?php echo form_error('rencana_kelas') ?></label>
			<input type="text" class="form-control" name="rencana_kelas" id="rencana_kelas" placeholder="I,S,A,B,,C,D" value="<?php echo $rencana_kelas; ?>" />
		</div>
		</div>
	    <div class="form-group">
            <label for="keterangan">Keterangan <?php echo form_error('keterangan') ?></label>
            <textarea class="form-control" rows="3" name="keterangan" id="keterangan" placeholder="Keterangan"><?php echo $keterangan; ?></textarea>
        </div>
	    <input type="hidden" name="id_kunjungan" value="<?php echo $id_kunjungan; ?>" /> 
		<input type="hidden" class="form-control" name="id_konsumen" id="id_konsumen" placeholder="Id Konsumen" value="<?php echo $id_konsumen; ?>" />
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('kunjungan/index/'.$id_konsumen) ?>" class="btn btn-default">Cancel</a>
	</form>
	<script language="javascript">
		function cekStatus(){
			if($("#status").val() =="MQL"){
				$("#custom").slideDown("slow");
			}else{
				$("#custom").slideUp("slow");
			}
		}
		$("#status").change(function(){
			cekStatus()
		})
		$(document).ready(function(){
			cekStatus()
		})
		
		 $('#datePicker')
			.datepicker({
				autoclose: true,
				format: 'yyyy-mm-dd'
			}).on('changeDate', function(e) {
				
			});
		
	</script>
    </body>
</html>
