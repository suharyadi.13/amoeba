<!DOCTYPE html>
<html lang="en">

	<?php
	$this->load->view("template/head_file");
	?>
    <body>

  <!-- container section start -->
  <section id="container" class="">
      <!--header start-->
      <header class="header dark-bg">
            <div class="toggle-nav">
                <div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom"><i class="icon_menu"></i></div>
            </div>
            <!--logo start-->
            <a href="index.html" class="logo"><span class="lite">SIM</span> PEMASARAN
              <span class="text-danger">
			<?php
			if($this->session->userdata("username")!="" ){
				echo " : ".$this->session->userdata("username");
			}
			?>
      </span>
			</a>
            <!--logo end-->
            <div class="top-nav notification-row">                
                <!-- notificatoin dropdown start-->
                <ul class="nav pull-right top-menu">
                    
                    <!-- user login dropdown start-->
                    <li class="dropdown">
                        <a data-toggle="dropdown" href="<?php echo site_url()."/login/logout/" ?>">
                            <span class="username">Logout <?php echo $this->session->userdata('nama_user') ?></span>
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu extended logout">
                            <div class="log-arrow-up"></div>
                            <li class="eborder-top">
                                <a href="<?php echo site_url()."/datauser/update/".$this->session->userdata('id_user') ?>"><i class="icon_profile"></i> My Profile</a>
                            </li>
                            <li>
                                <a href="<?php echo site_url()."/login/logout/" ?>"><i class="icon_key_alt"></i> Log Out</a>
                            </li>
                        </ul>
                    </li>
                    <!-- user login dropdown end -->
                </ul>
                <!-- notificatoin dropdown end-->
            </div>
      </header>      
      <!--header end-->

      <!--sidebar start-->
      <?php
      $this->load->view("template/sidemenu");
      ?>

      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
              <div class="row">
				<div class="col-lg-12">
					<h3 class="page-header"><i class="fa fa-list-alt"></i>SISTEM INFORMASI PEMASARAN</h3>
					<ol class="breadcrumb">
						<li><i class="fa fa-home"></i><a href="<?php echo base_url() ?>">Home</a></li>
						<li><i class="fa fa-desktop"></i><?php echo $this->uri->segment(1)  ?></li>
					</ol>
				</div>
			</div>
                  
				  
