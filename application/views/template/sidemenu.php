<aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu">                
                  <li class="active">
                      <a class="" href="<?php echo site_url()."/dashboard/" ?>">
                          <i class="icon_house_alt"></i>
                          <span>Dashboard</span>
                      </a>
                  </li>
                  <li class="sub-menu">
                      <a href="javascript:;" class="">
                          <i class="icon_document_alt"></i>
                          <span>Master Data</span>
                          <span class="menu-arrow arrow_carrot-right"></span>
                      </a>
                      <ul class="sub">
                          <li><a class="" href="<?php echo site_url()."/product/" ?>">
                              <i class="icon_tag_alt"></i>
                              <span>Product</span>
                          </a></li>
                          <li><a class="" href="<?php echo site_url()."/area/" ?>">
                              <i class="icon_tag_alt"></i>
                              <span>Area</span>
                          </a></li>   
                          <li><a class="" href="<?php echo site_url()."/segmen/" ?>">
                              <i class="icon_tag_alt"></i>
                              <span>Segmen</span>
                          </a></li>                          
                      </ul>
                  </li> 
                  <?php
                  if($this->session->userdata("level") < 2){
                  ?>

                  <li class="">
                      <a class="" href="<?php echo site_url()."/Target/" ?>">
                          <i class="icon_desktop"></i>
                          <span>Target Sales</span>
                      </a>
                  </li>
                  <?php
                  }
                  ?>
                  <li class="sub-menu">
                      <a href="javascript:;" class="">
                          <i class="icon_document_alt"></i>
                          <span>Data Konsumen</span>
                          <span class="menu-arrow arrow_carrot-right"></span>
                      </a>
                      <ul class="sub">
                          <li><a class="" href="<?php echo site_url()."/konsumen/" ?>">List Konsumen</a></li>
                          <li><a class="" href="<?php echo site_url()."/konsumen/form_konsumen" ?>">Tambah Konsumen</a></li>                          
                      </ul>
                  </li>
                  
                  <li class="sub-menu">
                      <a href="javascript:;" class="">
                          <i class="icon_document_alt"></i>
                          <span>Data Prospect</span>
                          <span class="menu-arrow arrow_carrot-right"></span>
                      </a>
                      <ul class="sub">
                          <li >
                              <a class="" href="<?php echo site_url()."/prospect/" ?>"> 
                                  <span>Seluruh Prospect</span>
                              </a>
                          </li>
                          <li><a class="" href="<?php echo site_url()."/transaksi/show_all" ?>">Report Prospect</a></li>
                          <li><a class="" href="<?php echo site_url()."/transaksi/show_all_PO" ?>">Report Deal</a></li>                          
                      </ul>
                  </li>
                 <?php
                    if($this->session->userdata("level") < 2){
                    ?>
                    
                  <li class="sub-menu">
                      <a href="javascript:;" class="">
                          <i class="icon_piechart"></i>
                          <span>Chart</span>
                          <span class="menu-arrow arrow_carrot-right"></span>
                      </a>
                      <ul class="sub">
                          <li><a class="" href="<?php echo site_url()."/dashboard/chart" ?>">Chart Transaksi Sales</a></li>
                          <li><a class="" href="<?php echo site_url()."/dashboard/ProgresTransaksi" ?>">Progres Transaksi</a></li>                          
                      </ul>
                  </li>   
                  <li >
                      <a href="<?php echo site_url()."/datauser" ?>" class="">
                          <i class="icon_desktop"></i>
                          <span>Daftar Sales</span>
                      </a>
                  </li>
                 <?php
                    }
                  ?>
                <li >
                      <a class="" href="<?php echo site_url()."/datauser/update/".$this->session->userdata("id_user") ?>">
                          <i class="icon_profile"></i>
                          <span>My Profile</span>
                      </a>
                  </li> 
                  <li >
                      <a class="" href="<?php echo site_url()."/login/logout" ?>">
                          <i class="icon_key_alt"></i>
                          <span>Logout <?php echo $this->session->userdata('nama_user') ?></span>
                      </a>
                  </li>
              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->