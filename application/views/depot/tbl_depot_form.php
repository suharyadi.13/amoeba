<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Tbl_depot <?php echo $button ?></h2>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="varchar">Agen <?php echo form_error('agen') ?></label>
            <input type="text" class="form-control" name="agen" id="agen" placeholder="Agen" value="<?php echo $agen; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Kode <?php echo form_error('kode') ?></label>
            <input type="text" class="form-control" name="kode" id="kode" placeholder="Kode" value="<?php echo $kode; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Nama Depot <?php echo form_error('nama_depot') ?></label>
            <input type="text" class="form-control" name="nama_depot" id="nama_depot" placeholder="Nama Depot" value="<?php echo $nama_depot; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Alamat Depot <?php echo form_error('alamat_depot') ?></label>
            <input type="text" class="form-control" name="alamat_depot" id="alamat_depot" placeholder="Alamat Depot" value="<?php echo $alamat_depot; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Hotline Depot <?php echo form_error('hotline_depot') ?></label>
            <input type="text" class="form-control" name="hotline_depot" id="hotline_depot" placeholder="Hotline Depot" value="<?php echo $hotline_depot; ?>" />
        </div>
	    <input type="hidden" name="no" value="<?php echo $no; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('depot') ?>" class="btn btn-default">Cancel</a>
	</form>
    </body>
</html>