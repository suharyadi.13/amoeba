<!-- load library jquery dan highcharts -->
<script src="<?php echo base_url();?>assets/highcharts/jquery.js"></script>
<script src="<?php echo base_url();?>assets/highcharts/highcharts.js"></script>
<!-- end load library -->
 
<?php
    /* Mengambil query report*/
    foreach($report as $result){
        $bulan[] = $result['id_user2']; //ambil bulan
        $valuePL[] = (float) ( $result['PL_I_DB'] +  $result['PL_S_DB'] +  $result['PL_A_DB'] +  $result['PL_B_DB'] +  $result['PL_C_DB'] +  $result['PL_D_DB'] );  // PL DOMBA
		$valuePS[] = (float) ( $result['PS_I_DB'] +  $result['PS_S_DB'] +  $result['PS_A_DB'] +  $result['PS_B_DB'] +  $result['PS_C_DB'] +  $result['PS_D_DB'] );  // PS DOMBA
    }
    /* end mengambil query*/
     
?>
 
<!-- Load chart dengan menggunakan ID -->
<div id="report"></div>
<!-- END load chart -->
 
<!-- Script untuk memanggil library Highcharts -->
<script type="text/javascript">
$(function () {
    $('#report').highcharts({
        chart: {
            type: 'column',
            margin: 75,
            options3d: {
                enabled: true,
                alpha: 10,
                beta: 25,
                depth: 70
            }
        },
        title: {
            text: 'Report PL & PS',
            style: {
                    fontSize: '18px',
                    fontFamily: 'Verdana, sans-serif'
            }
        },
        subtitle: {
           text: 'Penjualan',
           style: {
                    fontSize: '15px',
                    fontFamily: 'Verdana, sans-serif'
            }
        },
        plotOptions: {
            column: {
                depth: 25
            }
        },
        credits: {
            enabled: false
        },
        xAxis: {
            categories:  <?php echo json_encode($bulan);?>
        },
        exporting: { 
            enabled: true 
        },
        yAxis: {
            title: {
                text: 'Jumlah'
            },
        },
        tooltip: {
             formatter: function() {
                 return 'Julmlah  '+this.series.name+' <b>' + this.x + '</b> adalah  : <b>' + Highcharts.numberFormat(this.y,0) + '</b> Ekor'
             }
          },
        series: [{
            name: 'PL',
            data: <?php echo json_encode($valuePL);?>,
            shadow : true,
            dataLabels: {
                enabled: true,
                color: '#045396',
                align: 'center',
                formatter: function() {
                     return Highcharts.numberFormat(this.y, 0);
                }, // one decimal
                y: 0, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
		{
            name: 'PS',
            data: <?php echo json_encode($valuePS);?>,
            shadow : true,
            dataLabels: {
                enabled: true,
                color: '#045396',
                align: 'center',
                formatter: function() {
                     return Highcharts.numberFormat(this.y, 0);
                }, // one decimal
                y: 0, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }
		]
    });
});
        </script