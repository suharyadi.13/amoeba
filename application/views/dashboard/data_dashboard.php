<div class="row ">
<div class="panel">
	<header class="panel-heading no-border">
	  <?php echo $title; 
	 //echo " : ".$this->session->userdata('nama_user');
	  ?>
	</header>
	<div class="panel col-md-6">
		
	</div>	
</div>
</div>
<div class="row tile_count">
  <?php
  foreach($data_total as $item){
  ?>
  <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count" style="overflow: visible;">
    <span class="count_top"><i class="fa fa-user"></i> <?php echo $item['title'] ?></span>
    <div class="count" ><?php echo number_format((int)$item['total']) ?></div>
    <span class="count_bottom"><i class="green">total </i> Data Tahun ini</span>
  </div>
  <?php
  }
  ?> 
</div>
<div class="row tile_count">
  <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <div class="info-box brown-bg">
      <i class="fa fa-shopping-cart"></i>
      <div class="count" id="totalAchv">0</div>
      <div class="title">TOTAL ACHIEVEMEN</div>
    </div>
    <!--/.info-box-->
  </div>
  <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
    <div class="info-box green-bg">
      <i class="fa fa-cubes"></i>
      <div class="count" id="totalReal">0</div>
      <div class="title">TOTAL OMZET</div>
    </div>
    <!--/.info-box-->
  </div>
  <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
    <div class="info-box blue-bg">
      <i class="fa fa-table"></i> 
      <div class="count" id="totalReal">
      <div class="bt
      n btn-lg">
        <i class="fa fa-arrow"></i>
      </div> </div>
      <div class="title">Tampilkan Data</div>
    </div>
    <!--/.info-box-->
  </div>
  </div>

  <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
          <li class="nav-item">
            <a class="nav-link" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Pencapaian Bulanan</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Pencapaian TW</a>
          </li> 
        </ul>
        <div class="tab-content" id="myTabContent">
          <div class="tab-pane fade" id="home" role="tabpanel" aria-labelledby="home-tab">
           <section class="panel" style="overflow: auto;">
            <header class="panel-heading">
              Pencapaian Sales bulanan
            </header>  
              <table class="table" style="text-align: center;">
                <thead>
                <tr>
                  <td rowspan="3">Nama User</td>
                  <td colspan="24"  style="text-align: center;">Jumlah</td>
                </tr>
                <tr>
                  <?php
                  foreach ($bulan as $key => $value) {
                      echo '<td colspan="2"  style="text-align: center;">'.$value.'</td>'; 
                  }
                  ?>
                </tr>
                <tr>
                  <?php
                  foreach ($bulan as $key => $value) {
                      echo '<td>Renc</td>'; 
                      echo '<td>Targ</td>'; 
                  }
                  ?>
                </tr> 
                </thead>
                <tbody> 
              <?php

              foreach ($pencapaian as $key => $value) {
                  echo '<tr>';
                  echo '<td>'.$value['nama_user'].'</td>';
                  foreach ($bulan as $item) {
                      echo '<td>'.number_format($value['target_'.$item]).'</td>';
                      echo '<td>'.number_format($value['real_'.$item]).'</td>';
                  }
                  echo '</tr>';
              }
              ?>
                </tbody>
              </table>
            </section>
          </div>
          <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
            <section class="panel" style="overflow: auto;">
            <header class="panel-heading">
              Pencapaian Sales TW
            </header>  
              <table class="table">
                <thead>
                <tr>
                  <td rowspan="3">Nama User</td>
                  <td colspan="24"  style="text-align: center;">Jumlah</td>
                </tr>
                <tr  style="text-align: center;">
                  <?php
                  foreach ($tw as $key => $value) {
                      echo '<td colspan="2">'.$value.'</td>'; 
                  }
                  ?>
                </tr>
                <tr  style="text-align: center;">
                  <?php
                  foreach ($tw as $key => $value) {
                      echo '<td>Renc</td>'; 
                      echo '<td>Targ</td>'; 
                  }
                  ?>
                </tr> 
                </thead>
                <tbody> 
              <?php

              foreach ($pencapaian_tw as $key => $value) {
                  echo '<tr>';
                  echo '<td>'.$value['nama_user'].'</td>';
                  foreach ($tw as $item) {
                      echo '<td>'.number_format($value['target_'.$item]).'</td>';
                      echo '<td>'.number_format($value['real_'.$item]).'</td>';
                  }
                  echo '</tr>';
              }
              ?>
                </tbody>
              </table>
            </section>
          </div>
          
        </div>
      </div>
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="dashboard_graph"> 
          <div class="row x_title">
            <div class="col-md-6">
              <h3>Grafik Pencapaian vs Target<small>,Tahun  <?php echo date("Y")?></small></h3> 
            </div>
            <div class="col-md-6">
              <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                <span><?php echo date("Y-m-d H:i:s") ?></span>  
              </div>
            </div>
          </div>
          <div class="col-md-4 col-sm-4 col-xs-12" style="font-size: 12px">
            <section class="panel">
            <header class="panel-heading">
              MONTHLY DEAL SALES IN RP
            </header>
            <table class="table" >
              <thead>
                <tr>
                  <th>Bulan</th>
                  <th>Tahun</th>
                  <th>Target Tahun</th>
                  <th>Achievment</th>
                </tr>
              </thead>
              <tbody>
              
              <?php 
              $bulan = $this->custom_library->ShortNamaBulan;
              $data =  array();
              $totalReal = 0;
              $totalRencana= 0;
              $totalAchv = 0;
              foreach ($grafik_total_omzet as $key => $value) {
                $data[] = array_slice($value,1);
              }
              //var_dump($data[0]);die;
              if(count($data)>0){
                foreach ($bulan as $key => $value) {  
                  if($data[0][$value]>0 && $data[1][$value]){
                    $achv = round(($data[0][$value] / $data[1][$value] *100),2);
                  }else{
                    $achv = 0;
                  }
                  echo '<tr>
                    <td>'.$value.'</td>
                    <td>'.number_format($data[0][$value]).'</td>
                    <td>'.number_format($data[1][$value]).'</td>
                    <td>'.$achv.' %</td>
                  </tr> ';
                  $totalReal += (float)$data[0][$value];
                  $totalRencana += (float)$data[1][$value];

                  $totalAchv = round(($totalReal/$totalRencana*100),2);
                }
                echo '<tr>
                    <td>TOTAL</td>
                    <td>'.number_format($totalReal).'</td>
                    <td>'.number_format($totalRencana).'</td>
                    <td>'.$totalAchv.' %</td>
                  </tr> ';
              }
              ?> 
              </tbody>
            </table>
          </section>
          </div>
          <div class="col-md-8 col-sm-8 col-xs-12">
            <div id="placeholder33" style="height: 260px; display: none" class="demo-placeholder"></div>
            <canvas id="canvas" width="400" height="250px"></canvas>
          </div>
          
          <div class="clearfix"></div>
        </div>
      </div>
  </div>
  <br>
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="dashboard_graph"> 
        <div class="row x_title">
          <div class="col-md-6">
            <h3>Grafik Cummulative (Pencapaian vs Target)<small>,Tahun  <?php echo date("Y")?></small></h3> 
          </div>
          <div class="col-md-6">
            <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
              <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
              <span><?php echo date("Y-m-d H:i:s") ?></span>  
            </div>
          </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12" style="font-size: 12px">
          <section class="panel">
          <header class="panel-heading">
            MONTHLY CUMMULATIVE DEAL SALES IN RP
          </header>
          <table class="table">
            <thead>
              <tr>
                <th>Bulan</th>
                <th>Tahun</th>
                <th>Target Tahun</th>
                <th>Achievment</th>
              </tr>
            </thead>
            <tbody>
            
            <?php 
            $bulan = $this->custom_library->ShortNamaBulan;
            $data =  array();
            $dataCumulative = array();
            foreach ($grafik_total_omzet as $key => $value) {
              $data[] = array_slice($value,1);
            }

            //var_dump($data[0]);die;
            if(count($data)>0){
              $dataRealisasi = $data[0];
              $dataTarget = $data[1];
              $cumulativeTarget = "";
              $cumulativeReal = 0;
              $cumulativeAch = 0;

              foreach ($bulan as $key => $value) {  
                if($cumulativeTarget==""){
                  $cumulativeTarget =  (float)$dataTarget[$value] ;
                  $cumulativeReal = (float)$dataRealisasi[$value];
                  if($data[0][$value]>0 && $data[1][$value]){
                    $achv = round(($data[0][$value] / $data[1][$value] *100),2);
                  }else{
                    $achv = 0;
                  }
                  echo '<tr>
                    <td>'.$value.'</td>
                    <td>'.number_format($data[0][$value]).'</td>
                    <td>'.number_format($data[1][$value]).'</td>
                    <td>'.$achv.' %</td>
                  </tr> ';
                }else{
                   
                  $cumulativeTarget += (float)$data[1][$value];
                  $cumulativeReal += (float)$data[0][$value];
                  $cumulativeAch = round(($cumulativeReal/$cumulativeTarget*100),2);
                  echo '<tr>
                    <td>'.$value.'</td>
                    <td>'.number_format($cumulativeReal).'</td>
                    <td>'.number_format($cumulativeTarget).'</td>
                    <td>'.$cumulativeAch.' %</td>
                  </tr> ';
                }
                $dataCumulative['Realisai'][]=$cumulativeReal;
                $dataCumulative['Target'][]=$cumulativeTarget;
                
              }
            }
            ?> 
            </tbody>
          </table>
        </section>
        </div>
        <div class="col-md-8 col-sm-8 col-xs-12">
          <div id="placeholder33" style="height: 260px; display: none" class="demo-placeholder"></div>
          <canvas id="canvasCummulative" width="400" height="250px"></canvas>
        </div>
        
        <div class="clearfix"></div>
      </div>
    </div>
  </div>

  <br>

  <div class="row">

    <div class="col-md-6 portlets">
    <div class="panel panel-default">
      <div class="panel-heading">
        <div class="pull-left"><h4>Top Sales <small>(Total Omzet sales /tahun)</small></h4></div>
        <div class="widget-icons pull-right"> 
          <a href="#" class="wclose"><i class="icon_desktop"></i></a>
        </div>
        <div class="clearfix"></div>
      </div>
      <div class="panel-body"> 
          <?php 
          foreach ($sales_total_omzet as $key => $value) { 
          ?>
          <div class="widget_summary" style="border-bottom: solid 1px #F2F5F7;">
            <div class="w_left w_25">
              <span><?php echo strtoupper($value['nama_user']) ?> </span>
            </div> 
            <div class="w_right w_70">
              <span>: Rp. <?php echo number_format($value['total']) ?></span>
            </div>
            <div class="clearfix"></div>
          </div>
          <?php
          }
          ?>
        </div>
      </div>
    </div>
    <div class="col-md-6 portlets">
    <div class="panel panel-default">
      <div class="panel-heading">
        <div class="pull-left"><h4>Top Produk <small>(Produk Terlaris)</small></h4></div>
        <div class="widget-icons pull-right"> 
          <a href="#" class="wclose"><i class="icon_desktop"></i></a>
        </div>
        <div class="clearfix"></div>
      </div>
      <div class="panel-body"> 
        <div class="padd">
        
          <?php 
          foreach ($sales_top_product as $key => $value) { 
          ?>
          <div class="widget_summary" style="border-bottom: solid 1px #F2F5F7;">
            <div class="w_left w_25">
              <span><?php echo strtoupper($value['nama_product']) ?> </span>
            </div> 
            <div class="w_right w_70">
              <span>: Rp. <?php echo number_format($value['total']) ?></span>
            </div>
            <div class="clearfix"></div>
          </div>
          <?php
          }
          ?>
        </div>
        <div class="widget-foot">
          <!-- Footer goes here -->
        </div>
      </div>
    </div>

  </div>
   
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel tile fixed_height_500 overflow_hidden">
        <div class="x_title">
          <div class="col-md-6">
            <h2>Grafik Pencapaian vs Target Sales<small>,Tahun  <?php echo date("Y")?></small></h2>
          </div>
          <div class="col-md-6">
            <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
              <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
              <span><?php echo date("Y-m-d H:i:s") ?></span>  
            </div>
          </div> 
          <div class="clearfix"></div>
        </div>
        <div class="x_content" style="height:100% ">  
           <canvas id="canvasSales" width="600px" height="200px" ></canvas>
        </div>
      </div>
    </div> 
  </div>




<script>
$("#totalAchv").html("<?php echo $totalAchv ?> %");
$("#totalReal").html("Rp. <?php echo number_format($totalReal) ?>");
var MONTHS = ["Jan","Feb","Mar","Apr","Mei","Jun","Jul","Agus","Sept","Okt","Nov","Des"];
var DataGraph = Array();
var LabelGraph = Array();
 <?php 
 $x=0;
  
 ?>  
var config = {
      type: 'line',
      data: {
        labels: MONTHS,
        datasets: [
        <?php
        $x=0;
        $bgColor = array('rgb(255, 99, 132)','rgb(75, 192, 192)','blue','orange','black');
        $borderColor = array('rgb(54, 162, 235)','rgb(255, 99, 132)','orange','black','orange');
        foreach ($grafik_total_omzet as $key => $value) {
          $data = implode(",",array_slice($value,1));
          //var_dump(array_slice($value,1));die();
          echo "
          {
            label: '".$value['title']."',
            backgroundColor: '".$bgColor[$x]."',
            borderColor:'".$borderColor[$x]."',
            data: [".$data."],
            fill: false,
          },";
          $x++;
        } ?>]
      },
      options: {
        responsive: true,
        title: {
          display: true,
          text: 'Total Pencapaian vs Rencana'
        },
        scales: {
          yAxes: [{
            ticks: { 
              suggestedMin: 10,
              suggestedMax: 50
            }
          }]
        }
      }
    };

  var configCummulative = {
      type: 'line',
      data: {
        labels: MONTHS,
        datasets: [
        <?php
        $x=0;
        $bgColor = array('rgb(255, 99, 132)','rgb(75, 192, 192)','blue','orange','black');
        $borderColor = array('rgb(54, 162, 235)','rgb(255, 99, 132)','orange','black','orange');
        foreach ($dataCumulative as $key => $value) {
          $data = implode(",",$value);
          //var_dump(array_slice($value,1));die();
          echo "
          {
            label: '".$key."',
            backgroundColor: '".$bgColor[$x]."',
            borderColor:'".$borderColor[$x]."',
            data: [".$data."],
            fill: false,
          },";
          $x++;
        } ?>]
      },
      options: {
        responsive: true,
        title: {
          display: true,
          text: 'Total Cumulative Pencapaian vs Rencana'
        },
        scales: {
          yAxes: [{
            ticks: { 
              suggestedMin: 10, 
              suggestedMax: 50
            }
          }]
        }
      }
    };

    /*window.onload = function() {
      var ctx = document.getElementById('canvas').getContext('2d');
      window.myLine = new Chart(ctx, config);
    };*/
  </script>

  <script>
    <?php 
    foreach ($sales_total_omzet as $key => $value) {
      $namaSales[] =  $value['nama_user'];
      $targetSales[] = (float)$value['target'];
      $realSales[] = (float)$value['total'];


    }?>
    var color = Chart.helpers.color;
    var horizontalBarChartData = {
      labels: ["<?php echo implode('","',$namaSales) ?>"],
      datasets: [{
        label: 'Realisasi',
        backgroundColor: color(window.chartColors.blue).alpha(0.5).rgbString(),
        borderColor: window.chartColors.red,
        borderWidth: 1,
        data: [<?php echo implode(',',$realSales) ?>]
      },
      {
        label: 'Target',
        backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
        borderColor: window.chartColors.blue,
        borderWidth: 1,
        data: [<?php echo implode(',',$targetSales) ?>]
      }] 
    };
     

    window.onload = function() {
      var ctx = document.getElementById('canvasSales').getContext('2d');
      window.myBar = new Chart(ctx, {
        type: 'bar',
        data: horizontalBarChartData,
        options: {
          responsive: true,
          legend: {
            position: 'top',
          },
          title: {
            display: true,
            text: 'Pencapaian vs Rencana Sales'
          }
        }
      });
      var ctx2 = document.getElementById('canvas').getContext('2d');
      window.myLine = new Chart(ctx2, config);
      var ctx3 = document.getElementById('canvasCummulative').getContext('2d');
      window.myLine = new Chart(ctx3, configCummulative);
    };

  $(document).ready(function(){
    $('#myTab a').on('click', function (e) {
      e.preventDefault()
      $(this).tab('show')
    })
    $('#myTab a[href="#home"]').tab('show') // Select tab by name      
  })    
  </script>