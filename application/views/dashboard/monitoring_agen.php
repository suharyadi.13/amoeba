<style>
#th_jtrans,#td_jtrans{display:none; }
</style>

<!doctype html>
<html>
    <head>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('assets/datatables/dataTables.bootstrap.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-4">
                <h2 style="margin-top:0px">Data Pergerakan Agen</h2>
            </div>
            <div class="col-md-4 text-center">
                <div style="margin-top: 4px"  id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
            <div class="col-md-4 text-right">
                <?php //echo anchor(site_url('datauser/create'), 'Create', 'class="btn btn-primary"'); ?>
				<?php echo anchor(site_url('datauser/excel'), 'Excel', 'class="btn btn-primary"'); ?>
	    </div>
        </div>
        <table class="table table-bordered table-striped" id="mytable">
            <thead>
                <tr>
					<th width="80px" id="col_id">No</th>
					<th>Nama User</th>
					<th>ID User</th>
					<th>Jml Konsumen</th>
					<th>Jml Kunjungan Todate</th>
					<th>Jml Kunjungan Today</th>
					<th>Jml Konsumen Deal</th>
					<th>Belum Input Transaksi</th>
					
				</tr>
            </thead>
	    <tbody>
            </tbody>
        </table>
        <script src="<?php echo base_url('assets/datatables/jquery.dataTables.js') ?>"></script>
        <script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js') ?>"></script>
        <script type="text/javascript">
			var dataSet = [<?php echo $dataset; ?>]
			
            $(document).ready(function () {
                $("#mytable").dataTable({
					  data: dataSet
					}
				);
            });
        </script>
    </body>
</html>