<div class="row">
  <div class="col-md-12">
    <section class="panel">
      <header class="panel-heading">
        Filter Data Transaksi
      </header>
      <div class="panel-body">
        <form role="form" method="POST" action="<?php echo site_url('dashboard/chart') ?>">
          <div class="form-group">
            <label for="exampleInput">Pilih Tanggal</label>
            <div class="row">
              <div class="col-md-4"><input type="date" class="form-control" id="start" name="start" placeholder="Pilih tanggal Awal" value="<?php echo $start ?>"></div>
              <div class="col-md-1 text-center">s.d </div>
              <div class="col-md-4"><input type="date" class="form-control" id="end" name="end" placeholder="Pilih tanggal Awal" value="<?php echo $end ?>"></div>
             </div>
          </div>
           
          <button type="submit" class="btn btn-primary">Tampilkan Data</button>
        </form>

      </div>
    </section>
  </div>
  <div class="col-md-12">
    <section class="panel">
              <header class="panel-heading tab-bg-primary ">
                <ul class="nav nav-tabs">
                  <?php 
                  $act = "active";
                  foreach ($user as $key => $value) {
                    echo '
                    <li class="'.$act.'">
                      <a data-toggle="tab" href="#'.$value->id_user.'">'.$value->nama_user.'</a>
                    </li>';
                    $act = "";
                   } ?>
                </ul>
              </header>
              <div class="panel-body">
                <div class="tab-content">
                   <?php 
                    $act = "active";
                    foreach ($user as $key => $value) {
                      echo '
                      <div id="'.$value->id_user.'" class="tab-pane '.$act.'">';
                    
                      loadGrafik($value,$grafik_total_omzet_alluser[$value->id_user],$bulan);
                       //die();
                      echo $value->nama_user.'</div>
                     ';
                     $act = "";
                    } ?>
                   
                </div>
              </div>
            </section>
  </div>
</div>
<?php
function loadGrafik($user,$grafik_total_omzet_alluser,$bulan){
$datagrafik[] = $grafik_total_omzet_alluser;
foreach ($datagrafik as $id => $grafik_total_omzet) {
?>
<div class="row">
<div class="col-md-12 col-sm-12 col-xs-12" portlets> 
  <div class="panel panel-default">
    <div class="panel-heading">
      <div class="pull-left">Grafik Pencapaian Sales : <?php echo $user->nama_user ?></div>
      <div class="widget-icons pull-right"> 
        <a href="#" class="wclose"><i class="icon_desktop"></i></a>
      </div>
      <div class="clearfix"></div>
    </div>
    <div class="panel-body">  
      <div class="col-lg-12">
          <section class="panel">
            <div class="dashboard_graph"> 
          <div class="row x_title">
            <div class="col-md-6">
              <h3>Grafik Pencapaian vs Target<small>,Tahun  <?php echo date("Y")?></small></h3> 
            </div>
            <div class="col-md-6">
              <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                <span><?php echo date("Y-m-d H:i:s") ?></span>  
              </div>
            </div>
          </div>
          <div class="col-md-4 col-sm-4 col-xs-12" style="font-size: 12px">
            <section class="panel">
            <header class="panel-heading">
              MONTHLY DEAL SALES IN RP
            </header>
            <table class="table">
              <thead>
                <tr>
                  <th>Bulan</th>
                  <th>Tahun</th>
                  <th>Target Tahun</th>
                  <th>Achievment</th>
                </tr>
              </thead>
              <tbody>
              
              <?php  
              $data =  array();
              $totalReal = 0;
              $totalRencana= 0;
              $totalAchv = 0;
              foreach ($grafik_total_omzet as $key => $value) {
                $data[] = array_slice($value,1);
              }
              //var_dump($data[0]);die;
              if(count($data)>0){
                foreach ($bulan as $key => $value) {  
                  if($data[0][$value]>0 && $data[1][$value]){
                    $achv = round(($data[0][$value] / $data[1][$value] *100),2);
                  }else{
                    $achv = 0;
                  }
                  echo '<tr>
                    <td>'.$value.'</td>
                    <td>'.number_format($data[0][$value]).'</td>
                    <td>'.number_format($data[1][$value]).'</td>
                    <td>'.$achv.' %</td>
                  </tr> ';
                  $totalReal += (float)$data[0][$value];
                  $totalRencana += (float)$data[1][$value];
                  if($totalReal>0 && $totalRencana >0){
                    $totalAchv = round(($totalReal/$totalRencana*100),2);
                  }else{
                    $totalAchv = 0;
                  }
                }
                echo '<tr>
                    <td>TOTAL</td>
                    <td>'.number_format($totalReal).'</td>
                    <td>'.number_format($totalRencana).'</td>
                    <td>'.$totalAchv.' %</td>
                  </tr> ';
              }
              ?> 
              </tbody>
            </table>
          </section>
          </div>
          <div class="col-md-8 col-sm-8 col-xs-12">
            <div id="placeholder33" style="height: 260px; display: none" class="demo-placeholder"></div>
            <canvas id="canvas<?php echo $user->id_user ?>" width="400" height="250px"></canvas>
          </div>
          
          <div class="clearfix"></div>
        </div>
          </section>
          </div>
      </div>
      <br>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="dashboard_graph"> 
            <div class="row x_title">
              <div class="col-md-6">
                <h3>Grafik Cummulative (Pencapaian vs Target)<small>,Tahun  <?php echo date("Y")?></small></h3> 
              </div>
              <div class="col-md-6">
                <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
                  <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                  <span><?php echo date("Y-m-d H:i:s") ?></span>  
                </div>
              </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12" style="font-size: 12px">
              <section class="panel">
              <header class="panel-heading">
                MONTHLY CUMMULATIVE DEAL <?php echo strtoupper($user->nama_user) ?> IN RP
              </header>
              <table class="table">
                <thead>
                  <tr>
                    <th>Bulan</th>
                    <th>Tahun</th>
                    <th>Target Tahun</th>
                    <th>Achievment</th>
                  </tr>
                </thead>
                <tbody>
                
                <?php  
                $data =  array();
                $dataCumulative = array();
                foreach ($grafik_total_omzet as $key => $value) {
                  $data[] = array_slice($value,1);
                }

                //var_dump($data[0]);die;
                if(count($data)>0){
                  $dataRealisasi = $data[0];
                  $dataTarget = $data[1];
                  $cumulativeTarget = "";
                  $cumulativeReal = 0;
                  $cumulativeAch = 0;

                  foreach ($bulan as $key => $value) {  
                    if($cumulativeTarget==""){
                      $cumulativeTarget =  (float)$dataTarget[$value] ;
                      $cumulativeReal = (float)$dataRealisasi[$value];
                      if($data[0][$value]>0 && $data[1][$value]){
                        $achv = round(($data[0][$value] / $data[1][$value] *100),2);
                      }else{
                        $achv = 0;
                      }
                      echo '<tr>
                        <td>'.$value.'</td>
                        <td>'.number_format($data[0][$value]).'</td>
                        <td>'.number_format($data[1][$value]).'</td>
                        <td>'.$achv.' %</td>
                      </tr> ';
                    }else{
                       
                      $cumulativeTarget += (float)$data[1][$value];
                      $cumulativeReal += (float)$data[0][$value];
                      $cumulativeAch = round(($cumulativeReal/$cumulativeTarget*100),2);
                      echo '<tr>
                        <td>'.$value.'</td>
                        <td>'.number_format($cumulativeReal).'</td>
                        <td>'.number_format($cumulativeTarget).'</td>
                        <td>'.$cumulativeAch.' %</td>
                      </tr> ';
                    }
                    $dataCumulative['Realisai'][]=$cumulativeReal;
                    $dataCumulative['Target'][]=$cumulativeTarget;
                    
                  }
                }
                ?> 
                </tbody>
              </table>
            </section>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-12">
              <div id="placeholder33" style="height: 260px; display: none" class="demo-placeholder"></div>
              <canvas id="canvasCummulative<?php echo $user->id_user ?>" width="400" height="250px"></canvas>
            </div>
            
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
 



<script>
$("#totalAchv").html("<?php echo $totalAchv ?> %");
$("#totalReal").html("Rp. <?php echo number_format($totalReal) ?>");
var MONTHS = ["Jan","Feb","Mar","Apr","Mei","Jun","Jul","Agus","Sept","Okt","Nov","Des"];
var DataGraph = Array();
var LabelGraph = Array();
 <?php 
 $x=0;
  
 ?>  
var config<?php echo $user->id_user ?> = {
      type: 'line',
      data: {
        labels: MONTHS,
        datasets: [
        <?php
        $x=0;
        $bgColor = array('rgb(255, 99, 132)','rgb(75, 192, 192)','blue','orange','black');
        $borderColor = array('rgb(54, 162, 235)','rgb(255, 99, 132)','orange','black','orange');
        foreach ($grafik_total_omzet as $key => $value) {
          $data = implode(",",array_slice($value,1));
          //var_dump(array_slice($value,1));die();
          echo "
          {
            label: '".$value['title']."',
            backgroundColor: '".$bgColor[$x]."',
            borderColor:'".$borderColor[$x]."',
            data: [".$data."],
            fill: false,
          },";
          $x++;
        } ?>]
      },
      options: {
        responsive: true,
        title: {
          display: true,
          text: 'Total Pencapaian vs Rencana'
        },
        scales: {
          yAxes: [{
            ticks: { 
              suggestedMin: 10,
              suggestedMax: 50
            }
          }]
        }
      }
    };

  var configCummulative<?php echo $user->id_user ?> = {
      type: 'line',
      data: {
        labels: MONTHS,
        datasets: [
        <?php
        $x=0;
        $bgColor = array('rgb(255, 99, 132)','rgb(75, 192, 192)','blue','orange','black');
        $borderColor = array('rgb(54, 162, 235)','rgb(255, 99, 132)','orange','black','orange');
        foreach ($dataCumulative as $key => $value) {
          $data = implode(",",$value);
          //var_dump(array_slice($value,1));die();
          echo "
          {
            label: '".$key."',
            backgroundColor: '".$bgColor[$x]."',
            borderColor:'".$borderColor[$x]."',
            data: [".$data."],
            fill: false,
          },";
          $x++;
        } ?>]
      },
      options: {
        responsive: true,
        title: {
          display: true,
          text: 'Total Cumulative Pencapaian vs Rencana'
        },
        scales: {
          yAxes: [{
            ticks: { 
              suggestedMin: 10, 
              suggestedMax: 50
            }
          }]
        }
      }
    };

     
         
  </script>

<?php 
}
}//end function
?>
<script type="text/javascript">
window.onload = function() { 
    <?php
    foreach ($user as $key => $value) {
    ?>
      var ctx2 = document.getElementById('canvas<?php echo $value->id_user ?>').getContext('2d');
      window.myLine = new Chart(ctx2, config<?php echo $value->id_user ?>);
      var ctx3 = document.getElementById('canvasCummulative<?php echo $value->id_user ?>').getContext('2d');
      window.myLine = new Chart(ctx3, configCummulative<?php echo $value->id_user ?>);
    <?php } ?>
    };   
</script>
<!--  
<div class="row">
<div class="col-md-12 col-sm-12 col-xs-12" portlets> 
	<div class="panel panel-default">
	  <div class="panel-heading">
	    <div class="pull-left">Grafik Pencapaian Sales</div>
	    <div class="widget-icons pull-right"> 
	      <a href="#" class="wclose"><i class="icon_desktop"></i></a>
	    </div>
	    <div class="clearfix"></div>
	  </div>
	  <div class="panel-body">
	  	<?php
	  	foreach ($sales_total_omzet as $key => $value) { 
	  	?>
	  	<div class="col-lg-6">
			<section class="panel">
				<header class="panel-heading">
					Pencapaian <?php echo $value['nama_user'] ?>
				</header>
				<div class="panel-body text-center">
					<canvas id="canvasSales<?php echo $value['id_user'] ?>" height="300" width="500" style="width: 500px; height: 300px;"></canvas>
				</div>
			</section>
	    </div>
	<?php } ?> 
	</div>
</div>

<script>
    <?php 
    foreach ($sales_total_omzet as $key => $value) { 
    ?>
    var color = Chart.helpers.color;
    var horizontalBarChartData<?php echo $value['id_user'] ?> = {
      labels: ["<?php echo $value['nama_user'] ?>"],
      datasets: [{
        label: 'Realisasi',
        backgroundColor: color(window.chartColors.blue).alpha(0.5).rgbString(),
        borderColor: window.chartColors.red,
        borderWidth: 1,
        data: [<?php echo (float)$value['target'] ?>]
      },
      {
        label: 'Target',
        backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
        borderColor: window.chartColors.blue,
        borderWidth: 1,
        data: [<?php echo (float)$value['total'] ?>]
      }] 
    };

<?php } ?>
     

    window.onload = function() {
    <?php 
    foreach ($sales_total_omzet as $key => $value) { 
    ?>
      var ctx<?php echo $value['id_user'] ?> = document.getElementById('canvasSales<?php echo $value['id_user'] ?>').getContext('2d');
      window.myBar = new Chart(ctx<?php echo $value['id_user'] ?>, {
        type: 'bar',
        data: horizontalBarChartData<?php echo $value['id_user'] ?>,
        options: {
          responsive: true,
          legend: {
            position: 'top',
          },
          title: {
            display: true,
            text: 'Pencapaian vs Rencana Sales'
          }
        }
      });  
    <?php } ?>      
     };  
  </script> -->