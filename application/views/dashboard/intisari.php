<style>
table .tbl_foot{
	background-color:#8f8f94!important;
	color:white;
}
.agenL{
	background-color:#ccc!important;
	color:black;
}
</style>
<div class="row">
	<div class="col-sm-12">
	<section class="panel">
	  <header class="panel-heading">
		  Intisari Transaksi Agen Didepot
	  </header>
	  <table class="table table-striped">
		  <thead>
		  <tr>
			  <th  class="tbl_foot"  rowspan="2" style="width:5%;vertical-align:middle;text-align:center;">Agen</th>
			  <th  class="tbl_foot" rowspan="2"  style="width:30%;vertical-align:middle;text-align:left;">Nama Depot</th>
			  <th  class="tbl_foot" colspan="6" style="width:40%;vertical-align:middle;text-align:center;">Kelas Hewan</th>
		</tr>
		<tr>
			  <th class="tbl_foot">IST</th>
			  <th  class="tbl_foot">SPR</th>
			  <th  class="tbl_foot">A</th>
			  <th  class="tbl_foot">B</th>
			  <th  class="tbl_foot">C</th>
			  <th  class="tbl_foot">D</th>
			  <th class="tbl_foot" >JUMLAH</th>
		  </tr>
		  </thead>
		  <tbody>
		  <?php
		  $total_ist=0;
		  $total_spr=0;
		  $total_a=0;
		  $total_b=0;
		  $total_c=0;
		  $total_d=0;
		  $total_jumlah=0;
		  foreach($data as $item){
			 if(substr($item['agen'],0,1) =="L" ){
				 $class = "class='agenL'";
			 }else{
				 $class = "";
			 } 
			echo '<tr>
				<td '.$class.'>'.$item['agen'].'</td>
				<td '.$class.'>'.$item['nama_depot'].'</td>
				<td '.$class.'>'.(float)$item['jml_i'].'</td>
				<td '.$class.'>'.(float)$item['jml_s'].'</td>
				<td '.$class.'>'.(float)$item['jml_a'].'</td>
				<td '.$class.'>'.(float)$item['jml_b'].'</td>
				<td '.$class.'>'.(float)$item['jml_c'].'</td>
				<td '.$class.'>'.(float)$item['jml_d'].'</td>
				<td class="tbl_foot" >'.(float)$item['jumlah'].'</td>
			</tr>';
			$total_ist+=$item['jml_i'];
			$total_spr+=$item['jml_s'];
			$total_a+=$item['jml_a'];
			$total_b+=$item['jml_b'];
			$total_c+=$item['jml_c'];
			$total_d+=$item['jml_d'];
			$total_jumlah+=$item['jumlah'];
		}
		 echo ' 
		  <tr class="tbl_foot">
		  <td class="tbl_foot" colspan="2" >TOTAL</td>
		  <td class="tbl_foot" >'.$total_ist.'</td>
		  <td class="tbl_foot" >'.$total_spr.'</td>
		  <td class="tbl_foot" >'.$total_a.'</td>
		  <td class="tbl_foot" >'.$total_b.'</td>
		  <td class="tbl_foot" >'.$total_c.'</td>
		  <td class="tbl_foot" >'.$total_d.'</td>
		  <td class="tbl_foot" >'.$total_jumlah.'</td>
		  </tr>'
		 ?>
		  </tbody>
	  </table>
	</section>
	</div>
</div>