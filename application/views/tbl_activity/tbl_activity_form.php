<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Tbl_activity <?php echo $button ?></h2>
        <form action="<?php echo $action; ?>" method="post">
		<div class="form-group">
            <label for="varchar">id_user <?php echo form_error('id_user') ?></label>
            <input type="text" class="form-control" name="id_user" id="id_user" placeholder="id_user" value="<?php echo $id_user; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Last Page <?php echo form_error('last_page') ?></label>
            <input type="text" class="form-control" name="last_page" id="last_page" placeholder="Last Page" value="<?php echo $last_page; ?>" />
        </div>
	    <div class="form-group">
            <label for="date">Last Datetime <?php echo form_error('last_datetime') ?></label>
            <input type="text" class="form-control" name="last_datetime" id="last_datetime" placeholder="Last Datetime" value="<?php echo $last_datetime; ?>" />
        </div>
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('tbl_activity') ?>" class="btn btn-default">Cancel</a>
	</form>
    </body>
</html>