<!DOCTYPE html>
<html lang="en">

	<?php
	$this->load->view("head_file");
	?>
    <body>

  <!-- container section start -->
  <section id="container" class="">
      <!--header start-->
      <header class="header dark-bg">
            <div class="toggle-nav">
                <div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom"><i class="icon_menu"></i></div>
            </div>
            <!--logo start-->
            <a href="index.html" class="logo"><span class="lite">SIM</span> PEMASARAN
              <span class="text-danger">
			<?php
			if($this->session->userdata("username")!="" ){
				echo " : ".$this->session->userdata("username");
			}
			?>
      </span>
			</a>
            <!--logo end-->
            <div class="top-nav notification-row">                
                <!-- notificatoin dropdown start-->
                <ul class="nav pull-right top-menu">
                    
                    <!-- user login dropdown start-->
                    <li class="dropdown">
                        <a data-toggle="dropdown" href="<?php echo site_url()."/login/logout/" ?>">
                            <span class="username">Logout <?php echo $this->session->userdata('nama_user') ?></span>
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu extended logout">
                            <div class="log-arrow-up"></div>
                            <li class="eborder-top">
                                <a href="<?php echo site_url()."/datauser/update/".$this->session->userdata('id_user') ?>"><i class="icon_profile"></i> My Profile</a>
                            </li>
                            <li>
                                <a href="<?php echo site_url()."/login/logout/" ?>"><i class="icon_key_alt"></i> Log Out</a>
                            </li>
                        </ul>
                    </li>
                    <!-- user login dropdown end -->
                </ul>
                <!-- notificatoin dropdown end-->
            </div>
      </header>      
      <!--header end-->

      <!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu">                
                  <li class="active">
                      <a class="" href="<?php echo site_url()."/dashboard/" ?>">
                          <i class="icon_house_alt"></i>
                          <span>Dashboard</span>
                      </a>
                  </li>
                   
				  <?php
				  if($this->session->userdata("level") > 2){
				  ?>

				  <li class="">
                      <a class="" href="<?php echo site_url()."/dashboard/MonitoringAgen/" ?>">
                          <i class="icon_desktop"></i>
                          <span>Monitoring Agen</span>
                      </a>
                  </li>
				  <?php
				  }
				  ?>
				      <li class="sub-menu">
              <a href="javascript:;" class="">
                  <i class="icon_document_alt"></i>
                  <span>Data Konsumen</span>
                  <span class="menu-arrow arrow_carrot-right"></span>
              </a>
                  <ul class="sub">
                      <li><a class="" href="<?php echo site_url()."/konsumen/" ?>">List Konsumen</a></li>
    						  <li><a class="" href="<?php echo site_url()."/form_konsumen" ?>">Tambah Konsumen</a></li>                          
                  </ul>
              </li>
                  <li >
                      <a class="" href="<?php echo site_url()."/kunjungan/show_all" ?>">
                          <i class="icon_document"></i>
                          <span>Rekap Kunjungan</span>
                      </a>
                  </li>
				  <li >
                      <a class="" href="<?php echo site_url()."/transaksi/show_all" ?>">
                          <i class="icon_document"></i>
                          <span>Rekap Transaksi</span>
                      </a>
                  </li>
				  <?php
					  if($this->session->userdata("level") == 5){
					  ?>
					<li >
                      <a href="<?php echo site_url()."/stok_hewan" ?>" class="">
                          <i class="icon_document"></i>
                          <span>Stok Hewan</span>
                      </a>
                  </li>   
					<li >
                      <a href="<?php echo site_url()."/dashboard/intisari" ?>" class="">
                          <i class="icon_document"></i>
                          <span>Intisari</span>
                      </a>
                  </li>  
				  <?php
					}
					
					if($this->session->userdata("level") > 2){
					?>
					
				  <li class="sub-menu">
                      <a href="javascript:;" class="">
                          <i class="icon_piechart"></i>
                          <span>Chart</span>
                          <span class="menu-arrow arrow_carrot-right"></span>
                      </a>
                      <ul class="sub">
                          <li><a class="" href="<?php echo site_url()."/dashboard/chart" ?>">Chartz PL & PS</a></li>
						  <li><a class="" href="<?php echo site_url()."/dashboard/ProgresTransaksi" ?>">Progres Transaksi</a></li>                          
                      </ul>
                  </li>	  
				  <?php
					}
				  ?>
                  <li >
                      <a href="<?php echo site_url()."/datauser" ?>" class="">
                          <i class="icon_desktop"></i>
                          <span>Daftar Agen</span>
                      </a>
                  </li>
				 
				<li >
                      <a class="" href="<?php echo site_url()."/datauser/update/".$this->session->userdata("id_user") ?>">
                          <i class="icon_profile"></i>
                          <span>My Profile</span>
                      </a>
                  </li>	
                  <li >
                      <a class="" href="<?php echo site_url()."/login/logout" ?>">
                          <i class="icon_key_alt"></i>
                          <span>Logout <?php echo $this->session->userdata('nama_user') ?></span>
                      </a>
                  </li>
              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->

      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
            <div class="row">
      				<div class="col-lg-12">
      					<h3 class="page-header"><i class="fa fa-list-alt"></i>SISTEM INFORMASI PEMASARAN</h3>
      					<ol class="breadcrumb">
      						<li><i class="fa fa-home"></i><a href="<?php echo base_url() ?>">Home</a></li>
      						<li><i class="fa fa-desktop"></i><?php echo $this->uri->segment(1)  ?></li>
      					</ol>
      				</div>
      			</div>
            <div class="row">
                  
				  
