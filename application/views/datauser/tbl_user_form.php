<!doctype html>
<html>
    <head>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Data User <?php echo $button ?></h2>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="varchar">Username <?php echo form_error('username') ?></label>
            <?php  echo $username; ?>
        </div>
		<div class="form-group">
            <label for="int">Level <?php echo form_error('level') ?></label>
            <?php echo $level; ?>
        </div>
		<div class="form-group">
            <label for="varchar">Nama Lengkap <?php echo form_error('nama_user') ?></label>
            <input type="text" class="form-control" name="nama_user" id="nama_user" placeholder="Nama User" value="<?php echo $nama_user; ?>" />
        </div>
		<div class="form-group">
            <label for="varchar">No Telp <?php echo form_error('no_telp') ?></label>
            <input type="text" class="form-control" name="no_telp" id="no_telp" placeholder="No Telp" value="<?php echo $no_telp; ?>" />
        </div>
		<div class="form-group">
            <label for="varchar">Email <?php echo form_error('email') ?></label>
            <input class="form-control" type="email" name="email" required placeholder="Alamat Email anda" value="<?php echo $email; ?>" />
        </div>
		<div class="form-group">
            <label for="varchar">Jumlah Sales <?php echo form_error('jumlah_sales') ?></label>
            <input type="text" class="form-control" name="jumlah_sales" id="jumlah_sales" placeholder="Jumlah Sales dibawah anda" value="<?php echo $jumlah_sales; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Password <?php echo form_error('password') ?></label>
            <input type="text" class="form-control" name="password" id="password" placeholder="Password" value="<?php echo $password; ?>" />
        </div>
	    <input type="hidden" name="id_user" value="<?php echo $id_user; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 	    
	</form>
	
	 <!-- Modal -->
	 <!-- 
	 <a class="btn btn-success" data-toggle="modal" href="#myModal" id="loadModal">
		  Dialog
	  </a>
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
		  <div class="modal-content">
			  <div class="modal-header">
				  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				  <h4 class="modal-title">Modal Tittle</h4>
			  </div>
			  <div class="modal-body">

				  Body goes here...

			  </div>
			  <div class="modal-footer">
				  <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
				  <button class="btn btn-success" type="button">Save changes</button>
			  </div>
		  </div>
	  </div>
	</div>-->
	<!-- modal -->
	<script language="javascript">
	$("#loadModal").trigger("click")
	</script>
    </body>
</html>