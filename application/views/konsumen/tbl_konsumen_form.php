<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Data Konsumen <?php echo $button ?></h2>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="varchar">Sales <?php echo form_error('id_user') ?> </label>
			
			<select name="id_user" class="form-control">
			<?php 
				if(isset($user)){
					$user = json_decode(json_encode($user),TRUE);   
					foreach($user as $item){
						echo "<option value='".$item['id_user']."' ".$this->custom_library->cekCombo($item['id_user'],$id_user)."> ".$item['id_user']." - ".$item['nama_user']."</option> ";;
					}
				}else{
					echo "<option value='".$id_user."'> ".$id_user." - ".$item['nama_user']."</option> ";;
				}
			?>
			</select>
        </div>
	    <div class="form-group">
            <label for="varchar">Nama Konsumen <?php echo form_error('nama_konsumen') ?></label>
            <input type="text" class="form-control" name="nama_konsumen" id="nama_konsumen" placeholder="Nama Konsumen" value="<?php echo $nama_konsumen; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Telp Konsumen <?php echo form_error('telp_konsumen') ?></label>
            <input type="text" class="form-control" name="telp_konsumen" id="telp_konsumen" placeholder="Telp Konsumen" value="<?php echo $telp_konsumen; ?>" />
        </div>
        <div class="form-group">
            <label for="varchar">Email Konsumen <?php echo form_error('email_konsumen') ?></label>
            <input type="text" class="form-control" name="email_konsumen" id="email_konsumen" placeholder="email Konsumen" value="<?php echo $telp_konsumen; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Alamat Konsumen <?php echo form_error('alamat_konsumen') ?></label>
            <input type="text" class="form-control" name="alamat_konsumen" id="alamat_konsumen" placeholder="Alamat Konsumen" value="<?php echo $alamat_konsumen; ?>" />
        </div>		
        <!-- <div class="form-group">            
            <label for="varchar">Hubungan Dengan Sales <?php echo form_error('hubungan_dengan_sales') ?></label>            
            <input type="text" class="form-control" name="hubungan_dengan_sales" id="hubungan_dengan_sales" placeholder="Hubungan Dengan Sales" value="<?php echo $hubungan_dengan_sales; ?>" />        
        </div> -->
		<div class="form-group">
            <label for="enum">Jenis Pelanggan <?php echo form_error('jenis_pelanggan') ?></label>
			<?php
				$options = $this->custom_library->GetJenisPelanggan();
				echo form_dropdown('jenis_pelanggan', $options,$jenis_pelanggan,'class="form-control"');
			?>
        </div>
	    
	    <input type="hidden" name="id_konsumen" value="<?php echo $id_konsumen; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('konsumen') ?>" class="btn btn-default">Cancel</a>
	</form>
    </body>
</html>