<style>
#th_jtrans,#td_jtrans{display:none; }
</style>

<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('assets/datatables/dataTables.bootstrap.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-4">
                <h2 style="margin-top:0px">Data Konsumen</h2>
            </div>
            <div class="col-md-4 text-center">
                <div style="margin-top: 4px"  id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
            <div class="col-md-4 text-right">
                <?php echo anchor(site_url('konsumen/create'), 'Create', 'class="btn btn-primary"'); ?>
		<?php echo anchor(site_url('konsumen/excel'), 'Excel', 'class="btn btn-primary"'); ?>
	    </div>
        </div>
        <table class="table table-bordered table-striped" id="mytable">
            <thead>
                <tr>
					<th width="80px" id="col_id">No</th>
					<th>Id Korsal</th>
					<th>Nama Konsumen</th>
					<th>Telp</th>
					<th>Status</th>
					<th id="th_jorder">Jenis Order</th>
					<th >Jenis Transaksi</th>
					<th>Action</th>
					
				</tr>
            </thead>
	    <tbody>
            </tbody>
        </table>
        <script src="<?php echo base_url('assets/datatables/jquery.dataTables.js') ?>"></script>
        <script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js') ?>"></script>
        <script type="text/javascript">
			var dataSet = [<?php echo $dataset; ?>]
			
            $(document).ready(function () {
                $("#mytable").dataTable({
					  data: dataSet
					}
				);
            });
        </script>
    </body>
</html>