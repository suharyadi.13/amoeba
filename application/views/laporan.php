<html>
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width,initial-scale=1">

<title>Laporan Transaksi SIP</title>    
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="https://kit-pro.fontawesome.com/releases/v5.9.0/css/pro.min.css">
<link rel="stylesheet" type="text/css" href="http://202.149.67.146:800/surplus//assets/login_v3/fonts/iconic/css/material-design-iconic-font.min.css">

<style type="text/css">
table { 
  table-layout:fixed;
  width:110%;
}   
td,th{
    width:120px;
    white-space:nowrap;
}
td{
    overflow:auto;
}
</style>  

</head>
<body>
    <div class="container-fluid">
    	<div class="row d-flex justify-content-center">
        	<div class="col-11 col-md-11"><h2><?php echo $title ?></h2></div>
            <div class="col-11 col-md-11 text-right"><a class="btn btn-primary" href="<?php echo base_url(); ?>">Kembali</a></div>
        	<div class="col-11 col-md-11 table-responsive" style="overflow:auto;">
        	<table class="table table-striped" style="width:100%" id="tbl_data">
        		<thead>
        		<?php
        		echo '<tr>';
                //echo '<th scope="col">No</th>';
                
        		foreach($field as $item){
        			echo '<th scope="col">'.$item.'</th>';
        		}
        		echo '<th></th>';
        		echo '</tr>';
        		?>
        		</thead>
        		<tbody>
        		<?php
        		/*foreach($DataLaporanAll as $index=>$data){
        			echo '<tr>';
        			foreach($data as $dataLap){
        				echo '<td>'.$dataLap.'</td>';
        			}
        			echo '</tr>';
        		}*/
        		?>	
        		</tbody>
        	</table>
        	</div>
    	</div>
<div class="row">
<div class="col-12">

<div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Update Data</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
       <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary"  id="simpan_trx">Simpan Transaksi</button>
      </div>
      <div class="modal-body">
        <form action="" method="POST" id="frm_edit">
        <?php
        $data = array(
                'name'  => 'id_laporan',
                'id'    => 'id_laporan',
                'value' => '',
                'type'  => 'hidden',
        );
        echo form_input($data);
        foreach($field as $index=>$item){
            if($index == "no_telp"){

            }else if($index == "depot"){
                echo '<div class="form-group">
                <label for="recipient-name" class="col-form-label">Depot</label>
                <select name="depot" id="depot" class="form-control">';
                foreach($data_depot as $index=>$item){
                    if(substr($item['agen'],0,1) =="L" && $group==""){

                            $group =  ' <optgroup label="DEPOT Agen L">';

                            echo $group;

                        }else if(substr($item['agen'],0,1) != "L" && $group !="" ){

                            echo "</optgroup>";

                            $group =  ' <optgroup label="DEPOT Agen Non L">';

                            echo $group;

                            $group = "";

                        }else if($x==$max){

                            echo "</optgroup>";

                        }

                        echo "<option value='".$item['nama_depot']." (".$item['kode'].")'  ".$this->custom_library->cekCombo($item['nama_depot']." (".$item['kode'].")",$depot)."> ".$item['nama_depot']." (".$item['kode'].")</option> ";;

                        $x++;
                }
                //echo form_dropdown("depot",$cmbData,'','class="form-control"');
                echo '</select></div>';
            }else{
            ?>  
              <div class="form-group">
                <label for="recipient-name" class="col-form-label"><?php echo $item ?></label>
                <input type="text" class="form-control" id="<?php echo $index?>" name="<?php echo $index?>" required>
              </div>
            <?php
            }
        }
        ?> 
        </form>
      </div>
     

    </div>
  </div>
</div>  
</div>
</div>
</div>
</body>
</html>
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script type="text/javascript" charset="utf8" src="<?php echo base_url() ?>/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" charset="utf8" src="<?php echo base_url() ?>/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/select/1.3.0/js/dataTables.select.min.js"></script>
<!-- <script type="text/javascript" src="<?php echo base_url() ?>/js/datatables.min.js"></script> -->

<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>





<script language="javascript">
var table;
var RowSelected;

function konfirm(){
    var cnf = confirm('yakin akan menghapus data');
    return cnf;
}



$(document).ready( function () {

    table = $('#tbl_data').DataTable({
        dom: "Bfrtip",
        ajax: "<?php echo $UrlLoadData ?>",
        
        columns: [ 
            <?php
                foreach($field as $index=>$item){
                    //if($index != "no_telp"){
                        echo '{ data: "'.$index.'" },';
                    //}
                }
            ?>
        ],
        select: true,
        buttons: [
            { extend: "excel"},
        ]
    });

     $("#frm_edit").submit(function(){
        console.log($("form").serialize());
        var data = $("#frm_edit").serialize();
        
        $.post("<?php echo base_url("laporan/SaveTransaksi")  ?>",data,function(result){
            var dataRespon = JSON.parse(result);
            if(dataRespon.error ==0){
                 alert(dataRespon.message);
                 //table.ajax.reload();
                 $('#exampleModal').modal('hide');
                 
                 var rows = table
						    .rows( '.selected' )
						    .remove()
						    .draw();
				
            }else{
                alert(dataRespon.message);
            }
        })
        return false;
    })
    $("#simpan_trx").click(function(){
       $("#frm_edit").submit();
    })
} );

function LoadDataForm(idSelected){
    $('#tbl_data').on( 'click', 'tr', function () {
        var Data =  table.row( this ).data();
        RowSelected = table.row( this );
        $("#id_laporan").val(idSelected);
        //console.log(Data);
        var indexRow= 0;
        $.each(Data, function( index, value ) {
            //if(index != "no_telp"){
                $("#"+index+"").val(value);
                indexRow++;
            //}
        });
        
    });
    $('#exampleModal').modal('show');        
}    
</script>