<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Format class
 * Help convert between various formats such as XML, JSON, CSV, etc.
 *
 * @author    Phil Sturgeon, Chris Kacerguis, @softwarespot
 * @license   http://www.dbad-license.org/
 */
class Custom_library {
	public $NamaBulan = array("Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");
	public $ShortNamaBulan = array("Jan","Feb","Mar","Apr","Mei","Jun","Jul","Agus","Sept","Okt","Nov","Des");

function Filter_by_level($userlogin,$level){

		if($level == 1)
			$leveNew =  substr($userlogin,0,1);	
		else if($level == 2)
			$leveNew =  substr($userlogin,0,2);	
		else if($level == 3)
			$leveNew =  substr($userlogin,0,3);
		else if($level == 4)
			$leveNew =  substr($userlogin,0,4);
		else if($level == 5)
			$leveNew =  substr($userlogin,0,5);

		// var_dump($userlogin);
		// die();
		return $leveNew;
}

function GetJenisPelanggan(){
	$options = array(
				'FARMASI'=>'FARMASI',
				'COSMETIC'=>'COSMETIC',
				'F & B'=>'F & B',
				'INSTANSI'=>'INSTANSI',
				'TEXTILE'=>'TEXTILE',
				'KAPUR'=>'KAPUR',
				'LAB COFE'=>'LAB COFE',
				'LAB   '=>'LAB   ',
				'LAB'=>'LAB',
				'FIBER'=>'FIBER',
				'SUPPLIER'=>'SUPPLIER',

		);
	return $options;	
}
function GetStatus(){
	$options = array(
				'PROSPEK'         => 'PROSPEK',
				'DEAL'           => 'DEAL',
				'CANCEL'           => 'CANCEL'			
		);
	return $options;	
}

function GetJenisTransaksi(){
	$options = array(
				'Tender'       => 'Tender',
				'PL'           => 'PL',
				'Beli_Putus'   => 'Beli_Putus'
		);
	return $options;	
}	

function GetJenisOrder(){
	$options = array(
				'Bahan_Kimia'       => 'Bahan_Kimia',
				'Alat_Lab'           => 'Alat_Lab',
				'Lain-lain'           => 'Lain-lain',
		);
	return $options;	
}
function GetJenisArea(){
	$options = array(
				'BANDUNG'=>'BANDUNG',
				'NANJUNG'=>'NANJUNG',
				'CILAME'=>'CILAME',
				'GEDE BAGE'=>'GEDE BAGE',
				'PARAKAN SAAT'=>'PARAKAN SAAT',
				'CIWASTRA'=>'CIWASTRA',
				'PADALARANG'=>'PADALARANG',
				'CIHAMPELAS'=>'CIHAMPELAS',
				'JAKARTA'=>'JAKARTA',
				'SUMBER SARI'=>'SUMBER SARI',
				'BOGOR'=>'BOGOR',
				'CIKOPO, CIKAMPEK'=>'CIKOPO, CIKAMPEK',
				'INDOTASEI, CIKAMPEK'=>'INDOTASEI, CIKAMPEK',
				'SEMARANG'=>'SEMARANG',
				'MAJALENGKA'=>'MAJALENGKA',
				'RANCAEKEK'=>'RANCAEKEK',
				'LAMPUNG'=>'LAMPUNG',
				'CIJAMBE'=>'CIJAMBE',
				'CIMAHI'=>'CIMAHI',
				'PALASARI'=>'PALASARI',
				'SOEKARNO HATTA'=>'SOEKARNO HATTA',
				'CIPATAT'=>'CIPATAT',
				'PASTEUR'=>'PASTEUR',
				'BATUJAJAR'=>'BATUJAJAR',
		);
	return $options;	
}		
function cekCombo($val1,$val2){
	if($val1 == $val2)
		return "selected='selected' ";
	else
		return "";
}

function getNamaBulan($bln){
	if($bln >= 1 && $bln <= 12 ){
		return $this->NamaBulan[$bln-1];
	}else{
		return $bln;
	}


}



}