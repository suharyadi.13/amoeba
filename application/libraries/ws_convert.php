<?php

class ws_convert {
 // SET SUPER GLOBAL
	var $CI = NULL;
	public function __construct() {
		$this->CI =& get_instance();
	}



	public function xml2Array($filename)
	{
	    $xml = simplexml_load_file($filename, "SimpleXMLElement", LIBXML_NOCDATA);
	    $json = json_encode($xml);
	    return json_decode($json,TRUE);
	}

	public function array_to_xml($array, &$xml_user_info){
		foreach($array as $key => $value) {
	        if(is_array($value)) {
	            if(!is_numeric($key)){
	                $subnode = $xml_user_info->addChild("$key");
	                $this->array_to_xml($value, $subnode);
	            }else{
	                $subnode = $xml_user_info->addChild("data");
	               $this->array_to_xml($value, $subnode);
	            }
	        }else {
	            $xml_user_info->addChild("$key", htmlspecialchars("$value"));
	        }
	    }
	}


}
?>