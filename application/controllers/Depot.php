<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Depot extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Depot_model');
        $this->load->library('form_validation');
		
		if($this->session->userdata('logged_in') != TRUE){
			$data['message'] = "Akses ditolak, silahkan login terlebih dahulu. !!";
			redirect('login');
		}
    }

    public function index()
    {
        $depot = $this->Depot_model->get_all();

        $data = array(
            'depot_data' => $depot
        );

        $this->load->view('depot/tbl_depot_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Depot_model->get_by_id($id);
        if ($row) {
            $data = array(
		'no' => $row->no,
		'agen' => $row->agen,
		'kode' => $row->kode,
		'nama_depot' => $row->nama_depot,
		'alamat_depot' => $row->alamat_depot,
		'hotline_depot' => $row->hotline_depot,
	    );
            $this->load->view('depot/tbl_depot_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('depot'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('depot/create_action'),
	    'no' => set_value('no'),
	    'agen' => set_value('agen'),
	    'kode' => set_value('kode'),
	    'nama_depot' => set_value('nama_depot'),
	    'alamat_depot' => set_value('alamat_depot'),
	    'hotline_depot' => set_value('hotline_depot'),
	);
        $this->load->view('depot/tbl_depot_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'agen' => $this->input->post('agen',TRUE),
		'kode' => $this->input->post('kode',TRUE),
		'nama_depot' => $this->input->post('nama_depot',TRUE),
		'alamat_depot' => $this->input->post('alamat_depot',TRUE),
		'hotline_depot' => $this->input->post('hotline_depot',TRUE),
	    );

            $this->Depot_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('depot'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Depot_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('depot/update_action'),
		'no' => set_value('no', $row->no),
		'agen' => set_value('agen', $row->agen),
		'kode' => set_value('kode', $row->kode),
		'nama_depot' => set_value('nama_depot', $row->nama_depot),
		'alamat_depot' => set_value('alamat_depot', $row->alamat_depot),
		'hotline_depot' => set_value('hotline_depot', $row->hotline_depot),
	    );
            $this->load->view('depot/tbl_depot_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('depot'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('no', TRUE));
        } else {
            $data = array(
		'agen' => $this->input->post('agen',TRUE),
		'kode' => $this->input->post('kode',TRUE),
		'nama_depot' => $this->input->post('nama_depot',TRUE),
		'alamat_depot' => $this->input->post('alamat_depot',TRUE),
		'hotline_depot' => $this->input->post('hotline_depot',TRUE),
	    );

            $this->Depot_model->update($this->input->post('no', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('depot'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Depot_model->get_by_id($id);

        if ($row) {
            $this->Depot_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('depot'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('depot'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('agen', 'agen', 'trim|required');
	$this->form_validation->set_rules('kode', 'kode', 'trim|required');
	$this->form_validation->set_rules('nama_depot', 'nama depot', 'trim|required');
	$this->form_validation->set_rules('alamat_depot', 'alamat depot', 'trim|required');
	$this->form_validation->set_rules('hotline_depot', 'hotline depot', 'trim|required');

	$this->form_validation->set_rules('no', 'no', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "tbl_depot.xls";
        $judul = "tbl_depot";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Agen");
	xlsWriteLabel($tablehead, $kolomhead++, "Kode");
	xlsWriteLabel($tablehead, $kolomhead++, "Nama Depot");
	xlsWriteLabel($tablehead, $kolomhead++, "Alamat Depot");
	xlsWriteLabel($tablehead, $kolomhead++, "Hotline Depot");

	foreach ($this->Depot_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->agen);
	    xlsWriteLabel($tablebody, $kolombody++, $data->kode);
	    xlsWriteLabel($tablebody, $kolombody++, $data->nama_depot);
	    xlsWriteLabel($tablebody, $kolombody++, $data->alamat_depot);
	    xlsWriteLabel($tablebody, $kolombody++, $data->hotline_depot);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

}

/* End of file Depot.php */
/* Location: ./application/controllers/Depot.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2016-08-24 17:59:43 */
/* http://harviacode.com */