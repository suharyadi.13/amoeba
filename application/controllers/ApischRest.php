<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class ApischRest extends REST_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
    {
        // Construct the parent class
        parent::__construct();
		$this->output->set_header('Access-Control-Allow-Origin: *');
		$this->load->model('user_model','',TRUE);
		$this->load->model('Berita_model','',TRUE);
		$this->load->model('Dashboard_model','',TRUE);
		$this->load->library(array('form_validation','custom_library','table'));

	}		
	public function index()
	{
		//$this->load->view('home');
	}

	public function index_login(){
		$url = 'http://localhost/eschool/home/WS_SingleSignIN';
		$json = file_get_contents($url);
		$obj = json_decode($json);

		// print_r($json);
		var_dump(realpath_cache_get());
	}

	public function ceklogin_POST(){
		$data['message'] ="";
		$username = $this->post('username');	
		$password = $this->post('password');	
		$UserPlayerID = $this->post('UserPlayerID');	

		if ($password !="" && $username!=""){
			$condition = array("username"=>$username,"password"=>$password);
			$DataLogin = $this->user_model->count_all($condition);
			if($DataLogin > 0){
				$DataUser = $this->user_model->get_by_id($condition)->result();
				$ses_data = array(
					'username'  => $DataUser[0]->username,
					'id_user'     => $DataUser[0]->id_user,
					'nama_user'     => $DataUser[0]->nama_user,
					'level'     => $DataUser[0]->level,
					'logged_in' => TRUE
				);
				$this->db->query("UPDATE tbl_user set UserPlayerID ='".$UserPlayerID."' WHERE id_user = '".$DataUser[0]->id_user."' ");
				$this->db->query("INSERT INTO tbl_activity VALUES('','".$DataUser[0]->id_user."','Login via Mobile','".date("Y-m-d h:i:s")."') ");
				$data = array(
					"message"=> "Login Succes",
					"Error_code" => "0",
					"data"=>$ses_data,
					"dataUser" =>$this->user_model->get_by_id($condition)->result(),
				);
						
				
			}else{
				$data = array(
					"message"=> "Username dan Password tidak sesuai silahkan periksa kembali !! ",
					"Error_code" => "1",
					"data"=>""
				);
			}
		}else{
			$data = array(
					"message"=> "Username dan Password tidak boleh kosong !!",
					"Error_code" => "1",
					"data"=>""
				);
		}
		
		
		$this->set_response($data, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
	}
	
	public function dashboard_POST(){
		$data['message'] ="";
		$username = $this->post('username');	
		$password = $this->post('password');	
		$user_id = $this->post('id_user');	
		$level = $this->post('level');	
		$short_user_id = $this->custom_library->Filter_by_level($user_id,$level);
		
		if ($password !="" && $username!=""){
			$DataDsh = $this->db->query("call data_trx('".$short_user_id."');")->result();
			
			/*$Content = array
								array("judul"=>"Deal PL","content"=>"<p style='text-align:left'>32 Domba</p> <p style='text-align:left'>4 Sapi</p> "),
								array("judul"=>"Deal PS","content"=>"<p style='text-align:left'>32 Domba</p> <p style='text-align:left'>4 Sapi</p> "),
						);
			*/
			foreach($DataDsh as $itemDsh){
				
				$Content[] = array("judul"=>"Deal Domba",
									"content"=>"<p style='text-align:left'>".$itemDsh->PL_domba." PL</p> 
														<p style='text-align:left'>".$itemDsh->PS_domba." PS</p> ");
				$Content[] = array("judul"=>"Deal Sapi",
									"content"=>"<p style='text-align:left'>".$itemDsh->PL_sapi." PL</p> 
														<p style='text-align:left'>".$itemDsh->PS_sapi." PS</p> ");											
			}			
			
			mysqli_next_result( $this->db->conn_id );
			$Berita = $this->Dashboard_model->getBerita($user_id,$level);
			$data = array(
					"message"=> "Data Loaded ",
					"Error_code" => "0",
					"data"=>$Content,
					"berita"=>$Berita
				);			
		}else{
			$data = array(
					"message"=> "Username dan Password tidak boleh kosong !!",
					"Error_code" => "1",
					"data"=>""
				);
		}
		
		
		$this->set_response($data, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
	}
	
	public function berita_POST(){
		$data['message'] ="";
		$user_id = $this->post('id_user');	
		$level = $this->post('level');	
		//$user_id = $this->custom_library->Filter_by_level($user_id,$level);
		
		if ($user_id !="" && $level !=""){
			$Berita = $this->Dashboard_model->getBerita($user_id,$level);
			$data = array(
					"message"=> "Data Loaded ",
					"Error_code" => "0",
					"data"=>$Berita
				);			
		}else{
			$data = array(
					"message"=> "Username tidak dikenali !!",
					"Error_code" => "1",
					"data"=>""
				);
		}
		
		
		$this->set_response($data, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
	}

	
}
