<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Area extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model(array('Konsumen_model','User_model','kunjungan_model'));
        $this->load->library(array('form_validation','session'));
		$this->load->library('custom_library');
		if($this->session->userdata('logged_in') != TRUE){
			$data['message'] = "Akses ditolak, silahkan login terlebih dahulu. !!";
			redirect('login');
		}
    }

    function index(){
        $this->list_area();
    }

    function list_area(){
   
        $data['list_data'] = $this->db->select("* ")
                            ->from("tbl_area")
                            ->get()
                            ->result_array();
        $data['title'] = "Data Area";
        $data['back_url']=site_url("area/");
        $data['add_url']=site_url("area/form_area");
        $data['edit_url']=site_url("area/edit_area");
        //$data['detail_url']=site_url("area/detail_area");
        $data['delete_url']=site_url("area/delete_area"); 
        $data['field_primary'] = 'id_area';
        $data['field'] =array(
            //'id_area'=>'ID Area',
            'nama_area'=>'Nama Area', 
        );

        $data['page'] = "display";
        $this->load->view('admin/index', $data);

    }

    function form_area(){
      
        $data['title'] = "Tambah Area";
        $data['save_url']=site_url("area/proses_add_area");
        $data['action']="add";
        $data['field_primary'] = 'id_area';
        $data['hidden'] = array('id_area'=>$this->getNumberID());
        $data['field'] =array(           
        //    'id_area'=>'ID Prduct',
            'nama_area'=>'Nama Area'
        );

        $data['page'] = "form";
        $this->load->view('admin/index', $data);
    }
    function detail_area($id){
        
        $this->db->select('*');
        $this->db->where('id_area',$id);
        $data['data'] = $this->db->get("tbl_area")->row_array();
        
        
        $data['title'] = "Detail Area";
        $data['save_url']=site_url("area/proses_add_area");
        $data['action']="add";
        $data['field_primary'] = 'id_area';
        
        $data['field'] =array(            
            'id_area'=>'ID Prduct',
            'nama_area'=>'Nama Area', 
        );

        $data['page'] = "detail";
        $this->load->view('admin/index', $data);
    }


    function proses_add_area(){
        //var_dump($this->form_validation->run() );
        $this->_rules();
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata("message",validation_errors());
            //die(validation_errors());
            $this->form_area();
        }else{
            $action = $this->input->post("action");
            $data['form'] =array(
                'id_area'=>$this->input->post("id_area"),
                'nama_area'=>$this->input->post("nama_area"), 
               
            );
            if($action=="add"){
               // array_merge($data['form'],array( 'id_user'=>$this->session->userdata('id_user')));
                $insert = $this->db->insert('tbl_area',$data['form']);
                if($insert ){
                    $this->session->set_flashdata("message","Data Berhasil disimpan");
                    redirect('Area/list_area');
                }else{
                    $this->form_area();
                }
            }else if($action=="edit"){
                $insert = $this->db->replace('tbl_area',$data['form']);
                if($insert ){
                    $this->session->set_flashdata("message","Data Berhasil disimpan");
                    redirect('Area/list_area');
                }else{
                    $this->form_area();
                }

            }else{
                redirect("Area");
            }
        }
    }
    function edit_area($id){
        //$id=$this->input->post("id");
        $this->db->select('*');
        $this->db->where('id_area',$id);
        $data['data'] = $this->db->get("tbl_area")->row_array();
        $data['title'] = "Edit Area";
        $data['save_url']=site_url("area/proses_add_area");
        $data['action']="edit";
        $data['field_primary'] = 'id_area';
        $data['hidden'] = array("id_area"=>$data['data']['id_area']);
        $data['field'] =array(            
            //'id_area'=>'ID Prduct',
            'nama_area'=>'Nama Area', 
        );

        $data['page'] = "form";
        $this->load->view('admin/index', $data);
    }

    function delete_area($id){
         
        $delete = $this->db->delete('tbl_area',array('id_area'=>$id));
        if($delete ){
            $this->session->set_flashdata("message","Data Berhasil dihapus");
            redirect('Area/list_area');
        }else{
            $this->session->set_flashdata("message","Data Gagal dihapus");
            $this->list_area();
        }
    }
    function getProduk_JSON($id){
        $this->db->select('*');
        $this->db->where('id_area',$id);
        $data = $this->db->get("tbl_area")->row_array();

        if(count($data) >0){
            $respone = array("message"=>"Load data sukses","data"=>$data,"error"=>"0");
        }else{
            $respone = array("message"=>"Load data gagal","data"=>"","error"=>"1");
        }

        echo json_encode($respone);
    }
    function getNumberID(){
        $lastArea = $this->db->select('max(id_area) last_id')
                        ->from('tbl_area')
                        ->get()->row_array();
        return $lastArea['last_id']+1;

    }

    public function _rules() 
    {
 
    $this->form_validation->set_rules('nama_area', 'Nama Area', 'trim|required'); 
    }





























   

}

/* End of file Area.php */
/* Location: ./application/controllers/Area.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2016-08-16 13:25:39 */
/* http://harviacode.com */