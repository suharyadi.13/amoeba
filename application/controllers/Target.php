<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Target extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model(array('Konsumen_model','User_model','kunjungan_model'));
        $this->load->library(array('form_validation','session'));
		$this->load->library('custom_library');
		if($this->session->userdata('logged_in') != TRUE){
			$data['message'] = "Akses ditolak, silahkan login terlebih dahulu. !!";
			redirect('login');
		}
    }

    function index(){
        $this->list_target();
    }

    function list_target(){
        $condition = array("tbl_target.id_user"=>$this->custom_library->Filter_by_level($this->session->userdata("id_user"),$this->session->userdata("level")));
        foreach($condition as $field=>$val){
            $this->db->like($field,$val,'after');
        }
        $year = ($this->input->post('year')!="")?$this->input->post('year'):date("Y");
        $this->db->where('tahun',$year);
        $data['list_data'] = $this->db->select("tbl_target.*,tbl_user.nama_user,
                            
                            ")
                            ->from("tbl_target")
                            ->join("tbl_user","tbl_user.id_user = tbl_target.id_user","left")
                            ->group_by('tbl_target.id_user,tbl_target.tahun')
                            ->get()
                            ->result_array();
        $data['title'] = "Data Target";
        $data['back_url']=site_url("target/");
        $data['add_realisasi']=site_url("Target/form_realisasi");
        $data['add_url']=site_url("Target/form_target");
        $data['edit_url']=site_url("Target/edit_target");
        $data['detail_url']=site_url("Target/detail_target");
        $data['delete_url']=site_url("Target/delete_target"); 
        $data['field_primary'] = 'id_target';
        $data['field'] =array(
            'nama_user'=>'Nama Sales',
            'tahun'=>'Tahun',
            'target_tahun'=>'Target',
            'target_januari'=>'januari',
            'realisasi_januari'=>'januari',
            'target_februari'=>'februari',
            'realisasi_februari'=>'februari',
            'target_maret'=>'maret',
            'realisasi_maret'=>'maret',
            'target_april'=>'april',
            'realisasi_april'=>'april',
            'target_mei'=>'mei',
            'realisasi_mei'=>'mei',
            'target_juni'=>'juni',
            'realisasi_juni'=>'juni',
            'target_juli'=>'juli',
            'realisasi_juli'=>'juli',
            'target_agustus'=>'agustus',
            'realisasi_agustus'=>'agustus',
            'target_september'=>'september',
            'realisasi_september'=>'september',
            'target_oktober'=>'oktober',
            'realisasi_oktober'=>'oktober',
            'target_november'=>'november',
            'realisasi_november'=>'november',
            'target_desember'=>'desember',
            'realisasi_desember'=>'desember',
        );

        $data['page'] = "display_target";
        $this->load->view('admin/index', $data);

    }

    function form_target(){
    
        $data['user'] = $this->User_model->getCombo();
        $data['title'] = "Tambah Target";
        $data['save_url']=site_url("Target/proses_add_target");
        $data['back_url']=site_url("Target/list_target");
        $data['action']="add";
        $data['field_primary'] = 'id_target';
        $data['hidden'] = array('id_target'=>$this->getNumberID());
        $tahun = array();
        for($x=date('Y');$x>=(date('Y')-10);$x--){
            $tahun[$x]['label'] = $x;
            $tahun[$x]['value'] = $x;
        }

        $data['field'] =array(            
            //'id_konsumen'=>'ID Target',
            'id_user'=>array('label'=>'Sales',
                            'type'=>'combo',
                            'data'=>$data['user'],
                        ),
            'NULL'=>"", //space kosong
            'tahun'=>array('label'=>'Tahun',
                            'type'=>'combo',
                            'data'=>$tahun
                        ),
            'target_tahun'=>'Nominal Target Tahun ',
        );
        $data['fieldBUlanan'] =array(
            'id_user'=>array('label'=>'Sales',
                            'type'=>'combo',
                            'data'=>$data['user'],
                        ),
            'tahun'=>array('label'=>'Tahun',
                            'type'=>'combo',
                            'data'=>$tahun
                        ), 
            'target_januari'=>'Target januari', 
            'target_februari'=>'Target februari', 
            'target_maret'=>'Target maret', 
            'target_april'=>'Target april', 
            'target_mei'=>'Target mei', 
            'target_juni'=>'Target juni', 
            'target_juli'=>'Target juli', 
            'target_agustus'=>'Target agustus', 
            'target_september'=>'Target september', 
            'target_oktober'=>'Target oktober', 
            'target_november'=>'Target november', 
            'target_desember'=>'Target desember',
        );
        $data['tabed'] = array("tahunan"=>$data['field'] ,"bulanan"=>$data['fieldBUlanan'] );
        $data['page'] = "target/formTarget";
        $this->load->view('admin/index', $data);
    }
    function detail_target($id){
        
        $year = ($this->input->post('year')!="")?$this->input->post('year'):date("Y");
        
        $data['data'] = $this->db->select("tbl_target.*,tbl_user.nama_user")
                            ->from("tbl_target")
                            ->join("tbl_user","tbl_user.id_user = tbl_target.id_user","left")
                            ->where('tahun',$year)
                            ->where('id_target',$id)
                            ->group_by('tbl_target.id_user,tbl_target.tahun')
                            ->get()
                            ->row_array();
        
       
        $data['user'] = $this->User_model->getCombo();
        $data['title'] = "Detail Target";
        $data['save_url']=site_url("Target/proses_add_target");
        $data['action']="add";
        $data['field_primary'] = 'id_target';
        
        $data['field'] =array(
            'nama_user'=>'Nama Sales',
            'NULL'=>"", //space kosong
            'tahun'=>'Tahun', 
            'target_januari'=>'Target januari',
            'realisasi_januari'=>'Realiasai januari',
            'target_februari'=>'Target februari',
            'realisasi_februari'=>'Realiasai februari',
            'target_maret'=>'Target maret',
            'realisasi_maret'=>'Realiasai maret',
            'target_april'=>'Target april',
            'realisasi_april'=>'Realiasai april',
            'target_mei'=>'Target mei',
            'realisasi_mei'=>'Realiasai mei',
            'target_juni'=>'Target juni',
            'realisasi_juni'=>'Realiasai juni',
            'target_juli'=>'Target juli',
            'realisasi_juli'=>'Realiasai juli',
            'target_agustus'=>'Target agustus',
            'realisasi_agustus'=>'Realiasai agustus',
            'target_september'=>'Target september',
            'realisasi_september'=>'Realiasai september',
            'target_oktober'=>'Target oktober',
            'realisasi_oktober'=>'Realiasai oktober',
            'target_november'=>'Target november',
            'realisasi_november'=>'Realiasai november',
            'target_desember'=>'Target desember',
            'realisasi_desember'=>'Realiasai desember',


        );

        $data['page'] = "detail";
        $this->load->view('admin/index', $data);
    }


    function proses_add_target(){
        //var_dump($this->form_validation->run() );
        $this->_rules();
        $action = $this->input->post("action");
        $id_target = $this->input->post("id_target");
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata("message",validation_errors());
            //die(validation_errors());
            if($action=="edit"){
                //$this->edit_target($id_target);
            }else{
                $this->form_target();
            }
        }else{
           
            
            if($action=="add"){
                if($this->input->post("target_tahun")!=""){
                    $targetBulan = round($this->input->post("target_tahun")/12);
                
                    $data['form'] =array(
                        'id_user'=>$this->input->post("id_user"),
                        'target_tahun'=>$this->input->post("target_tahun"),
                        'tahun'=>$this->input->post("tahun"),
                        'target_januari'=> $targetBulan,
                        'target_februari'=> $targetBulan,
                        'target_maret'=> $targetBulan,
                        'target_april'=> $targetBulan,
                        'target_mei'=> $targetBulan,
                        'target_juni'=> $targetBulan,
                        'target_juli'=> $targetBulan,
                        'target_agustus'=> $targetBulan,
                        'target_september'=> $targetBulan,
                        'target_oktober'=> $targetBulan,
                        'target_november'=> $targetBulan,
                        'target_desember'=> ($targetBulan+($this->input->post("target_tahun")-($targetBulan*12))),
                    );
                   // array_merge($data['form'],array( 'id_user'=>$this->session->userdata('id_user')));
                    
                }else{
                    $data['form'] =array(
                        'id_user'=>$this->input->post("id_user"),
                        'target_tahun'=>($this->input->post("target_januari")+$this->input->post("target_februari")+$this->input->post("target_maret")+$this->input->post("target_april")+$this->input->post("target_mei")+$this->input->post("target_juni")+$this->input->post("target_juli")+$this->input->post("target_agustus")+$this->input->post("target_september")+$this->input->post("target_oktober")+$this->input->post("target_november")+$this->input->post("target_desember")), 
                        'target_januari'=> $this->input->post("target_januari"),
                        'target_februari'=> $this->input->post("target_februari"),
                        'target_maret'=> $this->input->post("target_maret"),
                        'target_april'=> $this->input->post("target_april"),
                        'target_mei'=> $this->input->post("target_mei"),
                        'target_juni'=> $this->input->post("target_juni"),
                        'target_juli'=> $this->input->post("target_juli"),
                        'target_agustus'=> $this->input->post("target_agustus"),
                        'target_september'=> $this->input->post("target_september"),
                        'target_oktober'=> $this->input->post("target_oktober"),
                        'target_november'=> $this->input->post("target_november"),
                        'target_desember'=> ( $this->input->post("target_desember")),
                    );
                }

                $insert = $this->db->insert('tbl_target',$data['form']);
                    if($insert ){
                        $this->session->set_flashdata("message","Data Berhasil disimpan");
                        redirect('Target/list_target');
                    }else{
                        $this->form_target();
                    }
            }else if($action=="edit"){
                $data['form'] =array(
                    'realisasi_januari'=>$this->input->post('realisasi_januari'),
                    'realisasi_februari'=>$this->input->post('realisasi_februari'),
                    'realisasi_maret'=>$this->input->post('realisasi_maret'),
                    'realisasi_april'=>$this->input->post('realisasi_april'),
                    'realisasi_mei'=>$this->input->post('realisasi_mei'),
                    'realisasi_juni'=>$this->input->post('realisasi_juni'),
                    'realisasi_juli'=>$this->input->post('realisasi_juli'),
                    'realisasi_agustus'=>$this->input->post('realisasi_agustus'),
                    'realisasi_september'=>$this->input->post('realisasi_september'),
                    'realisasi_oktober'=>$this->input->post('realisasi_oktober'),
                    'realisasi_november'=>$this->input->post('realisasi_november'),
                    'realisasi_desember'=>$this->input->post('realisasi_desember'),
                );
                $insert = $this->db->update('tbl_target',$data['form'],array('id_target'=>$this->input->post("id_target")));
                if($insert ){
                    $this->session->set_flashdata("message","Data Berhasil disimpan");
                    redirect('Target/list_target');
                }else{
                    $this->edit_target($id_target);
                }

            }else{
                redirect("Target");
            }
        }
    }
    function edit_target($id){

        $this->db->select('*');
        $this->db->where('id_target',$id);
        $data['data'] = $this->db->get("tbl_target")->row_array();

        $data['user'] = $this->User_model->getCombo();
        $data['title'] = "Tambah Realisasi";
        $data['save_url']=site_url("Target/proses_add_target");
        $data['action']="edit";
        $data['field_primary'] = 'id_target';
        $data['hidden'] = array('id_target'=>$id);
        $data['back_url'] = site_url('target');
        $tahun = array();
        for($x=date('Y');$x>=(date('Y')-10);$x--){
            $tahun[$x]['label'] = $x;
            $tahun[$x]['value'] = $x;
        }
        $data['field'] =array(            
            'realisasi_januari'=> 'Realisasi januari',
            'realisasi_februari'=> 'Realisasi februari',
            'realisasi_maret'=> 'Realisasi maret',
            'realisasi_april'=> 'Realisasi april',
            'realisasi_mei'=> 'Realisasi mei',
            'realisasi_juni'=> 'Realisasi juni',
            'realisasi_juli'=> 'Realisasi juli',
            'realisasi_agustus'=> 'Realisasi agustus',
            'realisasi_september'=> 'Realisasi september',
            'realisasi_oktober'=> 'Realisasi oktober',
            'realisasi_november'=> 'Realisasi november',
            'realisasi_desember'=> 'Realisasi desember',
        );

        $data['page'] = "form";
        $this->load->view('admin/index', $data);
    }

    function delete_target($id){
         
        $delete = $this->db->delete('tbl_target',array('id_target'=>$id));
        if($delete ){
            $this->session->set_flashdata("message","Data Berhasil dihapus");
            redirect('Target/list_target');
        }else{
            $this->session->set_flashdata("message","Data Gagal dihapus");
            $this->list_target();
        }
    }
    function form_realisasi(){
        $data['user'] = $this->User_model->getCombo();
        $data['title'] = "Tambah Target";
        $data['save_url']=site_url("Target/proses_add_target");
        $data['action']="add";
        $data['field_primary'] = 'id_target';
        //$data['hidden'] = array('id_target'=>$this->getNumberID());
        $tahun = array();
        for($x=date('Y');$x>=(date('Y')-10);$x--){
            $tahun[$x]['label'] = $x;
            $tahun[$x]['value'] = $x;
        }
        $data['field'] =array(            
            //'id_konsumen'=>'ID Target',
            'id_user'=>array('label'=>'User',
                            'type'=>'combo',
                            'data'=>$data['user'],
                        ),
            'NULL'=>"", //space kosong
            'tahun'=>array('label'=>'Tahun',
                            'type'=>'combo',
                            'data'=>$tahun
                        ),
            'target_tahun'=>'Nominal Target Tahun ',
        );

        $data['page'] = "form";
        $this->load->view('admin/index', $data);
    }
    function getNumberID(){
        $lastKonsumen = $this->db->select('max(id_target) last_id')
                        ->from('tbl_target')
                        ->get()->row_array();
        return $lastKonsumen['last_id']+1;

    }

    public function _rules() 
    {
    if($this->input->post("action")=="add"){
        $this->form_validation->set_rules('id_target', 'ID Target', 'trim|required|is_unique[tbl_target.id_target]');
        $this->form_validation->set_rules('tahun', 'Tahun Target', 'trim|required');
        $this->form_validation->set_rules('target_tahun', 'Jumlah Target Tahun', 'trim|required');
    }else if($this->input->post("action")=="edit"){
       $this->form_validation->set_rules('id_target', 'ID Target', 'trim|required');
    }
    $this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
    
    }

}

/* End of file Target.php */
/* Location: ./application/controllers/Target.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2016-08-16 13:25:39 */
/* http://harviacode.com */