<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Kunjungan extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model(array('Kunjungan_model','konsumen_model','transaksi_model'));
        $this->load->library( array('form_validation','custom_library'));
		$this->load->helper('date');
		
		if($this->session->userdata('logged_in') != TRUE){
			$data['message'] = "Akses ditolak, silahkan login terlebih dahulu. !!";
			redirect('login');
		}
    }

    public function index($id_konsumen)
    {
		$condition = array("id_konsumen"=>$id_konsumen);
        $kunjungan = $this->Kunjungan_model->get_all($condition);

        $data = array(
            'kunjungan_data' => $kunjungan,
			'id_konsumen' =>$id_konsumen,
			'konsumen'=>(array)$this->konsumen_model->get_by_id($id_konsumen),
			'transaksi'=>(array)$this->transaksi_model->get_all($condition),
			'page'=>'kunjungan/tbl_kunjungan_list'
        );
        $this->load->view('home', $data);
    }
	
	function show_all(){
		$condition = array("id_user"=>$this->custom_library->Filter_by_level($this->session->userdata("id_user"),$this->session->userdata("level")));
        $kunjungan_data = $this->Kunjungan_model->get_all($condition,1);
		
		$start = 0;
		$dataset ="";
		$maxRow = count($kunjungan_data);
		foreach ($kunjungan_data as $kunjungan)
		{
		$action = anchor(site_url('kunjungan/index/'.$kunjungan->id_konsumen),'<i class="icon_zoom-in_alt"></i>','class="btn btn-primary" title="detail data"'); 
		
		$dataset .= "
		['".++$start."','".$kunjungan->id_user.' : '.addslashes($kunjungan->nama_user) ."','".addslashes($kunjungan->nama_konsumen)."','".nice_date($kunjungan->tanggal_kunjungan,"d-M-Y")."','".$kunjungan->status."','".$kunjungan->jenis_order."','".addslashes(preg_replace("/[\r\n]+/", " ", $kunjungan->keterangan))."','".addslashes($action)."']";
			if($start  < ($maxRow)){
				$dataset .=  ',';
			}
		}


        $data = array(
			'dataset' => $dataset,
			'page'=>'kunjungan/tbl_kunjungan_list_all'
        );
        $this->load->view('home', $data);

	}

    public function read($id) 
    {
        $row = $this->Kunjungan_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_kunjungan' => $row->id_kunjungan,
		'id_konsumen' => $row->id_konsumen,
		'tanggal_kunjungan' => $row->tanggal_kunjungan,
		'status' => $row->status,
		'keterangan' => $row->keterangan,
		'page'=>'kunjungan/tbl_kunjungan_read'
        );
			$this->load->view('home', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('kunjungan/index/'.$row->id_konsumen));
        }
    }

    public function create($id_konsumen) 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('kunjungan/create_action/'.$id_konsumen),
			'id_kunjungan' => set_value('id_kunjungan'),
			'id_konsumen' => $id_konsumen,
			'tanggal_kunjungan' => set_value('tanggal_kunjungan'),
			'status' => set_value('status'),
			'keterangan' => set_value('keterangan'),
			'tanggal_survey' => set_value('tanggal_survey'),
			'rencana_kelas' => set_value('rencana_kelas'),
			'jenis_order' => set_value('jenis_order'),
			'jenis_transaksi' => set_value('jenis_transaksi'),
			'page'=>'kunjungan/tbl_kunjungan_form'
        );
			$this->load->view('home', $data);
    }
    
	/*
		proses Function for submit create kunjungan
	*/
    public function create_action($id_konsumen) 
    {
		//add validation if MQL selected
		if($this->input->post('status',TRUE)=="MQL"){
			$this->form_validation->set_rules('jenis_transaksi', 'Jenis Transaksi', 'required');	
			if($this->input->post('jenis_transaksi',TRUE)=="PL"){
				$this->form_validation->set_rules('tanggal_survey', 'Rencana Tanggal Transaksi', 'required');
			}else{
				$this->form_validation->set_rules('rencana_kelas', 'Rencana Kelas', 'required');
			}
			
		}
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create($id_konsumen);
        } else {
            $data = array(
			'id_konsumen' => $this->input->post('id_konsumen',TRUE),
			'tanggal_kunjungan' => $this->input->post('tanggal_kunjungan',TRUE),
			'status' => $this->input->post('status',TRUE),
			'keterangan' => $this->input->post('keterangan',TRUE),
			'tanggal_survey' => $this->input->post('tanggal_survey',TRUE),
			'jenis_transaksi' => $this->input->post('jenis_transaksi',TRUE),
			'jenis_order' => implode($this->input->post('jenis_order'),";"),
			'rencana_kelas' => $this->input->post('rencana_kelas',TRUE),
			'datetime'=> date("y-m-d h:i:s")
	    );
//		var_dump($data);die();
		
            $this->Kunjungan_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
			$this->update_status_kons($data);
            redirect(site_url('kunjungan/index/'.$id_konsumen));
        }
    }
	
    function update_status_kons($data){
		if($data['id_konsumen'] !=""){
				$dataupdate = array(
										'id_konsumen'=>$data['id_konsumen'],
										'status'=>$data['status'],
										'tanggal_survey'=>$data['tanggal_survey'],
										'rencana_kelas'=>$data['rencana_kelas'],
										'jenis_order'=>$data['jenis_order'],
										'jenis_transaksi'=>$data['jenis_transaksi']
									);
				$this->konsumen_model->update($data['id_konsumen'],$dataupdate);
		}
		
	}
    public function update($id) 
    {
        $row = $this->Kunjungan_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('kunjungan/update_action'),
				'id_kunjungan' => set_value('id_kunjungan', $row->id_kunjungan),
				'id_konsumen' => set_value('id_konsumen', $row->id_konsumen),
				'tanggal_kunjungan' => set_value('tanggal_kunjungan', $row->tanggal_kunjungan),
				'status' => set_value('status', $row->status),
				'keterangan' => set_value('keterangan', $row->keterangan),
				'page'=>'kunjungan/tbl_kunjungan_form'
        );
			$this->load->view('home', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('kunjungan/index/'.$row->id_konsumen));
        }
    }
    
    public function update_action($id_konsumen) 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_kunjungan', TRUE));
        } else {
            $data = array(
		'id_konsumen' => $this->input->post('id_konsumen',TRUE),
		'tanggal_kunjungan' => $this->input->post('tanggal_kunjungan',TRUE),
		'status' => $this->input->post('status',TRUE),
		'keterangan' => $this->input->post('keterangan',TRUE),
	    );

            $this->Kunjungan_model->update($this->input->post('id_kunjungan', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('kunjungan/index/'.$id_konsumen));
        }
    }
    
    public function delete($id_konsumen,$id) 
    {
        $row = $this->Kunjungan_model->get_by_id($id);

        if ($row) {
            $this->Kunjungan_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('kunjungan/index/'.$id_konsumen));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('kunjungan/index/'.$id_konsumen));
        }
    }

    public function _rules() 
    {
		$this->form_validation->set_rules('tanggal_kunjungan', 'Tanggal Kunjungan', 'trim|required');
		$this->form_validation->set_rules('status', 'status', 'trim|required');
		$this->form_validation->set_rules('keterangan', 'keterangan', 'trim|required');
		$this->form_validation->set_rules('id_kunjungan', 'id_kunjungan', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "Data_kunjungan.xls";
        $judul = "Data Kunjungan";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "ID korsal");
	xlsWriteLabel($tablehead, $kolomhead++, "korsal");
	xlsWriteLabel($tablehead, $kolomhead++, "nama_konsumen");
	xlsWriteLabel($tablehead, $kolomhead++, "tanggal_kunjungan");
	xlsWriteLabel($tablehead, $kolomhead++, "Status");
	xlsWriteLabel($tablehead, $kolomhead++, "Keterangan");
	
	$condition = array("id_user"=>$this->custom_library->Filter_by_level($this->session->userdata("id_user"),$this->session->userdata("level")));
	$kunjungan = $this->Kunjungan_model->get_all($condition);
	
	foreach ($kunjungan as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->id_user);
		xlsWriteLabel($tablebody, $kolombody++, $data->nama_user);
		xlsWriteLabel($tablebody, $kolombody++, $data->nama_konsumen);
	    xlsWriteLabel($tablebody, $kolombody++, $data->tanggal_kunjungan);
	    xlsWriteLabel($tablebody, $kolombody++, $data->status);
	    xlsWriteLabel($tablebody, $kolombody++, $data->keterangan);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

}

/* End of file Kunjungan.php */
/* Location: ./application/controllers/Kunjungan.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2016-08-17 09:16:07 */
/* http://harviacode.com */