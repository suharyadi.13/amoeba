<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Prospect extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
		$this->load->model('Dashboard_model');
		$this->load->model(array('Konsumen_model','User_model','kunjungan_model'));
        $this->load->library(array('form_validation','session'));
        $this->load->library('custom_library');
        if($this->session->userdata('logged_in') != TRUE){
            $data['message'] = "Akses ditolak, silahkan login terlebih dahulu. !!";
            redirect('login');
        }
    }

    function index(){
        $this->list_prospect();
    }

    function list_prospect(){

        $this->db->select('*');
        $this->db->from('tbl_prospect');
        $this->db->join('tbl_user','tbl_user.id_user = tbl_prospect.id_user','LEFT');
        $this->db->join('tbl_konsumen','tbl_konsumen.id_konsumen = tbl_prospect.id_konsumen');        
        if ($this->session->userdata('level') != 1 ){
            $this->db->where("tbl_prospect.id_user",$this->session->userdata('id_user'));
        }
        $data['list_data'] = $this->db->get()->result_array();
        $data['title'] = "Data Prospect";
        $data['back_url']=site_url("prospect/");
        $data['add_url']=site_url("Prospect/form_prospect");
        $data['edit_url']=site_url("Prospect/edit_prospect");
        $data['detail_url']=site_url("Prospect/detail_prospect");
        $data['delete_url']=site_url("Prospect/delete_prospect"); 
        $data['field_primary'] = 'id_prospect';
        $data['field'] =array(
            //'id_prospect'=>'ID Prospek',
            'tgl_prospect'=>'Tgl Prospek',
            'nama_perusahaan'=>'Konsumen',
            //'user'=>'User',
            'probability'=>'Probability',
            'estimasi_PO'=>'Estimasi PO',
            'nama_user'=>'Sales',

        );

        $data['page'] = "display";
        $this->load->view('admin/index', $data);

    }

    function form_prospect(){

        $data['konsumen'] = $this->Konsumen_model->getCombo("tbl_konsumen","id_konsumen","nama_perusahaan");
        $data['area'] = $this->Konsumen_model->getCombo("tbl_area","id_area","nama_area");
        $data['title'] = "Tambah Prospect";
        $data['save_url']=site_url("Prospect/proses_add_prospect");
        $data['user'] = $this->User_model->getCombo();
        $data['action']="add";
        $data['js_file'] = 'prospect';
        $data['field_primary'] = 'id_prospect';        
        $data['hidden'] = array("id_prospect"=>$this->getNumberID());
        $data['field'] =array(            
            //'id_prospect'=>'ID Prospek',
            'id_konsumen'=>array('label'=>'Konsumen',
                            'type'=>'combo',
                            'data'=>$data['konsumen'],
                        ),
             'tgl_prospect'=>array('label'=>'Tgl Prospek',
                            'type'=>'date',
                        ),
            //'user'=>array('label'=>'Sales','data'=>$data['user'],'type'=>'combo'),
            'probability'=>'Probability',
            'estimasi_PO'=>array('label'=>'Estimasi PO',
                            'type'=>'month',
                        ),
            'id_user'=>array('label'=>'Sales',
                            'type'=>'combo',
                            'data'=>$data['user'],
                        ),

        );

        $data['page'] = "form";
        $this->load->view('admin/index', $data);
    }
    function detail_prospect($id){
        $this->db->select('*');
        $this->db->join('tbl_user','tbl_user.id_user = tbl_prospect.id_user','LEFT');
        $this->db->join('tbl_konsumen','tbl_konsumen.id_konsumen = tbl_prospect.id_konsumen','LEFT');
        $this->db->where('id_prospect',$id);
        $data['data'] = $this->db->get("tbl_prospect")->row_array();
        if(count($data['data'])<=0){
            $this->session->set_flashdata("message","Data prospect tidak ditemukan");
            redirect('Prospect/list_prospect');
        }
        

        $data['konsumen'] = $this->Konsumen_model->getCombo("tbl_konsumen","id_konsumen","nama_perusahaan");
        $data['area'] = $this->Konsumen_model->getCombo("tbl_area","id_area","nama_area");
        $data['title'] = "Detail Prospect";
        $data['closing_url']=site_url("prospect/closing_prospect/".$id);
        $data['save_url']=site_url("Prospect/proses_add_prospect");
        $data['user'] = $this->User_model->getCombo();
        $data['action']="add";
        $data['field_primary'] = 'id_prospect';
        $data['hidden'] = array("id_prospect"=>$data['data']['id_prospect']);
        $data['field'] =array(            
            //'id_prospect'=>'ID Prospek',
            'nama_perusahaan'=>'Konsumen',
            'tgl_prospect'=>array('label'=>'Tgl Prospek',
                            'type'=>'date',
                        ),
            //'user'=>'User',
            'probability'=>'Probability',
            'estimasi_PO'=>'Estimasi PO',
            'nama_user'=>'Sales'

        );

        $data['page'] = "detail_sub";
        $data['data_sub'] = $this->list_detail_prospect($id,$data['data']);
        $this->load->view('admin/index', $data);
    }


    function proses_add_prospect(){
        //var_dump($this->form_validation->run() );
        $this->_rules();
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata("message",validation_errors());
            //die(validation_errors());
            $this->form_prospect();
        }else{
            $action = $this->input->post("action");
            $data['form'] =array(
                'id_prospect'=>$this->input->post("id_prospect"),
                'tgl_prospect'=>$this->input->post("tgl_prospect"),
                'id_konsumen'=>$this->input->post("id_konsumen"),
                'user'=>$this->input->post("user"),
                'probability'=>$this->input->post("probability"),
                'estimasi_PO'=>$this->input->post("estimasi_PO"),
                'id_user'=>$this->input->post("id_user"),
               
            );
            if($action=="add"){
               // array_merge($data['form'],array( 'id_user'=>$this->session->userdata('id_user')));
                $insert = $this->db->insert('tbl_prospect',$data['form']);
                if($insert ){
                    $this->session->set_flashdata("message","Data Berhasil disimpan");
                    redirect('Prospect/detail_prospect/'.$this->input->post("id_prospect"));
                }else{
                    $this->form_prospect();
                }
            }else if($action=="edit"){
                $insert = $this->db->replace('tbl_prospect',$data['form']);
                if($insert ){
                    $this->session->set_flashdata("message","Data Berhasil disimpan");
                    $this->Dashboard_model->insert_activity('Update data Prospect',json_encode($data['form']));
                    redirect('Prospect/list_prospect');
                }else{
                    $this->form_prospect();
                }

            }else{
                redirect("Prospect");
            }
        }
    }
    function edit_prospect($id){
        //$id=$this->input->post("id");
        $this->db->select('*');
        $this->db->where('id_prospect',$id);
        $data['data'] = $this->db->get("tbl_prospect")->row_array();
        $data['konsumen'] = $this->Konsumen_model->getCombo("tbl_konsumen","id_konsumen","nama_perusahaan");
        $data['area'] = $this->Konsumen_model->getCombo("tbl_area","id_area","nama_area");
        $data['title'] = "Edit Prospect";
        $data['save_url']=site_url("Prospect/proses_add_prospect");
        $data['user'] = $this->User_model->getCombo();
        $data['action']="edit";
        $data['js_file'] = 'prospect';
        $data['field_primary'] = 'id_prospect';
        $data['hidden'] = array("id_prospect"=>$data['data']['id_prospect']);
        $data['field'] =array(            
            //'id_prospect'=>'ID Prospek',
            'id_konsumen'=>array('label'=>'Konsumen',
                            'type'=>'combo',
                            'data'=>$data['konsumen'],
                        ),
            'tgl_prospect'=>array('label'=>'Tgl Prospek',
                            'type'=>'date',
                        ),
            //'user'=>'User',
            'probability'=>'Probability',
            'estimasi_PO'=>array('label'=>'Estimasi PO',
                            'type'=>'month',
                        ),
            'id_user'=>array('label'=>'Sales',
                            'type'=>'combo',
                            'data'=>$data['user'],
                        ),

        );

        $data['page'] = "form";
        $this->load->view('admin/index', $data);
    }

    function closing_prospect($id){
        $this->form_validation->set_rules('tgl_closing', 'Tgl Closing Prospect', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata("message",validation_errors());
        }else{
            $query ="UPDATE tbl_prospect SET probability='100%',nominal_PO='".$this->input->post('nominal_po')."', tgl_PO='".$this->input->post('tgl_closing')."' WHERE id_prospect='".$id."'; ";
            $this->db->query($query);
            foreach($this->input->post('status') as $index=>$item){
                $query = " UPDATE tbl_prospect_detail set tbl_prospect_detail.status='".$item."' 
                            WHERE id_detail_prospect='".$index."'; ";
                $this->db->query($query);
            }
            $this->db->query($query);
        }
        $this->detail_prospect($id);
    }

    function delete_prospect($id){
         
        $delete = $this->db->delete('tbl_prospect',array('id_prospect'=>$id));
        if($delete ){
            $this->db->delete('tbl_prospect_detail',array('id_prospect'=>$id));
            $this->session->set_flashdata("message","Data Berhasil dihapus");
            redirect('Prospect/list_prospect');
        }else{
            $this->session->set_flashdata("message","Data Gagal dihapus");
            $this->list_prospect();
        }
    }

    
    function list_detail_prospect($id,$dataProspect){
        $this->db->select("tbl_prospect_detail.*,
                    tbl_prospect.*,
                    tbl_konsumen.nama_perusahaan nama_konsumen,
                    tbl_user.nama_user,
                    tbl_product.nama_product,
                    CONCAT(REPLACE(FORMAT(FLOOR(tbl_prospect_detail.harga_satuan),0),',','.'),',',SUBSTRING_INDEX(FORMAT(tbl_prospect_detail.harga_satuan,2),'.',-1)) format_harga_satuan,
                    (tbl_prospect_detail.harga_satuan*tbl_prospect_detail.qty) as total_harga");
        $this->db->from('tbl_prospect_detail');
        $this->db->join('tbl_product','tbl_product.id_product = tbl_prospect_detail.id_product','LEFT');
        $this->db->join('tbl_prospect','tbl_prospect.id_prospect = tbl_prospect_detail.id_prospect','LEFT');
        $this->db->join('tbl_user','tbl_user.id_user = tbl_prospect.id_user','LEFT');
        $this->db->join('tbl_konsumen','tbl_konsumen.id_konsumen = tbl_prospect.id_konsumen');   
        $this->db->where('tbl_prospect_detail.id_prospect',$id);     
        $data['list_data'] = $this->db->get()->result_array();
        $data['title'] = "Data Produk Prospect";
        $data['back_url']=site_url("prospect/");  
        if($dataProspect['tgl_PO']=="" || is_null($dataProspect['tgl_PO'])){
            $data['add_url']=site_url("Prospect/form_detail_prospect/".$id); 
            //$data['edit_url']=site_url("Prospect/detail_prospect/".$id);
            //$data['detail_url']=site_url("Prospect/detail_prospect/".$id);
            $data['delete_url']=site_url("Prospect/delete_detail_prospect/".$id.""); 
        }
        $data['field_primary'] = 'id_detail_prospect';
        
        $data['field'] =array(
            //'id_prospect'=>'ID Prospek',
            'tgl_prospect'=>'Tgl Prospek',
            'nama_konsumen'=>'Konsumen',
            'nama_product'=>'Produk',
            'type' => 'Type',
            'qty' =>'Qty',
            'format_harga_satuan'=>'Hrg Satuan',
            'total_harga'=>'Total',
            'status'=>"Status"
        );

        $data['page'] = "display";

        return $data;

    }
    function form_detail_prospect($id){
        $this->db->select('*');
        $this->db->where('id_prospect',$id);
        $this->db->join('tbl_konsumen','tbl_konsumen.id_konsumen=tbl_prospect.id_konsumen','left');
        $data['prospect'] = $this->db->get("tbl_prospect")->row_array();
        if(count($data['prospect'])<=0){
            $this->session->set_flashdata("message","Data prospect tidak ditemukan");
            redirect('Prospect/list_prospect');
        }
        $data['product'] = $this->Konsumen_model->getCombo("tbl_product","id_product","CONCAT(nama_product,' ',type)");
        $data['title'] = "Tambah Produk Prospect : <span style='color:#000000 !important;'>".$data['prospect']['nama_konsumen'].'</span>';
        
        $data['save_url']=site_url("Prospect/proses_add_detail_prospect");
        $data['user'] = $this->User_model->getCombo();
        $data['action']="add";
        $data['js_file'] = 'prospect';
        $data['field_primary'] = 'id_detail_prospect';        
        $data['hidden'] = array("id_prospect"=>$id,'id_detail_prospect'=>$this->getNumberIDDetail());
        $data['field'] =array(            
            //'id_prospect'=>'ID Prospek',
            'id_product'=>array('label'=>'Produk',
                            'type'=>'combo',
                            'data'=>$data['product'],
                        ),
            'qty'=>'Qty Produk',
            'type'=>'Type Produk',
            'harga_satuan'=>'Harga Jual Satuan',
        );

        $data['page'] = "form";
        $this->load->view('admin/index', $data);

    }
    function proses_add_detail_prospect(){
        //var_dump($this->form_validation->run() );
        $this->_rulesDetail();
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata("message",validation_errors());
            //die(validation_errors());
            $this->form_detail_prospect($this->input->post("id_prospect"));
        }else{
            $action = $this->input->post("action");
            $data['form'] =array(
                'id_detail_prospect'=>$this->input->post("id_detail_prospect"),
                'id_prospect'=>$this->input->post("id_prospect"),
                'id_product'=>$this->input->post("id_product"),
                'qty'=>$this->input->post("qty"),
                'type'=>$this->input->post("type"),
                'harga_satuan'=>$this->input->post("harga_satuan"),
            );
            if($action=="add"){
               // array_merge($data['form'],array( 'id_user'=>$this->session->userdata('id_user')));
                $insert = $this->db->insert('tbl_prospect_detail',$data['form']);
                if($insert ){
                    $this->session->set_flashdata("message","Data Berhasil disimpan");
                    redirect('Prospect/detail_prospect/'.$this->input->post("id_prospect"));
                }else{
                    $this->form_detail_prospect($this->input->post("id_prospect"));
                }
            }else if($action=="edit"){
                $insert = $this->db->replace('tbl_prospect_detail',$data['form']);
                if($insert ){
                    $this->session->set_flashdata("message","Data Berhasil disimpan");
                    redirect('Prospect/list_prospect');
                }else{
                    $this->form_detail_prospect($this->input->post("id_prospect"));
                }

            }else{
                redirect("Prospect/detail_prospect".$this->input->post("id_prospect"));
            }
        }
    }
    function delete_detail_prospect($prospect,$id){
         $delete = $this->db->delete('tbl_prospect_detail',array('id_detail_prospect'=>$id));
        if($delete ){
            $this->session->set_flashdata("message","Data Berhasil dihapus");
            redirect('Prospect/detail_prospect/'.$prospect);
        }else{
            $this->session->set_flashdata("message","Data Gagal dihapus");
            $this->list_detail_prospect($prospect);
        }
    }

    function getNumberID(){
        $lastKonsumen = $this->db->select('max(id_prospect) last_id')
                        ->from('tbl_prospect')
                        ->get()->row_array();
        return $lastKonsumen['last_id']+1;

    }
    function getNumberIDDetail(){
        $lastKonsumen = $this->db->select('max(id_detail_prospect) last_id')
                        ->from('tbl_prospect_detail')
                        ->get()->row_array();
        return $lastKonsumen['last_id']+1;

    }

    public function _rules() 
    {
    if($this->input->post("action")=="add"){
        $this->form_validation->set_rules('id_prospect', 'ID Prospect', 'trim|required|is_unique[tbl_prospect.id_prospect]',array('is_unique'=>"ID Prospek telah digunakan sebelumnya, silahkan simpan sekali lagi"));
    }
    $this->form_validation->set_rules('tgl_prospect', 'Tgl Prospect', 'trim|required');
    //$this->form_validation->set_rules('user', 'User ', 'trim|required');
    $this->form_validation->set_rules('estimasi_PO', 'Estimasi PO ', 'trim|required');
    $this->form_validation->set_rules('probability', 'Probability ', 'trim|required');
    $this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
    }

    public function _rulesDetail() 
    {
    if($this->input->post("action")=="add"){
    $this->form_validation->set_rules('id_detail_prospect', 'ID Prospect', 'trim|required|is_unique[tbl_prospect_detail.id_detail_prospect]',array('is_unique'=>"ID Detail Prospek telah digunakan sebelumnya, silahkan simpan sekali lagi"));
    }
    $this->form_validation->set_rules('id_prospect', 'ID Prospek', 'trim|required');
    $this->form_validation->set_rules('id_product', 'Product ', 'trim|required');
    $this->form_validation->set_rules('qty', 'Qty ', 'trim|required');
    //$this->form_validation->set_rules('type', 'Type ', 'trim|required');
    $this->form_validation->set_rules('harga_satuan', 'Harga Satuan ', 'trim|required');
    $this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
    }
}