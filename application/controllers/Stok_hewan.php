<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Stok_hewan extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Stok_hewan_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $stok_hewan = $this->Stok_hewan_model->get_all();

        $data = array(
            'stok_hewan_data' => $stok_hewan
        );
		$data = array(
           /*  'datauser_data' => $datauser,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'], */
			 'stok_hewan_data' => $stok_hewan,
			'page'=> 'stok_hewan/tbl_stok_hewan_list',
        );
        $this->load->view('home', $data);

        //$this->load->view('stok_hewan/tbl_stok_hewan_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Stok_hewan_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_depot' => $row->id_depot,
		'nama_depot' => $row->nama_depot,
		'stok_ist' => $row->stok_ist,
		'stok_spr' => $row->stok_spr,
		'stok_a' => $row->stok_a,
		'stok_b' => $row->stok_b,
		'stok_c' => $row->stok_c,
		'stok_d' => $row->stok_d,
	    );
            $this->load->view('stok_hewan/tbl_stok_hewan_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('stok_hewan'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('stok_hewan/create_action'),
	    'id_depot' => set_value('id_depot'),
	    'nama_depot' => set_value('nama_depot'),
	    'stok_ist' => set_value('stok_ist'),
	    'stok_spr' => set_value('stok_spr'),
	    'stok_a' => set_value('stok_a'),
	    'stok_b' => set_value('stok_b'),
	    'stok_c' => set_value('stok_c'),
	    'stok_d' => set_value('stok_d'),
		'page' =>'stok_hewan/tbl_stok_hewan_form',
	    );
        $this->load->view('home', $data);
        
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'stok_ist' => $this->input->post('stok_ist',TRUE),
		'stok_spr' => $this->input->post('stok_spr',TRUE),
		'stok_a' => $this->input->post('stok_a',TRUE),
		'stok_b' => $this->input->post('stok_b',TRUE),
		'stok_c' => $this->input->post('stok_c',TRUE),
		'stok_d' => $this->input->post('stok_d',TRUE),
	    );

            $this->Stok_hewan_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('stok_hewan'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Stok_hewan_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('stok_hewan/update_action'),
				'id_depot' => set_value('id_depot', $row->id_depot),
				'nama_depot' => set_value('nama_depot', $row->nama_depot),
				'stok_ist' => set_value('stok_ist', $row->stok_ist),
				'stok_spr' => set_value('stok_spr', $row->stok_spr),
				'stok_a' => set_value('stok_a', $row->stok_a),
				'stok_b' => set_value('stok_b', $row->stok_b),
				'stok_c' => set_value('stok_c', $row->stok_c),
				'stok_d' => set_value('stok_d', $row->stok_d),
				'page' =>'stok_hewan/tbl_stok_hewan_form',
				);
			$this->load->view('home', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('stok_hewan'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_depot', TRUE));
        } else {
            $data = array(
		'stok_ist' => $this->input->post('stok_ist',TRUE),
		'stok_spr' => $this->input->post('stok_spr',TRUE),
		'stok_a' => $this->input->post('stok_a',TRUE),
		'stok_b' => $this->input->post('stok_b',TRUE),
		'stok_c' => $this->input->post('stok_c',TRUE),
		'stok_d' => $this->input->post('stok_d',TRUE),
	    );

            $this->Stok_hewan_model->update($this->input->post('id_depot', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('stok_hewan'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Stok_hewan_model->get_by_id($id);

        if ($row) {
            $this->Stok_hewan_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('stok_hewan'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('stok_hewan'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('stok_ist', 'stok ist', 'trim|required');
	$this->form_validation->set_rules('stok_spr', 'stok spr', 'trim|required');
	$this->form_validation->set_rules('stok_a', 'stok a', 'trim|required');
	$this->form_validation->set_rules('stok_b', 'stok b', 'trim|required');
	$this->form_validation->set_rules('stok_c', 'stok c', 'trim|required');
	$this->form_validation->set_rules('stok_d', 'stok d', 'trim|required');

	$this->form_validation->set_rules('id_depot', 'id_depot', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Stok_hewan.php */
/* Location: ./application/controllers/Stok_hewan.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-07-26 19:29:25 */
/* http://harviacode.com */