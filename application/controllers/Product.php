<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Product extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model(array('Konsumen_model','User_model','kunjungan_model'));
        $this->load->library(array('form_validation','session'));
		$this->load->library('custom_library');
		if($this->session->userdata('logged_in') != TRUE){
			$data['message'] = "Akses ditolak, silahkan login terlebih dahulu. !!";
			redirect('login');
		}
    }

    function index(){
        $this->list_product();
    }

    function list_product(){
   
        $data['list_data'] = $this->db->select("*,
                            CONCAT(REPLACE(FORMAT(FLOOR(harga_satuan),0),',','.'),',',SUBSTRING_INDEX(FORMAT(harga_satuan,2),'.',-1)) format_harga_satuan")
                            ->from("tbl_product")
                            ->get()
                            ->result_array();
        $data['title'] = "Data Product";
        $data['back_url']=site_url("product/");
        $data['add_url']=site_url("Product/form_product");
        $data['edit_url']=site_url("Product/edit_product");
        $data['detail_url']=site_url("Product/detail_product");
        $data['delete_url']=site_url("Product/delete_product"); 
        $data['field_primary'] = 'id_product';
        $data['field'] =array(
            //'id_product'=>'ID Prduct',
            'nama_product'=>'Nama Product',
            'type'=>'Type',
            'format_harga_satuan'=>'Harga Satuan',
        );

        $data['page'] = "display";
        $this->load->view('admin/index', $data);

    }

    function form_product(){
      
        $data['title'] = "Tambah Product";
        $data['save_url']=site_url("Product/proses_add_product");
        $data['action']="add";
        $data['field_primary'] = 'id_product';
        $data['hidden'] = array('id_product'=>$this->getNumberID());
        $data['field'] =array(           
        //    'id_product'=>'ID Prduct',
            'nama_product'=>'Nama Product',
            'type'=>'Type',
            'harga_satuan'=>'Harga Satuan',
        );

        $data['page'] = "form";
        $this->load->view('admin/index', $data);
    }
    function detail_product($id){
        
        $this->db->select('*');
        $this->db->where('id_product',$id);
        $data['data'] = $this->db->get("tbl_product")->row_array();
        
        
        $data['title'] = "Detail Product";
        $data['save_url']=site_url("Product/proses_add_product");
        $data['action']="add";
        $data['field_primary'] = 'id_product';
        
        $data['field'] =array(            
            'id_product'=>'ID Prduct',
            'nama_product'=>'Nama Product',
            'type'=>'Type',
            'harga_satuan'=>'Harga Satuan',
        );

        $data['page'] = "detail";
        $this->load->view('admin/index', $data);
    }


    function proses_add_product(){
        //var_dump($this->form_validation->run() );
        $this->_rules();
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata("message",validation_errors());
            //die(validation_errors());
            $this->form_product();
        }else{
            $action = $this->input->post("action");
            $data['form'] =array(
                'id_product'=>$this->input->post("id_product"),
                'nama_product'=>$this->input->post("nama_product"),
                'type'=>$this->input->post("type"),
                'harga_satuan'=>$this->input->post("harga_satuan"),
               
            );
            if($action=="add"){
               // array_merge($data['form'],array( 'id_user'=>$this->session->userdata('id_user')));
                $insert = $this->db->insert('tbl_product',$data['form']);
                if($insert ){
                    $this->session->set_flashdata("message","Data Berhasil disimpan");
                    redirect('Product/list_product');
                }else{
                    $this->form_product();
                }
            }else if($action=="edit"){
                $insert = $this->db->replace('tbl_product',$data['form']);
                if($insert ){
                    $this->session->set_flashdata("message","Data Berhasil disimpan");
                    redirect('Product/list_product');
                }else{
                    $this->form_product();
                }

            }else{
                redirect("Product");
            }
        }
    }
    function edit_product($id){
        //$id=$this->input->post("id");
        $this->db->select('*');
        $this->db->where('id_product',$id);
        $data['data'] = $this->db->get("tbl_product")->row_array();
        $data['title'] = "Edit Product";
        $data['save_url']=site_url("Product/proses_add_product");
        $data['action']="edit";
        $data['field_primary'] = 'id_product';
        $data['hidden'] = array("id_product"=>$data['data']['id_product']);
        $data['field'] =array(            
            //'id_product'=>'ID Prduct',
            'nama_product'=>'Nama Product',
            'type'=>'Type',
            'harga_satuan'=>'Harga Satuan',
        );

        $data['page'] = "form";
        $this->load->view('admin/index', $data);
    }

    function delete_product($id){
         
        $delete = $this->db->delete('tbl_product',array('id_product'=>$id));
        if($delete ){
            $this->session->set_flashdata("message","Data Berhasil dihapus");
            redirect('Product/list_product');
        }else{
            $this->session->set_flashdata("message","Data Gagal dihapus");
            $this->list_product();
        }
    }
    function getProduk_JSON($id){
        $this->db->select('*');
        $this->db->where('id_product',$id);
        $data = $this->db->get("tbl_product")->row_array();

        if(count($data) >0){
            $respone = array("message"=>"Load data sukses","data"=>$data,"error"=>"0");
        }else{
            $respone = array("message"=>"Load data gagal","data"=>"","error"=>"1");
        }

        echo json_encode($respone);
    }
    function getNumberID(){
        $lastProduct = $this->db->select('max(id_product) last_id')
                        ->from('tbl_product')
                        ->get()->row_array();
        return $lastProduct['last_id']+1;

    }

    public function _rules() 
    {
    if($this->input->post("action")=="add"){
        $this->form_validation->set_rules('id_product', 'ID Product', 'trim|required|is_unique[tbl_product.id_product]');
    }
    $this->form_validation->set_rules('nama_product', 'Nama Product', 'trim|required');
    $this->form_validation->set_rules('type', 'Type', 'trim|required');
    $this->form_validation->set_rules('harga_satuan', 'Harga Satuan', 'trim|required');
    $this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
    }





























   

}

/* End of file Product.php */
/* Location: ./application/controllers/Product.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2016-08-16 13:25:39 */
/* http://harviacode.com */