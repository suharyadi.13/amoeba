<?php
class Login extends CI_Controller {

	private $limit = 10;

	function __construct()
	{
		parent::__construct();
		#load library dan helper yang dibutuhkan
		$this->load->library(array('table','form_validation','email'));
		$this->load->helper(array('form', 'url','string'));
		$this->load->model('user_model','',TRUE);
		$this->load->model('Dashboard_model','',TRUE);
		$this->load->library('session');
		
	}

	function index($offset = 0, $order_column = 'id', $order_type = 'asc')
	{
		if($this->session->userdata('logged_in') == TRUE){
			redirect('dashboard');
		}
		
		$data['message'] ="";	
		$this->load->view("login",$data);
		
	}
	function validate(){
		$this->_set_rules();
		$data['message'] ="";
		$username = $this->input->post('username');	
		$password = $this->input->post('password');	
		if ($this->form_validation->run() === TRUE){
			$condition = array("username"=>$username,"password"=>$password);
			$DataLogin = $this->user_model->count_all($condition);
			if($DataLogin > 0){
				$DataUser = $this->user_model->get_by_id($condition)->result();
				$ses_data = array(
					'username'  => $DataUser[0]->username,
					'id_user'     => $DataUser[0]->id_user,
					'nama_user'     => $DataUser[0]->nama_user,
					'level'     => $DataUser[0]->level,
					'logged_in' => TRUE
				);
					$this->db->query("INSERT INTO tbl_activity(id_user,action,datetime) VALUES('".$DataUser[0]->id_user."','Login Web','".date("Y-m-d h:i:s")."') ");
					$this->session->set_userdata($ses_data);
					
					$Berita = $this->Dashboard_model->getBerita($DataUser[0]->id_user,$DataUser[0]->level);
					
					if(count($Berita) >0){
						redirect('berita/LoadBeritaWeb');
					}else{
						redirect('konsumen');
					}
			}else{
				$data['message']="Username dan Password tidak sesuai silahkan periksa kembali !! ";
				$this->load->view("login",$data);
			}
		}else{
			$data['message']	="Username dan Password tidak boleh kosong !! ";
			$this->load->view("login",$data);
		}
	}	
	function validateMobile($username,$password){
		
		$data['message'] ="";
		$target = $this->input->get('page');	
		//$password = $this->input->post('password');	
		if ($username!="" and $password!=""){
			$condition = array("username"=>$username,"password"=>$password);
			$DataLogin = $this->user_model->count_all($condition);

			if($DataLogin > 0){
				$DataUser = $this->user_model->get_by_id($condition)->result();

				$ses_data = array(
					'username'  => $DataUser[0]->username,
					'id_user'     => $DataUser[0]->id_user,
					'nama_user'     => $DataUser[0]->nama_user,
					'level'     => $DataUser[0]->level,
					'logged_in' => TRUE
				);
					$this->db->query("INSERT INTO tbl_activity VALUES('','".$DataUser[0]->id_user."','Open via Mobile','',".date("Y-m-d h:i:s")."') ");
					$this->session->set_userdata($ses_data);
					redirect($target);
			}else{
				$data['message']="Username dan Password tidak sesuai silahkan periksa kembali !! ";
				$this->load->view("login",$data);
			}
		}else{
			$data['message']	="Username dan Password tidak boleh kosong !! ";
			$this->load->view("login",$data);
		}
	}
	function logout(){
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('id_user');
		$this->session->unset_userdata('logged_in');
		$data['message'] = "Anda berhasil logout.. terima kasih";
		$this->session->sess_destroy();
		$this->load->view("login",$data);
	}
	
	function _set_rules(){
		$this->form_validation->set_rules('username', 'username', 'required|trim');
		$this->form_validation->set_rules('password', 'Password', 'required');
	}
}