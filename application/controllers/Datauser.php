<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Datauser extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Datauser_model');
        $this->load->library('form_validation');
		$this->load->library('custom_library');
		
		if($this->session->userdata('logged_in') != TRUE){
			$data['message'] = "Akses ditolak, silahkan login terlebih dahulu. !!";
			redirect('login');
		}
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        $dataset = "";
        if ($q <> '') {
            $config['base_url'] = base_url() . 'datauser/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'datauser/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'datauser/index.html';
            $config['first_url'] = base_url() . 'datauser/index.html';
        }

         $config['per_page'] = 10000;
		$condition = array("id_user"=>$this->custom_library->Filter_by_level($this->session->userdata("id_user"),$this->session->userdata("level")));
		
		$config['total_rows'] = $this->Datauser_model->total_rows($q,$condition);
        $datauser_data = $this->Datauser_model->get_limit_data($config['per_page'], $start, $q,$condition);
		
		$maxRow = count($datauser_data);
		foreach ($datauser_data as $datauser)
		{
		//$action = anchor(site_url('datauser/index/'.$datauser->id_user),'<i class="icon_zoom-in_alt"></i>','class="btn btn-primary" title="detail data"'); 
		$action = anchor(site_url('datauser/update/'.$datauser->id_user),'<i class="fa fa-edit"></i>','class="btn btn-success" title="edit data"'); 
		
		$dataset .= "
		['".++$start."','".str_replace("'","",$datauser->nama_user)."' , '".addslashes($datauser->username) ."','".addslashes($datauser->password)."','".addslashes($datauser->level)."','".addslashes($datauser->no_telp)."','".addslashes($action)."']";
			if($start  < ($maxRow)){
				$dataset .=  ',';
			}
		}
		
      /*   $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Datauser_model->total_rows($q,$condition);
        $datauser = $this->Datauser_model->get_limit_data($config['per_page'], $start, $q,$condition);
		var_dump( $this->db->last_query());
        
        $this->pagination->initialize($config); */

        $data = array(
           /*  'datauser_data' => $datauser,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'], */
            'start' => $start,
			'page'=> 'datauser/tbl_user_list',
			'dataset' => $dataset
        );
        $this->load->view('home', $data);
    }

    public function read($id) 
    {
        $row = $this->Datauser_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_user' => $row->id_user,
		'nama_user' => $row->nama_user,
		'username' => $row->username,
		'password' => $row->password,
		'level' => $row->level,
		'no_telp' => $row->no_telp,
		'email' => $row->email,
		'jumlah_sales' => $row->jumlah_sales,
		
	    );
            $this->load->view('datauser/tbl_user_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('datauser'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('datauser/create_action'),
	    'id_user' => set_value('id_user'),
	    'nama_user' => set_value('nama_user'),
	    'username' => set_value('username'),
	    'password' => set_value('password'),
	    'level' => set_value('level'),
		'no_telp' => set_value('no_telp'),
		'email' => set_value('email'),
		'jumlah_sales' => set_value('jumlah_sales'),
	);
        $this->load->view('datauser/tbl_user_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'nama_user' => $this->input->post('nama_user',TRUE),
		'username' => $this->input->post('username',TRUE),
		'password' => $this->input->post('password',TRUE),
		'level' => $this->input->post('level',TRUE),
	    );

            $this->Datauser_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('datauser'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Datauser_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('datauser/update_action'),
				'id_user'=>set_value('nama_user', $row->id_user),
				'username' => set_value('nama_user', $row->username),
				'level' => set_value('nama_user', $row->level),
				'nama_user' => set_value('nama_user', $row->nama_user),
				'password' => set_value('password', $row->password),
				'no_telp' => set_value('no_telp', $row->no_telp),
				'email' => set_value('email', $row->email),
				'jumlah_sales' => set_value('jumlah_sales', $row->jumlah_sales),
				'page' =>'datauser/tbl_user_form',
	    );
            $this->load->view('home', $data);
        } else {
            redirect(site_url('datauser/update/'.$this->session->userdata('id_user')));
            $this->session->set_flashdata('message', 'Record Not Found');
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_user', TRUE));
        } else {
            $data = array(
		'nama_user' => $this->input->post('nama_user',TRUE),
		//'username' => $this->input->post('username',TRUE),
		'password' => $this->input->post('password',TRUE),
		//'level' => $this->input->post('level',TRUE),
	    );

            $this->Datauser_model->update($this->input->post('id_user', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('datauser/update/'.$this->input->post('id_user', TRUE)));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Datauser_model->get_by_id($id);

        if ($row) {
            $this->Datauser_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('datauser'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('datauser'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('nama_user', 'nama user', 'trim|required');
	//$this->form_validation->set_rules('username', 'username', 'trim|required');
	$this->form_validation->set_rules('password', 'password', 'trim|required');
	//$this->form_validation->set_rules('level', 'level', 'trim|required');

	$this->form_validation->set_rules('id_user', 'id_user', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Datauser.php */
/* Location: ./application/controllers/Datauser.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2016-08-17 13:52:41 */
/* http://harviacode.com */