<?php



if (!defined('BASEPATH')) exit('No direct script access allowed');



class Dashboard extends CI_Controller

{

    function __construct()

    {

        parent::__construct();
        $this->load->model(array('Datauser_model','Dashboard_model','transaksi_model','User_model'));
        $this->load->library(array('form_validation','custom_library','table'));
		$this->load->helper('date');
		if($this->session->userdata('logged_in') != TRUE){

			$data['message'] = "Akses ditolak, silahkan login terlebih dahulu. !!";

			redirect('login');

		}

    }



    public function index()

    {
        $year = date("Y");
        $data['tahun'] = $year ;
    	$data['title'] = "Dashboard Data";
    	$data['page'] = "dashboard/data_dashboard";
        $data['bulan'] = $this->custom_library->ShortNamaBulan;
        $data['tw'] = array('tw1','tw2','tw3');
        $procPencapaian = "CALL data_pencapaian(?)";
        $procPencapaianTW = "CALL data_pencapaian_tw(?)";
        $dataProc = array('tahun' => $year); 
        $pencapaian = $this->db->query($procPencapaian, $dataProc);
        mysqli_next_result( $this->db->conn_id );
        $pencapaiantw = $this->db->query($procPencapaianTW, $dataProc);
        mysqli_next_result( $this->db->conn_id );
         
        $data['pencapaian'] = $pencapaian->result_array();
        $data['pencapaian_tw'] = $pencapaiantw->result_array();

        $data['data_total'] = $this->Dashboard_model->getDashboarData($year)->result_array();
        $data['grafik_total_omzet'] = $this->Dashboard_model->getGrafikOmzet($year)->result_array();
        $data['sales_total_omzet'] = $this->Dashboard_model->getTopSalesOmzet($year)->result_array();
        $data['sales_top_product'] = $this->Dashboard_model->getTopProduct($year)->result_array();
		$this->load->view("admin/index",$data);

	}

    function chart(){
        $year = date("Y");
        $data['start'] = ($this->input->post('start')!="")?$this->input->post('start'):date("y-m-d");
        $data['end'] = ($this->input->post('end')!="")?$this->input->post('end'):date("y-m-d");
        $data['tahun'] = date("Y",strtotime($data['start']))." ".date("Y",strtotime($data['end']));
        $data['bulan'] = $this->custom_library->ShortNamaBulan;
        $data['title'] = "Data Pencapaian Sales";
        $data['page'] = "dashboard/grafik_sales";
        $data['user'] = (array)$this->User_model->get_all();
        foreach ($data['user'] as $key => $value) {
            $data['grafik_total_omzet_alluser'][$value->id_user] = $this->Dashboard_model->getGrafikOmzetRange($data['start'],$data['end'],$value->id_user)->result_array();
        }
        

        $this->load->view("admin/index",$data);
    }

}