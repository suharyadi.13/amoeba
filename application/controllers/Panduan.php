<?php

if (!defined('BASEPATH'))  exit('No direct script access allowed');

class Panduan extends CI_Controller
{
    public $FormatLap = "kode agen#No Faktur#Sapi-Domba-1/7#ist/spr/A/B/C/D#Jumlah#harga#Nama Konsumen#Telp Konsumen#Alamat Konsumen#tgl Transaksi#PL/PS";
    
    function __construct()
    {
        parent::__construct();
        $this->load->model('Table_model');
        $this->load->helper('form');
    }
    public function index()
    {
    	$this->load->view("panduan_sales");
    }
}