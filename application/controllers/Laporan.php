<?php

if (!defined('BASEPATH'))  exit('No direct script access allowed');

class Laporan extends CI_Controller
{
    public $FormatLap = "kode agen#No Faktur#Sapi-Domba-1/7#ist/spr/A/B/C/D#Jumlah#harga#Nama Konsumen#Telp Konsumen#Alamat Konsumen#tgl Transaksi#PL/PS#depot";
    public $FormatReg = "id#Agen#Nama#posisi";

    function __construct()
    {
        parent::__construct();
        $this->load->model('Table_model');
        $this->load->helper('form');
        $this->load->library(array('form_validation'));
        $this->load->library(array('Custom_library','session'));
    }

    public function index()
    {
        $Laporan = $this->Table_model->get_all()->result_array();
        $data['field'] = $this->PopulateField($this->FormatLap);
        $data['field'] = array_merge(array("sales"=>"Nama Sales"),$data['field']);
        $data['field'] = array_merge(array("no_telp"=>"no_telp"),$data['field']);
        $data['title'] = "Data Laporan Transaksi Masuk";
        $data['UrlLoadData'] = base_url("laporan/LoadDataLaporan");

        //load depot
        $this->Table_model->table="tbl_depot";
        $data['data_depot'] = $this->Table_model->get_all()->result_array();


        $this->load->view('laporan', $data);
    }
    public function LoadDataLaporan()
    {
        $validateBrowser = $this->isMobile();
        if($validateBrowser){
            $apiWa = 'https://api.whatsapp.com/';
        }else{
            $apiWa = 'https://web.whatsapp.com/';
        }
        
        $condition =array("status"=>0);
        $level=$this->custom_library->Filter_by_level($this->session->userdata("id_user"),$this->session->userdata("level"));

        //$Laporan = $this->Table_model->get_all($condition,$level)->result_array();
        $Laporan = $this->db->query("SELECT
								tbl_laporan.id,
								tbl_laporan.message,
								tbl_laporan.from_number,
								tbl_laporan.`status`,
								tbl_laporan.datetime,
								view_register.Agen,
								view_register.Nama,
								view_register.posisi,
								view_register.telp,
								view_register.datetime datetime_register
								FROM
								tbl_laporan
								LEFT JOIN view_register ON view_register.telp = tbl_laporan.from_number
								WHERE
								tbl_laporan.message like '".$level."%' AND tbl_laporan.status=0")->result_array();
       // $data['field'] = explode("#",$this->FormatLap);
        $DataLaporanAll = array();
        if(count($Laporan)>0){
            foreach($Laporan as $index =>$item){
                $DataLaporan = $this->FetchDataLap($item);
                $messageChat = "Saya mau bertanya tentang transaksi ini , ".$this->readData($DataLaporan);
                $item['from_number'] = str_replace("+","",$item['from_number']);

                $DataLaporan = array_merge($DataLaporan,array('no_telp'=>'<a class="btn btn-success" href="'.$apiWa.'send?phone='.$item['from_number'].'&text='.urlencode($messageChat).'" target="_blank" title="Chat sales"><i class="fab fa-whatsapp"></i></a> <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalForm" data-id="'.$item['id'].'" data-whatever="@'.$DataLaporan['Nama_Konsumen'].'" onClick="LoadDataForm(\''.$item['id'].'\')"  title="Detail Data"><i class="fas fa-search-plus"></i></button> <a class="btn sm-btn btn-danger" href="'.base_url('laporan/delete/'.$item['id']).'"  title="Delete Data" onClick="return konfirm()"><i class="fas fa-minus"></i></a>'));

                $DataLaporan  = array_merge(array("sales"=>$item['Nama']),$DataLaporan);

                $DataLaporanAll[] =   (object) $DataLaporan;
            }
            $data['data'] = $DataLaporanAll;
        }else{
            $data['message'] ="Data Laporan Kosong";
        }
        // echo "<pre>";
        // var_dump($data);
        // die();
        $data["options"]=array();
        $data["files"]=array();
        $data["draw"]= 100;
        $data["recordsTotal"]=count($Laporan);
        $data["recordsFiltered"]=count($Laporan);;
        echo json_encode($data);
        
    }

    public function LoadMessage()
    {
        //$Laporan = $this->Table_model->get_all()->result_array();
        $data['field'] = $this->PopulateField($this->FormatLap);
        $data['field'] = array_merge(array("sales"=>"Nama Sales"),$data['field']);
        $data['field'] = array_merge(array("no_terdaftar"=>"no_terdaftar"),$data['field']);
        $data['field'] = array_merge(array("no_telp"=>"no_telp"),$data['field']);
    
        $data['title'] = "Seluruh Data Masuk";
        $data['UrlLoadData'] = base_url("laporan/LoadDataMessage");

        //load depot
        $this->Table_model->table="tbl_depot";
        $data['data_depot'] = $this->Table_model->get_all()->result_array();


        $this->load->view('laporan', $data);
    }
    public function LoadDataMessage()
    {
        $validateBrowser = $this->isMobile();
        if($validateBrowser){
            $apiWa = 'https://api.whatsapp.com/';
        }else{
            $apiWa = 'https://web.whatsapp.com/';
        }
        
        //$condition =array("status"=>0);
        //$level=$this->custom_library->Filter_by_level($this->session->userdata("id_user"),$this->session->userdata("level"));

        //$Laporan = $this->Table_model->get_all($condition,$level)->result_array();
        $Laporan = $this->db->query("SELECT
									tbl_laporan.id,
									tbl_laporan.message,
									tbl_laporan.from_number,
									tbl_laporan.`status`,
									tbl_laporan.datetime,
									view_register.Agen,
									view_register.Nama,
									view_register.posisi,
									view_register.telp
									FROM
									tbl_laporan
									LEFT JOIN view_register ON view_register.telp = tbl_laporan.from_number ")->result_array();
       // $data['field'] = explode("#",$this->FormatLap);
        $DataLaporanAll = array();
        if(count($Laporan)>0){
            foreach($Laporan as $index =>$item){
                $DataLaporan = $this->FetchDataLap($item);
                $messageChat = "Saya mau bertanya tentang transaksi ini , ".$this->readData($DataLaporan);
                $item['from_number'] = str_replace("+","",$item['from_number']);

                $DataLaporan = array_merge($DataLaporan,array('no_telp'=>'<a class="btn btn-success" href="'.$apiWa.'send?phone='.$item['from_number'].'&text='.urlencode($messageChat).'" target="_blank" title="Chat sales"><i class="fab fa-whatsapp"></i></a> <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalForm" data-id="'.$item['id'].'" data-whatever="@'.$DataLaporan['Nama_Konsumen'].'" onClick="LoadDataForm(\''.$item['id'].'\')"  title="Detail Data"><i class="fas fa-search-plus"></i></button> <a class="btn sm-btn btn-danger" href="'.base_url('laporan/delete/'.$item['id']).'"  title="Delete Data" onClick="return konfirm()"><i class="fas fa-minus"></i></a>','no_terdaftar'=>$item['from_number']));

                $DataLaporan  = array_merge(array("sales"=>$item['Nama']),$DataLaporan);

                $DataLaporanAll[] =   (object) $DataLaporan;
            }
            $data['data'] = $DataLaporanAll;
        }else{
            $data['message'] ="Data Laporan Kosong";
        }
        // echo "<pre>";
        // var_dump($data);
        // die();
        $data["options"]=array();
        $data["files"]=array();
        $data["draw"]= 100;
        $data["recordsTotal"]=count($Laporan);
        $data["recordsFiltered"]=count($Laporan);;
        echo json_encode($data);
        
    }

    function PopulateField($fieldData){
        $field = explode("#",$fieldData);
        $dataField = array();
        foreach($field as $index=>$item){
            $dataField[str_replace(" ","_",str_replace("/","_",$item))] = $item;
        }

        return $dataField;
    }

    function FetchDataLap($dataItem,$fieldData ="" ){
        if($fieldData ==""){
           $fieldData = $this->FormatLap;
        }
        $data = $dataItem['message'];
        $field = $this->PopulateField($fieldData);
        $dataFetch = explode("#",$data);
        $FieldData = array();
        $rowNum = 0;
        foreach($field as $index=>$item){
            if(isset($dataFetch[$rowNum])){
                $FieldData = array_merge($FieldData,array($index=>$dataFetch[$rowNum]));
            }else{
                $FieldData = array_merge($FieldData,array($index=>""));
            }
            $rowNum++;
        }

        return $FieldData;
    }

    function readData($data){
        if(is_array($data)){
            $dataReaded = "";
            foreach($data as $index=>$item){
                $dataReaded .= $index.":".$item.",";
            }
            return $dataReaded;
        }else{
            return "";
        }
    }

    function delete($id){
    	if($this->db->query("DELETE FROM tbl_laporan where id='".$id."'")){
    		$this->session->set_flashdata("Delete data succes");
    	}

    	redirect("laporan");
    }
    
    function SaveTransaksi(){
        $message = array("message"=>"","error"=>"1","data"=>"");
        $data['field'] = $this->PopulateField($this->FormatLap);
        
        $Kelas = strtolower($this->input->post("ist_spr_A_B_C_D"));
        $jmlhHewan = strtolower($this->input->post("Jumlah"));

        $this->form_validation->set_error_delimiters('', '');
        $this->form_validation->set_rules('Jumlah','Jumlah Hewan','trim|required|strtolower|greater_than[0]|less_than[100]',array(
                'required'      => '%s tidak boleh kosong.',
                'greater_than'     => '%s tidak boleh kurang dari 1',
                'less_than' => 'jumlah tidak boleh lebih dari 100'
        ));
        $this->form_validation->set_rules('ist_spr_A_B_C_D','Kelas Hewan','trim|required|strtolower|in_list[a,b,c,d,spr,ist]',array(
                'required'      => '%s tidak boleh kosong.',
                'in_list'     => '%s tidak dikenali, coba cek lagi harus sesuai format'
        ));
        $this->form_validation->set_rules('Sapi-Domba-1_7','Jenis Hewan','trim|required|strtolower|in_list[sapi,domba,1/7]',array(
                'required'      => '%s tidak boleh kosong.',
                'in_list'     => '%s tidak dikenali, coba cek lagi harus sesuai format'
        ));
        //var_dump($this->form_validation->run());
        //die();

        if ($this->form_validation->run() == FALSE)
        {
                $message = array("message"=>$this->form_validation->error_string(),"error"=>"1","data"=>"");
        }
        else
        {
            
        $validateTrx = $this->validateTrx();            
        if($validateTrx['error'] == 0){
            $data =array();
            $query = "INSERT INTO tbl_konsumen (id_user,nama_konsumen,telp_konsumen,alamat_konsumen,status,jenis_transaksi,jenis_order,rencana_kelas) VALUES('".$this->input->post("kode_agen")."','".$this->input->post("Nama_Konsumen")."','".$this->input->post("Telp_Konsumen")."','".$this->input->post("Alamat_Konsumen")."','DEAL','".$this->input->post("PL_PS")."','".$this->input->post("Sapi-Domba-1_7")."','".$this->input->post("ist_spr_A_B_C_D")."')";
            $inputKonsumen = $this->db->query($query);
            if($inputKonsumen){
                $lastKonsumenId = $this->db->insert_id();
                if($lastKonsumenId !=""){
                	$Jnstransaksi = ($this->input->post("PL_PS")!=""?$this->input->post("PL_PS"):"PL");
                    $tanggal_transaksi = ($this->input->post("tgl_Transaksi")!=""?$this->input->post("tgl_Transaksi"):date("Y-m-d"));
                    $queryLap = "INSERT INTO tbl_transaksi (id_konsumen,id_user,no_faktur,jenis_order,jenis_transaksi,kelas_".strtolower($this->input->post("ist_spr_A_B_C_D")).",harga_".strtolower($this->input->post("ist_spr_A_B_C_D")).",depot,sales,tanggal_transaksi) VALUES('".$lastKonsumenId."','".$this->input->post("kode_agen")."','".$this->input->post("No_Faktur")."','".$this->input->post("Sapi-Domba-1_7")."','".$Jnstransaksi."','".$this->input->post("Jumlah")."','".$this->input->post("harga")."','".$this->input->post("depot")."','".$this->input->post("sales")."','".$tanggal_transaksi."')";



                    if($this->db->query($queryLap)){
                        $queryUpdateLaporan = $this->db->query("UPDATE tbl_laporan SET status='1' WHERE id='".$this->input->post("id_laporan")."' ");
                    };
                    $message = array("message"=>"Data Berhasil disimpan","error"=>"0","data"=>"");
                }else{
                    $message = array("message"=>"Id konsumen tidak dikenali","error"=>"1","data"=>"");
                }
            }else{
                $message = array("message"=>"Gagal input konsumen","error"=>"1","data"=>"");
            }
        }else{
            $message = array("message"=>$validateTrx['message'],"error"=>"1","data"=>"");
        }
        }
        

        echo json_encode($message);

        
    }
    function validateTrx(){
        $message = array("message"=>"","error"=>"1","data"=>"");

        $cekAgen = $this->db->query("SELECT * FROM tbl_user WHERE id_user='".$this->input->post("kode_agen")."' ")->result();
        
        if(count($cekAgen)>0){
            $cekFaktur = $this->db->query("SELECT * FROM tbl_transaksi WHERE no_faktur='".$this->input->post("No_Faktur")."' AND jenis_order = '".$this->input->post("Sapi-Domba-1_7")."' AND kelas_".strtolower($this->input->post("ist_spr_A_B_C_D"))." = '".$this->input->post("Jumlah")."' ")->result();
            if(count($cekFaktur)>0){
                $message = array("message"=>"No Faktur telah diinput sebelumnya","error"=>"1","data"=>"");
            }else{
                $message = array("message"=>"Transaksi validated","error"=>"0","data"=>"");    
            }

        }else{
            $message = array("message"=>"Kode Agen tidak dikenali","error"=>"1","data"=>"");
        }

        return $message;
    }


    public function Register()
    {
        $data['title'] = "Data Registrasi";

        //$Register = $this->db->query("SELECT * FROM `tbl_laporan` WHERE SUBSTR(message FROM 1 FOR 3) ='Reg'")->result_array();

        $data['field'] = $this->PopulateField($this->FormatReg);
        $data['UrlLoadData'] = base_url("laporan/LoadDataRegister");

        //load depot
        $this->Table_model->table="tbl_depot";
        $data['data_depot'] = $this->Table_model->get_all()->result_array();


        $this->load->view('laporan', $data);
    }

    function LoadDataRegister(){
        $level=$this->custom_library->Filter_by_level($this->session->userdata("id_user"),$this->session->userdata("level"));
        $Laporan = $this->db->query("SELECT * FROM `view_register` where agen like '".$level."%'")->result_array();
        
        $DataLaporanAll = array();
        if(count($Laporan)>0){
            foreach($Laporan as $index =>$item){
                //$DataLaporan = $this->FetchDataLap($item,$this->FormatReg);
                $messageChat = "Saya mau bertanya  , "; //$this->readData($DataLaporan);
                //$item['from_number'] = str_replace("+","",$item['telp']);

                //$DataLaporan = array_merge($DataLaporan,array('no_telp'=>'<a class="btn btn-success" href="https://api.whatsapp.com/send?phone='.$item['telp'].'&text='.urlencode($messageChat).'" target="_blank" title="Chat sales"><i class="fab fa-whatsapp"></i></a> <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalForm" data-id="'.$item['id'].'"  onClick="LoadDataForm(\''.$item['id'].'\')"  title="Detail Data"><i class="fas fa-search-plus"></i></button> <a class="btn sm-btn btn-danger" href="'.base_url('laporan/delete/'.$item['id']).'"  title="Delete Data" onClick="return konfirm()"><i class="fas fa-minus"></i></a>'));

                 
                $DataLaporanAll[] =   (object) $item;
            }
            $data['data'] = $DataLaporanAll;
        }else{
            $data['message'] ="Data Laporan Kosong";
        }
        // echo "<pre>";
        // var_dump($data);
        // die();
        $data["options"]=array();
        $data["files"]=array();
        $data["draw"]= 100;
        $data["recordsTotal"]=count($Laporan);
        $data["recordsFiltered"]=count($Laporan);;
        echo json_encode($data);
    }

    public function callback_kelas($str)
    {
        $kelas = array('A','B','C','D','SPR','IST');    
            if (!in_array($str, $kelas))
            {
                    $this->form_validation->set_message('callback_kelas', 'Kolom {field} tidak sesuai');
                    return FALSE;
            }
            else
            {
                    return TRUE;
            }
    }

    function isMobile () {
      return is_numeric(strpos(strtolower($_SERVER['HTTP_USER_AGENT']), "mobile"));
    }

}