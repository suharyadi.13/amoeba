<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Konsumen extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model(array('Konsumen_model','User_model','kunjungan_model'));
        $this->load->library(array('form_validation','session'));
		$this->load->library('custom_library');
		if($this->session->userdata('logged_in') != TRUE){
			$data['message'] = "Akses ditolak, silahkan login terlebih dahulu. !!";
			redirect('login');
		}
    }

    function index(){
        $this->list_konsumen();
    }

    function list_konsumen(){
        $condition = array("tbl_konsumen.id_user"=>$this->custom_library->Filter_by_level($this->session->userdata("id_user"),$this->session->userdata("level")));

        //$this->db->select('*');
        foreach($condition as $field=>$val){
            $this->db->like($field,$val,'after');
        }
        $data['list_data'] = $this->db->select('*')
                            ->from("tbl_konsumen")
                            ->join("tbl_segmen","tbl_segmen.id_segmen = tbl_konsumen.id_segmen","left")
                            ->join("tbl_area","tbl_area.id_area = tbl_konsumen.id_area","left")
                            ->join("tbl_user","tbl_user.id_user = tbl_konsumen.id_user","left")
                            ->get()
                            ->result_array();
        $data['title'] = "Data Konsumen";
        $data['back_url']=site_url("konsumen/");
        $data['add_url']=site_url("Konsumen/form_konsumen");
        $data['edit_url']=site_url("Konsumen/edit_konsumen");
        $data['detail_url']=site_url("Konsumen/detail_konsumen");
        $data['delete_url']=site_url("Konsumen/delete_konsumen"); 
        $data['field_primary'] = 'id_konsumen';
        $data['field'] =array(
            'nama_perusahaan'=>'Nama Konsumen',
            'no_kontak'=>'No Kontak',
            'email'=>'Email',
            'nama_segmen'=>'Segmentasi',
            'nama_area'=>'Area', 
            'nama_konsumen'=>'Kontak Person',
            'nama_user'=>'Sales',
        );

        $data['page'] = "display";
        $this->load->view('admin/index', $data);

    }

    function form_konsumen(){
       
        $data['segemen'] = $this->Konsumen_model->getCombo("tbl_segmen","id_segmen","nama_segmen");
        $data['area'] = $this->Konsumen_model->getCombo("tbl_area","id_area","nama_area");
        $data['user'] = $this->User_model->getCombo();
        $data['title'] = "Tambah Konsumen";
        $data['save_url']=site_url("Konsumen/proses_add_konsumen");
        $data['action']="add";
        $data['field_primary'] = 'id_konsumen';
        $data['hidden'] = array('id_konsumen'=>$this->getNumberID());
        $data['field'] =array(            
            //'id_konsumen'=>'ID Konsumen',
            'nama_konsumen'=>'Nama Konsumen',
            'nama_perusahaan'=>'Nama Perusahaan',
            'no_kontak'=>'No Kontak', 
            'email'=>'Email',
            'alamat'=>'Alamat',
            'id_segmen'=>array('label'=>'Segmen',
                            'type'=>'combo',
                            'data'=>$data['segemen'],
                        ),
            'id_area'=>array('label'=>'Area',
                            'type'=>'combo',
                            'data'=>$data['area'],
                        ),
            'alamat'=>'Alamat', 
            'id_user'=>array('label'=>'User',
                            'type'=>'combo',
                            'data'=>$data['user'],
                        )
        );

        $data['page'] = "form";
        $this->load->view('admin/index', $data);
    }
    function detail_konsumen($id){
        
        $this->db->select('*');
        $this->db->where('id_konsumen',$id);
        $this->db->join('tbl_user','tbl_user.id_user = tbl_konsumen.id_user','left');
        $this->db->join('tbl_segmen','tbl_segmen.id_segmen = tbl_konsumen.id_segmen','left');
        $this->db->join('tbl_area','tbl_area.id_area = tbl_konsumen.id_area','left');
        $data['data'] = $this->db->get("tbl_konsumen")->row_array();
        
        $data['title'] = "Detail Konsumen";
        $data['back_url']=site_url("Konsumen/");
        $data['save_url']=site_url("Konsumen/proses_add_konsumen");
        $data['action']="add";
        $data['field_primary'] = 'id_konsumen';
        
        $data['field'] =array(            
            'id_konsumen'=>'ID Konsumen',
            'nama_konsumen'=>'Nama Konsumen',
            'nama_perusahaan'=>'Nama Perusahaan',
            'nama_segmen'=>'Segmen',            
            'no_kontak'=>'No Kontak',
            'email'=>'Email',
            'nama_area'=>'Area',
            'alamat'=>'Alamat',
            'nama_user'=>'Sales'
        );

        $data['page'] = "detail";
        $this->load->view('admin/index', $data);
    }


    function proses_add_konsumen(){
        //var_dump($this->form_validation->run() );
        $this->_rules();
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata("message",validation_errors());
            //die(validation_errors());
            $this->form_konsumen();
        }else{
            $action = $this->input->post("action");
            $data['form'] =array(
                'nama_konsumen'=>$this->input->post("nama_konsumen"),
                'nama_perusahaan'=>$this->input->post("nama_perusahaan"), 
                'no_kontak'=>$this->input->post("no_kontak"), 
                'email'=>$this->input->post("email"), 
                'id_segmen'=>$this->input->post("id_segmen"),
                'id_area'=>$this->input->post("id_area"),
                'alamat'=>$this->input->post("alamat"),
                'id_user'=>$this->input->post("id_user"),
               
            );
            if($action=="add"){
               // array_merge($data['form'],array( 'id_user'=>$this->session->userdata('id_user')));
                $insert = $this->db->insert('tbl_konsumen',$data['form']);
                if($insert ){
                    $this->session->set_flashdata("message","Data Berhasil disimpan");
                    redirect('Konsumen/list_konsumen');
                }else{
                    $this->form_konsumen();
                }
            }else if($action=="edit"){
                $insert = $this->db->replace('tbl_prospect',$data['form']);
                if($insert ){
                    $this->session->set_flashdata("message","Data Berhasil disimpan");
                    redirect('Konsumen/list_konsumen');
                }else{
                    $this->form_konsumen();
                }

            }else{
                redirect("Konsumen");
            }
        }
    }
    function edit_konsumen($id){
        //$id=$this->input->post("id");
        $this->db->select('*');
        $this->db->where('id_konsumen',$id);
        $data['data'] = $this->db->get("tbl_konsumen")->row_array();
        $data['segemen'] = $this->Konsumen_model->getCombo("tbl_segmen","id_segmen","nama_segmen");
        $data['area'] = $this->Konsumen_model->getCombo("tbl_area","id_area","nama_area");
        $data['user'] = $this->User_model->getCombo();
        $data['title'] = "Edit Konsumen";
        $data['save_url']=site_url("Konsumen/proses_add_konsumen");
        $data['action']="edit";
        $data['field_primary'] = 'id_konsumen';
        $data['hidden'] = array("id_konsumen"=>$data['data']['id_konsumen']);
        $data['field'] =array(            
            'nama_konsumen'=>'Nama Konsumen',
            'nama_perusahaan'=>'Nama Perusahaan',
            'no_kontak'=>'No Kontak',
            'email'=>'Email',
            'id_segmen'=>array('label'=>'Segmen',
                            'type'=>'combo',
                            'data'=>$data['segemen'],
                        ),
            'id_area'=>array('label'=>'Area',
                            'type'=>'combo',
                            'data'=>$data['area'],
                        ), 
            'alamat'=>'Alamat',
             'id_user'=>array('label'=>'Sales',
                            'type'=>'combo',
                            'data'=>$data['user'],
                        )
        );

        $data['page'] = "form";
        $this->load->view('admin/index', $data);
    }

    function delete_konsumen($id){
         
        $delete = $this->db->delete('tbl_konsumen',array('id_konsumen'=>$id));
        if($delete ){
            $this->session->set_flashdata("message","Data Berhasil dihapus");
            redirect('Konsumen/list_konsumen');
        }else{
            $this->session->set_flashdata("message","Data Gagal dihapus");
            $this->list_konsumen();
        }
    }
    function getNumberID(){
        $lastKonsumen = $this->db->select('max(id_konsumen) last_id')
                        ->from('tbl_konsumen')
                        ->get()->row_array();
        return $lastKonsumen['last_id']+1;

    }

    public function _rules() 
    {
    if($this->input->post("action")=="add"){
        $this->form_validation->set_rules('id_konsumen', 'ID Konsumen', 'trim|required|is_unique[tbl_konsumen.id_konsumen]');
    }
    $this->form_validation->set_rules('nama_konsumen', 'Nama Konsumen', 'trim|required');
    $this->form_validation->set_rules('nama_perusahaan', 'Nama Perusahaan', 'trim|required');
    $this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
    }































    /*

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'konsumen/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'konsumen/index.html?q=' . urlencode($q);
        }else{
            $config['base_url'] = base_url() . 'konsumen/index.html';
            $config['first_url'] = base_url() . 'konsumen/index.html';
        }

        $config['per_page'] = 10000;
		$condition = array("id_user"=>$this->custom_library->Filter_by_level($this->session->userdata("id_user"),$this->session->userdata("level")));
		
		
        $konsumen_data = $this->Konsumen_model->get_limit_data($config['per_page'], $start, $q,$condition);
		
		$start = 0;
		$dataset ="";
		$maxRow = count($konsumen_data);
		foreach ($konsumen_data as $konsumen)
		{
		$action = anchor(site_url('kunjungan/index/'.$konsumen->id_konsumen),'<i class="icon_zoom-in_alt"></i>','class="btn btn-primary" title="detail data"'); 
		$action .= anchor(site_url('konsumen/update/'.$konsumen->id_konsumen),'<i class="fa fa-edit"></i>','class="btn btn-success" title="edit data"'); 
		$action .= anchor(site_url('konsumen/delete/'.$konsumen->id_konsumen),'<i class="icon_close_alt2"></i>','class="btn btn-warning" onclick="javasciprt: return confirm(\'Anda yakin akan menghapus data ?\')" title="delete data"'); 
		
		$dataset .= "
		['".++$start."','".$konsumen->id_user.' : '.addslashes($konsumen->nama_user) ."','".addslashes($konsumen->nama_konsumen)."','".addslashes($konsumen->telp_konsumen)."','".$konsumen->status."','".$konsumen->jenis_order."','".$konsumen->jenis_transaksi."','".addslashes($action)."']";
			if($start  < ($maxRow)){
				$dataset .=  ',';
			}
		}

        //$this->load->library('pagination');
        //$this->pagination->initialize($config);

        $data = array(
            //'konsumen_data' => $konsumen,
            //'q' => $q,
            //'pagination' => $this->pagination->create_links(),
            //'total_rows' => $config['total_rows'],
            'start' => $start,
			'page'=> 'konsumen/tbl_konsumen_list',
			'dataset' => $dataset
        );
        
		$this->load->view('home', $data);
    }

    public function read($id) 
    {
        $row = $this->Konsumen_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_konsumen' => $row->id_konsumen,
		'id_user' => $row->id_user,
		'nama_konsumen' => $row->nama_konsumen,
		'telp_konsumen' => $row->telp_konsumen,
		'alamat_konsumen' => $row->alamat_konsumen,
		'email_konsumen' => $row->email_konsumen,		
        'status' => $row->status,
		'jenis_pelanggan' => $row->jenis_pelanggan,
		'jenis_transaksi' => $row->jenis_transaksi,
		'jenis_order' => $row->jenis_order,
		'page'=> 'konsumen/tbl_konsumen_read'
        );
			$this->load->view('home', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('konsumen'));
        }
    }

    public function create() 
    {
		$condition = array("id_user"=>$this->custom_library->Filter_by_level($this->session->userdata("id_user"),$this->session->userdata("level")));
		
        $data = array(
            'button' => 'Create',
			'action' => site_url('konsumen/create_action'),
			'id_konsumen' => set_value('id_konsumen'),
			'id_user' => set_value('id_user'),
			'nama_konsumen' => set_value('nama_konsumen'),
			'telp_konsumen' => set_value('telp_konsumen'),
			'alamat_konsumen' => set_value('alamat_konsumen'),			'email_konsumen' => set_value('email_konsumen'),
			'status' => set_value('status'),
			'jenis_pelanggan' => set_value('jenis_pelanggan'),
			'jenis_transaksi' => set_value('jenis_transaksi'),
			'jenis_order' => set_value('jenis_order'),
			'page'=> 'konsumen/tbl_konsumen_form',
			'user' =>(array)$this->User_model->get_all($condition,1)
        );
		
		$this->load->view('home', $data);
        
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'id_user' => $this->input->post('id_user',TRUE),
		'nama_konsumen' => $this->input->post('nama_konsumen',TRUE),
		'telp_konsumen' => $this->input->post('telp_konsumen',TRUE),		'email_konsumen' => $this->input->post('email_konsumen'),
		'alamat_konsumen' => $this->input->post('alamat_konsumen',TRUE),		
		'jenis_pelanggan' => $this->input->post('jenis_pelanggan',TRUE),
			'datetime'=> date("y-m-d h:i:s")		
	    );

            $this->Konsumen_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('konsumen'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Konsumen_model->get_by_id($id);
		
		$condition = array("id_user"=>$this->custom_library->Filter_by_level($this->session->userdata("id_user"),$this->session->userdata("level")));
        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('konsumen/update_action'),
				'id_konsumen' => set_value('id_konsumen', $row->id_konsumen),
				'id_user' => set_value('id_user', $row->id_user),
				'nama_konsumen' => set_value('nama_konsumen', $row->nama_konsumen),
				'telp_konsumen' => set_value('telp_konsumen', $row->telp_konsumen),	
				'alamat_konsumen' => set_value('alamat_konsumen', $row->alamat_konsumen),				'email_konsumen' => set_value('email_konsumen', $row->email_konsumen),
				'jenis_pelanggan' => set_value('jenis_pelanggan', $row->jenis_pelanggan),
				'user' =>(array)$this->User_model->get_all($condition,1),
				'page'=> 'konsumen/tbl_konsumen_form'
        );
			$this->load->view('home', $data);
        
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('konsumen'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_konsumen', TRUE));
        } else {
            $data = array(
		'id_user' => $this->input->post('id_user',TRUE),
		'nama_konsumen' => $this->input->post('nama_konsumen',TRUE),
		'telp_konsumen' => $this->input->post('telp_konsumen',TRUE),
		'alamat_konsumen' => $this->input->post('alamat_konsumen',TRUE),		'email_konsumen' => $this->input->post('email_konsumen'),
		'jenis_pelanggan' => $this->input->post('jenis_pelanggan',TRUE),
	    );

            $this->Konsumen_model->update($this->input->post('id_konsumen', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('konsumen'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Konsumen_model->get_by_id($id);

        if ($row) {
            $this->Konsumen_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('konsumen'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('konsumen'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('nama_konsumen', 'nama konsumen', 'trim|required');
	//$this->form_validation->set_rules('telp_konsumen', 'telp konsumen', 'trim|required');
	$this->form_validation->set_rules('alamat_konsumen', 'alamat konsumen', 'trim|required');
	$this->form_validation->set_rules('jenis_pelanggan', 'Jenis Pelanggan', 'required');	
	$this->form_validation->set_rules('id_konsumen', 'id_konsumen', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel($q="")
    {
		//var_dump($q);
        $this->load->helper('exportexcel');
        $namaFile = "tbl_konsumen.xls";
        $judul = "tbl_konsumen";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
		
		$condition = array("id_user"=>$this->custom_library->Filter_by_level($this->session->userdata("id_user"),$this->session->userdata("level")));
		if($q != ""){
			$dataAll = $this->Konsumen_model->get_all($q) ;
		}else{
			$dataAll = $this->Konsumen_model->get_all($condition,1) ;
		}
		//var_dump($condition);
		//var_dump($dataAll);die();
	
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Kode Korsal");
	xlsWriteLabel($tablehead, $kolomhead++, "Nama Korsal");
	xlsWriteLabel($tablehead, $kolomhead++, "Nama Konsumen");
	xlsWriteLabel($tablehead, $kolomhead++, "Telp Konsumen");
	xlsWriteLabel($tablehead, $kolomhead++, "Alamat Konsumen");	
	xlsWriteLabel($tablehead, $kolomhead++, "Hubungan Dengan Sales");
	xlsWriteLabel($tablehead, $kolomhead++, "Jenis Pelanggan");
	xlsWriteLabel($tablehead, $kolomhead++, "Status");
	xlsWriteLabel($tablehead, $kolomhead++, "Jenis Transaksi");
	xlsWriteLabel($tablehead, $kolomhead++, "Jenis Order");
	xlsWriteLabel($tablehead, $kolomhead++, "Tanggal Survey");
	xlsWriteLabel($tablehead, $kolomhead++, "Rencana Kelas");
	

	foreach ($dataAll as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->id_user);
		xlsWriteLabel($tablebody, $kolombody++, $data->nama_user);
	    xlsWriteLabel($tablebody, $kolombody++, $data->nama_konsumen);
	    xlsWriteLabel($tablebody, $kolombody++, $data->telp_konsumen);
	    xlsWriteLabel($tablebody, $kolombody++, $data->alamat_konsumen);		
		xlsWriteLabel($tablebody, $kolombody++, $data->email_konsumen);
		xlsWriteLabel($tablebody, $kolombody++, $data->jenis_pelanggan);
		xlsWriteLabel($tablebody, $kolombody++, $data->status);
		xlsWriteLabel($tablebody, $kolombody++, $data->jenis_transaksi);
		xlsWriteLabel($tablebody, $kolombody++, $data->jenis_order);
		xlsWriteLabel($tablebody, $kolombody++, $data->tanggal_survey);
		xlsWriteLabel($tablebody, $kolombody++, $data->rencana_kelas);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }*/

   

}

/* End of file Konsumen.php */
/* Location: ./application/controllers/Konsumen.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2016-08-16 13:25:39 */
/* http://harviacode.com */