I<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Push_message extends CI_Controller {
	
	public function index()
	{
		$data['title'] ="Push Message SIP";
		$data["action"] = site_url("push_message/send");
			
		$this->load->view("push_message",$data);	
		

	}
	public function send(){
		$message = $this->input->post("message");
		$submit = $this->input->post("submit");
		if($message ==""){
			echo "Pesan tidak boleh kosong <br> <a href='javascript:history.go(-1)'>kembali</a>";
		}else{
			$response = $this->sendMessage($message);
			$return["allresponses"] = $response;
			$return = json_encode( $return);
			
			  print("\n\nJSON received:\n");
				print($return);
			  print("\n");		
			$data['title'] ="Push Message SIP";
			$data["action"] = site_url("push_message/send");
				
			$this->load->view("push_message",$data);	
		}
		
	}
	function sendMessage($message){
		$content = array(
			"en" => $message
			);
		
		$fields = array(
			'app_id' => "19afaddf-61a5-494c-9f7e-5a21930124d8",
			'included_segments' => array('Active Users'),
			'data' => array("info_id"=> "","judul_info"=>"Informasi Terbaru","isi_info"=>$message,"d_entry"=>date("d-m-Y")),
			'contents' => $content
		);

	$fields = json_encode($fields);
	print("\nJSON sent:\n");
	print($fields);
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8','Authorization: Basic NGJiZmYzNjItYWM2NC00NjUzLTkwYTktMzg3MGNkMjgwY2Yy  '));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		$response = curl_exec($ch);
		curl_close($ch);
		
		return $response;
	}
}